import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { HelperProvider } from '../helper/helper';
import 'rxjs/add/operator/timeout';
import { Storage } from '@ionic/storage';
import { LoadingController } from 'ionic-angular';

@Injectable()

export class ApiservicesProvider {
  apiUrl: any;
  serviceurl: any
  constructor(public helper: HelperProvider, public http: HttpClient, public storage: Storage, public loadingCtrl: LoadingController) {
    console.log('Hello ApiservicesProvider Provider');
    storage.get("url").then((val) => {
      this.serviceurl = val
      this.helper.serviceurl = this.serviceurl
    });
  }
  //===============Login Page ===========================
  logAnyError(err) {
    console.log(err);
  }
  login(username, password, MobileId,FireBaseId,MobileType, grant_Type, successCallback, failureCallback) {
    let headers = new HttpHeaders();
    let parameter = new HttpParams()
      .set('username', username)
      .set('password', password)
      .set('MobileId', MobileId)
      .set('FireBaseId',FireBaseId)
      .set('MobileType',MobileType)
      .set('grant_Type', grant_Type);
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded');
    let serviceUrl = this.helper.serviceurl + '/token';
    this.http.post(serviceUrl, parameter, { headers: headers })
    .timeout(15000)
      .subscribe(
        data => {
          successCallback(JSON.stringify(data))
        },
        err => {
          failureCallback(err);
        }
      )
  }
  // ===================== (10) Attendance page =======================
  insertFingerPrint(Latitude, Longitude, EmpId, Direction, successCallback, failureCallback) {
    let headers = new HttpHeaders();
   
    let parameter = new HttpParams()
      .set('Latitude', Latitude)
      .set('Longitude', Longitude)
      .set('EmpId', EmpId)
      .set('Direction', Direction)
     console.log(JSON.stringify(parameter))
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/InsertFingerPrint';
    this.http.post(serviceUrl, parameter, { headers: headers })
      .timeout(40000)
      .subscribe(
        data => {
          successCallback(JSON.stringify(data))
        },
        err => {
          failureCallback(err);
        }
      )

  }

  insertFingerPrintForOffline(Latitude, Longitude, EmpId, Direction,FingerDate,SyncDate, successCallback, failureCallback) {
    let headers = new HttpHeaders();
   
    let loader = this.loadingCtrl.create({
      content: "",
    });
    loader.present();


    let parameter = new HttpParams()
      .set('Latitude', Latitude)
      .set('Longitude', Longitude)
      .set('EmpId', EmpId)
      .set('Direction', Direction)
      .set('FingerDate',FingerDate)
      .set('SyncDate',SyncDate)
     console.log(JSON.stringify(parameter))
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/InsertFingerPrint';
    this.http.post(serviceUrl, parameter, { headers: headers })
      .timeout(40000)
      .subscribe(
        data => {
          loader.dismiss()

          successCallback(JSON.stringify(data))
        },
        err => {
          loader.dismiss()
          
          failureCallback(err);
        }
      )

  }

  getCheckInOut(EmpId, Date, Direction, successCallback, failureCallback) {

    let headers = new HttpHeaders();
    let parameter = new HttpParams()
      .set('EmpId', EmpId)
      .set('Date', Date)
      .set('Direction', Direction)
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/GetCheckInOut?EmpId=' + EmpId + '&Date=' + Date + '&Direction=' + Direction;
    this.http.get(serviceUrl, { headers: headers })
      .timeout(15000)
      .subscribe(
        data => {
          successCallback(JSON.stringify(data))
        },
        err => {
          failureCallback(err)
        }
      )

  }
  saveFakeLocationLog(EmpId,successCallback, failureCallback) {
    // http://159.8.64.60:1111/api/HR/SAveFakeLocationLog?EmpId=33
    let headers = new HttpHeaders();
 
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/SAveFakeLocationLog?EmpId=' + EmpId ;
    this.http.get(serviceUrl, { headers: headers })
      .timeout(15000)
      .subscribe(
        data => {
          successCallback(JSON.stringify(data))
        },
        err => {
          failureCallback(err)
        }
      )

  }
  SaveEvent(EmpId, Direction, EventType, DayStatus, StartDate, EndDate, Location, ProjectId, Notes, successCallback, failureCallback) {

    let headers = new HttpHeaders();
    let parameter = new HttpParams()
      .set('EmpId', EmpId)
      .set('Direction', Direction)
      .set('EventType', EventType)
      .set('DayStatus', DayStatus)
      .set('StartDate', StartDate)
      .set('EndDate', EndDate)
      .set('Location', Location)
      .set('ProjectId', ProjectId)
      .set('Notes', Notes)
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/SaveEvent';
    this.http.post(serviceUrl, parameter, { headers: headers })
      .timeout(15000)
      .subscribe(
        data => {
          successCallback(data)
        },
        err => {
          failureCallback(err)
        }
      )

  }
  // ==============(12) Get All Projects in "outside reguest" page ======================
  getProjectsID(Direction, successCallback, failureCallback) {
    let headers = new HttpHeaders();
    let parameter = new HttpParams()
      .set('Direction', Direction)

    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/GetAllProjects?Direction=' + Direction + ''+ '&text=';
    this.http.get(serviceUrl, { headers: headers })
      .timeout(15000)
      .subscribe(
        data => {
          console.log(data);
          successCallback(data)
        },
        err => {
          failureCallback(err)
        }
      )

  }

  // ==============(15) save event in "overtime reguest" page ======================

  SaveEventOverTime(EmpId, Direction, EventType, DayStatus, Minutes, StartDate, EndDate, Notes, successCallback, failureCallback) {
    let headers = new HttpHeaders();
    let parameter = new HttpParams()
      .set('EmpId', EmpId)
      .set('Direction', Direction)
      .set('EventType', EventType)
      .set('DayStatus', DayStatus)
      .set('Minutes', Minutes)
      .set('StartDate', StartDate)
      .set('EndDate', EndDate)
      .set('Notes', Notes)
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/SaveEvent';
    this.http.post(serviceUrl, parameter, { headers: headers })
      .timeout(15000)
      .subscribe(
        data => {
          successCallback(JSON.stringify(data))
        },
        err => {
          failureCallback(err)
        }
      )

  }

  // ==============(7) get minutes in "overtime reguest" page ======================

  getOverTimeMinutes(EmpId, PermissionDate, successCallback, failureCallback) {
    let headers = new HttpHeaders();
    let parameter = new HttpParams()
      .set('EmpId', EmpId)
      .set('PermissionDate', PermissionDate)

    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/GetEmployeeOverTimeValue?EmpId=' + EmpId + '&PermissionDate=' + PermissionDate;
    this.http.get(serviceUrl, { headers: headers })
      .timeout(15000)
      .subscribe(
        data => {
          successCallback(JSON.stringify(data))
        },
        err => {
          failureCallback(err)
        }
      )

  }



  // ==============(11) save event in "permission reguest" page ======================

  SavePermission(EmpId, Direction, TotalBalance, MinBalance, MaxBalance, Minutes, EventType, DayStatus, StartDate, EndDate, successCallback, failureCallback) {
    let headers = new HttpHeaders();
    let parameter = new HttpParams()
      .set('EmpId', EmpId)
      .set('Direction', Direction)
      .set('TotalBalance', TotalBalance)
      .set('MinBalance', MinBalance)
      .set('MaxBalance', MaxBalance)
      .set('Minutes', Minutes)
      .set('EventType', EventType)
      .set('DayStatus', DayStatus)
      .set('StartDate', StartDate)
      .set('EndDate', EndDate)

    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/SaveEvent';
    this.http.post(serviceUrl, parameter, { headers: headers })
      .timeout(15000)
      .subscribe(
        data => {
          successCallback(JSON.stringify(data))
        },
        err => {
          failureCallback(err)
        }
      )

  }


  // ==============(6) Get Balance,Min,Max in "permission reguest" page ======================

  getPermissionBalance(EmpId, Direction, PermissionDate, successCallback, failureCallback) {
    let headers = new HttpHeaders();
    let parameter = new HttpParams()
      .set('EmpId', EmpId)
      .set('Direction', Direction)
      .set('PermissionDate', PermissionDate)

    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/GetEmployeePermissionBalance?EmpId=' + EmpId + '&Direction=' + Direction + '&PermissionDate=' + PermissionDate;
    this.http.get(serviceUrl, { headers: headers })
      .timeout(15000)
      .subscribe(
        data => {
          successCallback(JSON.stringify(data))
        },
        err => {
          failureCallback(err)
        }
      )

  }


  // ==============(14) save event in "Leave reguest" page ======================

  SaveRequestVacation(EmpId, Direction, EventType, EventId, DayStatus, StartDate, EndDate, Notes, successCallback, failureCallback) {
    let headers = new HttpHeaders();
    let parameter = new HttpParams()
      .set('EmpId', EmpId)
      .set('Direction', Direction)
      .set('EventType', EventType)
      .set('EventId', EventId)
      .set('DayStatus', DayStatus)
      .set('StartDate', StartDate)
      .set('EndDate', EndDate)
      .set('Notes', Notes)

    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/SaveEvent';
    this.http.post(serviceUrl, parameter, { headers: headers })
      .timeout(15000)
      .subscribe(
        data => {
          successCallback(JSON.stringify(data))
        },
        err => {
          failureCallback(err)
        }
      )

  }

  // ==============(4) Get Employee All Vacations in "Leave reguest" page ======================

  getVacations(EmpId, Direction, successCallback, failureCallback) {
    let headers = new HttpHeaders();
    let parameter = new HttpParams()
      .set('EmpId', EmpId)
      .set('Direction', Direction)

    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/GetAllEmployeeVacation?EmpId=' + EmpId + '&Direction=' + Direction;

    this.http.get(serviceUrl, { headers: headers })
      .timeout(15000)
      .subscribe(
        data => {
          successCallback(JSON.stringify(data))
        },
        err => {
          failureCallback(err)
        }
      )

  }
  // ==============(23) get new notification "notification page" ======================
  getnewNotification(EmpId, successCallback, failureCallback) {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/ReadFireBaseNotifyLog?EmpId=' +EmpId;
    this.http.get(serviceUrl, { headers: headers })
      .timeout(15000)
      .subscribe(
        data => {
          console.log(data);
          successCallback(data)
        },
        err => {
          failureCallback(err)
        }
      )
  }
  // ==============(24) mark notification as seen "notifications page" ======================
  markasread(notificationid, successCallback, failureCallback) {
    let headers = new HttpHeaders();
    let parameter = new HttpParams()
      .set('NotificatioId', notificationid)

    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/MarkNotificationAsSeen?NotificationId=' + notificationid;
    this.http.post(serviceUrl, parameter, { headers: headers })
      .timeout(15000)
      .subscribe(
        data => {
          successCallback(JSON.stringify(data))
        },
        err => {
          failureCallback(err)
        }
      )

  }
  // ==============(25) get new notification "notification page" ======================
  RemoveEvent(EventId, successCallback, failureCallback) {
    let headers = new HttpHeaders();

    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/RemoveEvent?EventId=' + EventId;
    this.http.get(serviceUrl, { headers: headers })
      .timeout(15000)
      .subscribe(
        data => {
          console.log(data);
          successCallback(data)
        },
        err => {
          failureCallback(err)
        }
      )
  }
  // ==============(9) get only Employee salary "payslips page" ======================
  getEmployeeSalary(SalaryId, EmpId, Direction, successCallback, failureCallback) {
    let headers = new HttpHeaders();

    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/OnlyEmplpoyeePayRollSalary?SalaryId=' + SalaryId + '&EmpId=' + EmpId + '&Direction=' + Direction;
    this.http.get(serviceUrl, { headers: headers })
      .timeout(15000)
      .subscribe(
        data => {
          console.log(data);
          successCallback(data)
        },
        err => {
          failureCallback(err)
        }
      )
  }
  // ==============() get All salary Dates "payslips page" ======================
  getAllSalaryDates(EmpId,Direction, successCallback, failureCallback) {
    let headers = new HttpHeaders();

    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/GetAllSalaryDates?EmpId=' + EmpId + '&Direction=' + Direction;
    this.http.get(serviceUrl, { headers: headers })
      .timeout(15000)
      .subscribe(
        data => {
          console.log(data);
          successCallback(data)
        },
        err => {
          failureCallback(err)
        }
      )
  }

  ReadEmployeeDueCuts(Id, Direction, successCallback, failureCallback) {
    let headers = new HttpHeaders();

    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/ReadEmployeeDueCuts?Id=' + Id + '&Direction=' + Direction;
    this.http.get(serviceUrl, { headers: headers })
      .timeout(15000)
      .subscribe(
        data => {
          console.log(data);
          successCallback(data)
        },
        err => {
          failureCallback(err)
        }
      )
  } 
  readVacationRequests(Id, type, Direction, successCallback, failureCallback) {
    let loader = this.loadingCtrl.create({
      content: "",
    });
    loader.present();
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/ReadEmployeeFollowersEvents?Type=' + type + '&EmpId=' + Id + '&Direction=' + Direction;
    this.http.get(serviceUrl, { headers: headers })
      .timeout(15000)
      .subscribe(
        data => {
          loader.dismiss()
          console.log(data);
          successCallback(data);
        },
        err => {
          loader.dismiss()
          failureCallback(err);
        }
      )
  }
  vacationRequestAccept(empId, eventId, Direction, successCallback, failureCallback) {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/AcceptEvents?id=' + eventId + '&EmpId=' + empId + '&Direction=' + Direction;

    this.http.get(serviceUrl, { headers: headers })
      .timeout(15000)
      .subscribe(
        data => {
          console.log(data);
          successCallback(data);
        },
        err => {
          failureCallback(err);
        }
      )
  }

  vacationRequestReject(empId, eventId, Direction, successCallback, failureCallback) {

    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/RejectEvent?id=' + eventId + '&EmpId=' + empId;
    this.http.get(serviceUrl, { headers: headers })
      .timeout(15000)
      .subscribe(
        data => {
          console.log(data);
          successCallback(data);
        },
        err => {
          failureCallback(err);
        }
      )
  }
  checkvalidemployee(empId, successCallback, failureCallback) {
    
    let loader = this.loadingCtrl.create({
      content: "",
    });
    loader.present();
    
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/IsValidEmployee?EmpId=' + empId;
    
    this.http.get(serviceUrl, { headers: headers })
      .timeout(15000)
      .subscribe(
        data => {
          loader.dismiss()
          console.log(data);
          successCallback(data);
        },
        err => {
          loader.dismiss()
          failureCallback(err);
        }
      )
  }
  messages(type,index,text,empId,dir, successCallback, failureCallback) {
    
    let loader = this.loadingCtrl.create({
      content: "",
    });
    loader.present();
    
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/ReadTickets?TicketType=' + type+ '&Index=' + index+'&Text='+text+'&EmpId='+empId+'&Direction='+dir;
    
    this.http.get(serviceUrl, { headers: headers })
      .timeout(15000)
      .subscribe(
        data => {
          loader.dismiss()
          successCallback(data);
        },
        err => {
          loader.dismiss()
          failureCallback(err);
        }
      )
  }
  viewmessages(id,empId,dir, successCallback, failureCallback) {
    
    let loader = this.loadingCtrl.create({
      content: "",
    });
    loader.present();
    
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/GetTicketById?Id=' + id+ '&EmpId=' + empId+'&Direction='+dir;
    
    this.http.get(serviceUrl, { headers: headers })
      .timeout(15000)
      .subscribe(
        data => {
          loader.dismiss()
          console.log(data);
          successCallback(data);
        },
        err => {
          loader.dismiss()
          failureCallback(err);
        }
      )
  }
  listnumbers(empId ,successCallback, failureCallback) {
    
    let loader = this.loadingCtrl.create({
      content: "",
    });
    loader.present();
    
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/ReadIndexTicketsCount?EmpId=' + empId;
    
    this.http.get(serviceUrl, { headers: headers })
      .timeout(15000)
      .subscribe(
        data => {
          loader.dismiss()
          console.log(data);
          successCallback(data);
        },
        err => {
          loader.dismiss()
          failureCallback(err);
        }
      )
  }
  AcceptReject(id,empId,dir,app ,successCallback, failureCallback) {
    console.log(id+"منن"+ empId)
    let loader = this.loadingCtrl.create({
      content: "",
    });
    loader.present();
    
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/SaveTicketingApprovedOrDis?TicketId=' + id+ '&EmpId=' + empId+'&Direction='+dir+'&IsApproved='+app ;
    
    this.http.get(serviceUrl, { headers: headers })
      .timeout(15000)
      .subscribe(
        data => {
          loader.dismiss()
          console.log(data);
          successCallback(data);
        },
        err => {
          loader.dismiss()
          failureCallback(err);
        }
      )
  }
  Allemps(index,dir,text,successCallback, failureCallback) {
    
  
    
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/GetAllEmployees?Index='+index +'&Direction='+dir+'&text='+text ;
    
    this.http.get(serviceUrl, { headers: headers })
      .timeout(15000)
      .subscribe(
        data => {
          
          console.log(data);
          successCallback(data);
        },
        err => {
          
          failureCallback(err);
        }
      )
  }
  replay(empid,message,data,id,successCallback, failureCallback) {
    
    let loader = this.loadingCtrl.create({
      content: "",
    });
    loader.present();
    
    let headers = new HttpHeaders();
    let parameter = new HttpParams()
    .set('EmpId', empid)
    .set('Body', message)
    .set('ToEmpList', data)
    .set('TicketId',id)
   
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/SaveReplyTicket' ;
    
    this.http.post(serviceUrl,parameter, { headers: headers })
      .timeout(15000)
      .subscribe(
        data => {
          loader.dismiss()
          console.log(data);
          successCallback(data);
        },
        err => {
          loader.dismiss()
          failureCallback(err);
        }
      )
  }
  addmessage(dir,successCallback, failureCallback) {
    
    let loader = this.loadingCtrl.create({
      content: "",
    });
    loader.present();
    
    let headers = new HttpHeaders();
  
   
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/TicketingSendToTypes?Direction='+dir ;
    
    this.http.get(serviceUrl, { headers: headers })
      .timeout(15000)
      .subscribe(
        data => {
          loader.dismiss()
          console.log(data);
          successCallback(data);
        },
        err => {
          loader.dismiss()
          failureCallback(err);
        }
      )
  }
  getdepart(type,dir,text,successCallback, failureCallback) {
    
    let loader = this.loadingCtrl.create({
      content: "",
    });
    loader.present();
    
    let headers = new HttpHeaders();
  
   
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/FillDeptUnit?Type='+type+'&Direction='+dir+'&text='+text ;
    
    this.http.get(serviceUrl, { headers: headers })
      .timeout(15000)
      .subscribe(
        data => {
          loader.dismiss()
          console.log(data);
          successCallback(data);
        },
        err => {
          loader.dismiss()
          failureCallback(err);
        }
      )
  }
  templates(dir,successCallback, failureCallback) {
    
    let loader = this.loadingCtrl.create({
      content: "",
    });
    loader.present();
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/FillTicketingTemplate?Direction='+dir ;
    
    this.http.get(serviceUrl, { headers: headers })
      .timeout(15000)
      .subscribe(
        data => {
          loader.dismiss()
          console.log(data);
          successCallback(data);
        },
        err => {
          loader.dismiss()
          failureCallback(err);
        }
      )
  }
  savemsg(files,msg,subj,temp,id,list,empid,successCallback, failureCallback) {
  console.log(typeof(msg))
  console.log(typeof(subj))
  console.log(typeof(temp))
  console.log(typeof(id))
  console.log(typeof(list))
  console.log(typeof(empid))



    if(files==[])
        {

        }
        else{  
          for(var j=0;j<files.length;j++)
          {
        var byteCharacters= atob(files[j].Bytes);
        console.log(byteCharacters)
        var byteNumbers = new Array(byteCharacters.length);
        for (var i = 0; i < byteCharacters.length; i++) {
          byteNumbers[i] = byteCharacters.charCodeAt(i);
      }
      var byteArray = new Uint8Array(byteNumbers);
      files[j].Bytes=byteNumbers 
  }
}    

let parameter=
{
  "Subject":subj,
  "Message": msg,
  "ServerIP": this.helper.serviceurl,
  "IsTemplate": temp,
  "TemplateId": id,
  "ToEmpList":list,
  "EmpId": empid,
  "Docs":files
}
   console.log(JSON.stringify(parameter))
   console.log(parameter)
    let headers = new HttpHeaders();
   
   
        headers = headers.set('Content-Type', 'application/json')
        .set('Authorization', 'Bearer ' + this.helper.accesstoken);
        let serviceUrl = this.helper.serviceurl + '/api/HR/SaveTickets' ;
        this.http.post(serviceUrl,JSON.stringify(parameter), { headers: headers })
          .timeout(15000)
          .subscribe(
            data => {
              files=[]
              successCallback(data);
            },
            err => {
              failureCallback(err);
            }
          )
      }
  templatesbody(id,dir,successCallback, failureCallback) {
    
    let loader = this.loadingCtrl.create({
      content: "",
    });
    loader.present();
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/GetTemplateData?id='+id +'&Direction='+dir ;
    
    this.http.get(serviceUrl, { headers: headers })
      .timeout(15000)
      .subscribe(
        data => {
          loader.dismiss()
          console.log(data);
          successCallback(data);
        },
        err => {
          loader.dismiss()
          failureCallback(err);
        }
      )
  }
  getdepmanager(id,type,successCallback, failureCallback) {
    
    let loader = this.loadingCtrl.create({
      content: "",
    });
    loader.present();
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/GetDeptUnitsManagers?id='+id +'&Type='+type ;
    
    this.http.get(serviceUrl, { headers: headers })
      .timeout(15000)
      .subscribe(
        data => {
          loader.dismiss()
          successCallback(data);
        },
        err => {
          loader.dismiss()
          failureCallback(err);
        }
      )
  }



  getFingerCred(successCallback, failureCallback) {
    let headers = new HttpHeaders();
    
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + this.helper.accesstoken);
    let serviceUrl = this.helper.serviceurl + '/api/HR/GetFingerCred?EmpId='+this.helper.Empid;
    this.http.get(serviceUrl, { headers: headers })
      .timeout(15000)
      .subscribe(
        data => {
          console.log(data);
          successCallback(data)
        },
        err => {
          failureCallback(err)
        }
      )

  }



}
