import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastController , AlertController, Platform} from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Diagnostic } from '@ionic-native/diagnostic';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class HelperProvider {
  public netwrokStatus = 1; //1 online , 0 offline
  public mockLocation = true;
  public gpsEnabled_bg = false
  public bg_loc_determined = false;
  public background_lat;
  public background_lng;
  public lang_direction;
  public currentLang;
  public serviceurl :string = "";
  public accesstoken: string = "";
  public DeviceId ;
  grant_Type = 'password';
  Empid: any;
  Direction = "ltr";
  formattedData: any;
  EventType = 1;
  DayStatus = 1;
  status:any
  DayStatusPermission = 5;
  EventTypePermission = 3;
  EventTypeOverTime = 4;
  EventTypeLeave = 2;
  statusOverTime = 5;
  userLat
  userLong
  userLocAccuracy
  location_status

  registration;
  device_type;
  constructor(public http: HttpClient,public platform: Platform, public toastCtrl: ToastController,
    public alertCtrl: AlertController, public translate: TranslateService,
    public diagnostic: Diagnostic, public locationAccuracy: LocationAccuracy , private geolocation: Geolocation,) {
    console.log('Hello HelperProvider Provider');

    var dateObj = new Date();
    var year = dateObj.getFullYear().toString();
    var month = dateObj.getMonth().toString();
    var date = dateObj.getDate().toString();

    this.formattedData = date + '/' + month + '/' + year;
    console.log(this.formattedData);
  }
  public presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 5000,
      position: 'bottom'
    });
    toast.present();
  }

  geoLoc(success) {
console.log("entering geoloc")
    let LocationAuthorizedsuccessCallback = (isAvailable) => {
      //console.log('Is available? ' + isAvailable);
      if (isAvailable) {
        // this.locAlert = 0
        console.log("entering is available")
        // if (this.platform.is('android')) {
        //   this.diagnostic.getLocationMode().then((status) => {
        //     console.log("entering getLocationMode "+status)
        //     if (!(status == "high_accuracy")) {
        //       this.requestOpenGPS(success)
        //     }
        //     else {
        //       this.GPSOpened(success);
        //     }
        //   })
        // }
        // else {
        //   this.requestOpenGPS(success);
        // }
        this.GPSOpened(success);
      }
      else {
        this.requestOpenGPS(success);
      }
    };
    let LocationAuthorizederrorCallback = (e) => {
      console.error(e)
      this.requestOpenGPS(success);
    }
      ;
    this.diagnostic.isLocationAvailable().then(LocationAuthorizedsuccessCallback).catch(LocationAuthorizederrorCallback);

  }
  GPSOpened(success) {
    console.log("GPSOpened")
    success("1");
    // let optionsLoc = {}
    // optionsLoc = { timeout: 30000, enableHighAccuracy: true, maximumAge: 3600 };
    // this.geolocation.getCurrentPosition(optionsLoc).then((resp) => {
    //   //this.events.publish("locationEnabled")
    //   this.userLat = resp.coords.latitude;
    //   this.userLong = resp.coords.longitude;
    //   this.userLocAccuracy = resp.coords.accuracy;
    //   let data = {
    //     inspectorLat: resp.coords.latitude,
    //     inspectorLong: resp.coords.longitude,
    //     inspectorLocAccuracy: resp.coords.accuracy
    //   }
    //   success(data);
    // }
    // ).catch((error) => {
    //   success("-1");
    // });


  }
  requestOpenGPS(success) {
    if (this.platform.is('ios')) {
      this.diagnostic.isLocationEnabled().then(enabled => {
        if(enabled){
          this.diagnostic.getLocationAuthorizationStatus().then(status => {
            if (status == this.diagnostic.permissionStatus.NOT_REQUESTED) {
              this.diagnostic.requestLocationAuthorization().then(status => {
                if (status == this.diagnostic.permissionStatus.NOT_REQUESTED) {
                  this.locationAccuracy.canRequest().then(requested => {
                    if (requested) {
                      this.requestOpenGPS(success)
                    }
                    else {
                      success("-1")
                    }
  
                  }).catch(err => success("-1"))
                }
                else if (status == this.diagnostic.permissionStatus.GRANTED || status == this.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE) {
                  this.GPSOpened(success);
                }
                else{
                  success("-1")
                }
              })
            }
            else if (status == this.diagnostic.permissionStatus.DENIED) {
              success("-1")
            }
            else if (status == this.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE) {
              this.GPSOpened(success);
            }
          });
        }
        else{
          this.diagnostic.requestLocationAuthorization().then(val => {
            if (val == "GRANTED") {
              this.requestOpenGPS(success)
            }
            else {
              success("-1")
            }
          })
        }
      })
    }
    else {
      this.diagnostic.isLocationAuthorized().then(authorized => {
        if (authorized) {
          this.locationAccuracy.canRequest().then((canRequest: boolean) => {
            if (canRequest) {
              this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
                (data) => {
                  console.log('Request successful'+ JSON.stringify(data))
                  this.GPSOpened(success);
                },
                error => {
                  console.log('Error requesting location permissions', error);

                  success("-1")
                }
              );
            }

            else {
              console.log('Error requesting location permissions');

              success("-1")
          
            }
          });
        }
        else {
          this.diagnostic.requestLocationAuthorization().then(val => {
            if (val == "GRANTED") {
              this.requestOpenGPS(success)
            }
            else {
              success("-1")
            }
          })
        }
      })
    }

  }
}
