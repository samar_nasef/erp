import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, Platform } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { HelperProvider } from '../../providers/helper/helper';
import { ApiservicesProvider } from '../../providers/apiservices/apiservices';

/**
 * Generated class for the EmployeesListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-employees-list',
  templateUrl: 'employees-list.html',
})
export class EmployeesListPage {
  employeesList = []
  invitefriends
  searchWord=""
  page = 0
  langDirection="";
  hideMoreBtn = false
  constructor(public navCtrl: NavController, public helper:HelperProvider, public navParams: NavParams,
    public service:ApiservicesProvider, public translate: TranslateService, public viewCtrl:ViewController,
  public platform: Platform) {
      this.langDirection = this.helper.lang_direction;
      this.invitefriends = this.translate.instant("invitefriends")
  }

  empChecked(item,ev){
    item.checked = !item.checked
  }

  searchEmp(ev: any)
  {
    if (ev.target.value == undefined) {
      this.searchWord = "" 
    }else{
      this.searchWord = ev.target.value
    }
  }
  loadMore(){
    this.page +=1
    this.service.Allemps(this.page,this.platform.isRTL ? 'rtl' : 'ltr',this.searchWord,(data)=>{
      console.log(JSON.stringify(data))
      if(data.length == 0){
        this.hideMoreBtn = true
      }
          for(let x=0;x<data.length;x++){
       data[x].checked = false;
       this.employeesList.push(data[x])
     }
     },(data)=>{
       console.log(JSON.stringify(data))
     })
  }
  searchEmployees(){
    if(navigator.onLine){
      this.employeesList = []
      this.page = 1
      this.service.Allemps(this.page,this.platform.isRTL ? 'rtl' : 'ltr',this.searchWord,(data)=>{
        console.log(JSON.stringify(data))
        if(data.length == 0){
          this.hideMoreBtn = true
        }
            for(let x=0;x<data.length;x++){
         data[x].checked = false;
       }
        this.employeesList = data
       },(data)=>{
         console.log(JSON.stringify(data))
       })
    }
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad InvitePopoverPage');
    this.employeesList = []
    this.page = 1
        this.service.Allemps(this.page,this.platform.isRTL ? 'rtl' : 'ltr',"",(data)=>{
          console.log(JSON.stringify(data))
          if(data.length == 0){
            this.hideMoreBtn = true
          }
              for(let x=0;x<data.length;x++){
           data[x].checked = false;
         }
          this.employeesList = data
         },(data)=>{
           console.log(JSON.stringify(data))
         })
  }
  DismissInvite(type){
    if(type == 2){
      this.employeesList = []
    }
    let invites = []
    for(let x=0;x<this.employeesList.length;x++){
      if(this.employeesList[x].checked){
        invites.push({id:this.employeesList[x].Value, name: this.employeesList[x].Text})
      }
    }
    this.viewCtrl.dismiss(JSON.stringify(invites))
  }

}
