import { Component } from '@angular/core';
import { NavController, AlertController, ToastController ,MenuController,Events, LoadingController, App} from 'ionic-angular';
import { NotificaionPage } from '../notificaion/notificaion';
import { TranslateService } from '@ngx-translate/core';
import { HelperProvider } from '../../providers/helper/helper';
import { Platform } from 'ionic-angular/platform/platform';
import { Geolocation } from '@ionic-native/geolocation';
import { ApiservicesProvider } from '../../providers/apiservices/apiservices';
import { Storage } from '@ionic/storage';
import { DatePipe } from '@angular/common';
import { Diagnostic } from '@ionic-native/diagnostic';
import { LoginPage } from '../login/login';
import { Network } from '@ionic-native/network';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';
import { stringify } from '@angular/core/src/util';

//declare var google: any;
//declare var window: any;
declare var AdvancedGeolocation;

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
  providers : [FingerprintAIO]
})

export class ContactPage {
  long: any;
  lat: any;
  orgLat;
  orgLong;
  moke:any
  myDate: string;
  AttendTime;
  LeaveTime;
  currentDate: any;
  formattedData: any;
  unixTime: any;
  attendtime:any
  attenddate:any
  leavetime:any
  leavedate:any
  serviceurl:any
  show:any=false
  hide:any=true
  langDirection: any;
  loader:any
  constructor(private faio: FingerprintAIO,public event:Events,public translate: TranslateService, public helper: HelperProvider
    , private alertCtrl: AlertController
    , private alrtCtrl: AlertController
    ,private diagnostic: Diagnostic
    ,public menu:MenuController
    , public platform: Platform
    ,public loadingCtrl:LoadingController
    , public geolocation: Geolocation
    , public toastCtrl: ToastController
    
    , public storage: Storage
    , public ApiservicesProvider: ApiservicesProvider
    ,public plt:Platform
    , public navCtrl: NavController, public datepipe: DatePipe) {


// remove this line 
// this.storage.set("AllowOfflineFinger", false); //false offlinefingerprint not available , true offlinefingerprint available



    this.myDate = this.datepipe.transform(new Date().toISOString(), 'MM/dd/yyyy');// new Date().toISOString();
   // to take authorization for device location
console.log("this.mydate ",this.myDate)
console.log(" datepipe, ",this.datepipe.transform(new Date().toISOString(), 'MM/dd/yyyy H:mm'))
  //  alert("pipe "+this.datepipe.transform(new Date().toISOString(), 'MM/dd/yyyy HH:MM')+ " , new date "+ new Date().toISOString()+" , finger print "+ "  ,sync :"+ this.datepipe.transform(new Date().toISOString(), 'MM/dd/yyyy HH:MM'))

  // this.faio.isAvailable().then(resp=>{
  //   alert("finger print is available "+
  //   JSON.stringify(resp))
  // }).catch(resp=>{
  //   alert("err finger print : " + JSON.stringify(resp))
  // })
   this.event.subscribe("successData", (data)=>{
     console.log("successData subscribe")
    
     this.successCallBackLocationForoffline(data);

   });

   this.event.subscribe("errorData", (data)=>{
    console.log("errorData subscribe")
   this.failureCallBackLocationForoffline(data)
  });


    if(this.plt.is('ios'))
        {
        this.diagnostic.requestLocationAuthorization("always");
        }
        else{
          this.diagnostic.requestLocationAuthorization();
        } 
        // to get data of leave and arrive for user
    this.getLeaveArriveTime();
    storage.get("url").then((val) => {
      this.serviceurl = val
      this.helper.serviceurl=this.serviceurl
    });
    if (this.helper.currentLang == 'ar') {
      this.translate.use('ar');
      this.helper.currentLang = 'ar';
      this.translate.setDefaultLang('ar');
      this.helper.lang_direction = 'rtl';
      this.langDirection = "rtl";
      this.platform.setDir('rtl', true)
    }
    else {

      this.translate.use('en');
      this.helper.currentLang = 'en';
      this.translate.setDefaultLang('en');
      this.helper.lang_direction = 'ltr';
      this.langDirection = "ltr";
      this.platform.setDir('ltr', true)
    }
  }
  successCallbac(result) {
    this.moke=result.isMock
  }
 
  errorCallbac(error) {
    console.log("faill"+JSON.stringify( error));
  }
  ionViewDidLoad(){
    
  }

  ionViewWillEnter(){
    console.log("ionViewWillEnter")
    this.ApiservicesProvider.getFingerCred((data) => {
      console.log("resp from getFingerCred : ",data)
      var jsonObject = data//JSON.parse(data);
      console.log("jsonObject.AllowOfflineFIngerPrint : ",jsonObject.AllowOfflineFIngerPrint)
      this.storage.set("AllowSensor", jsonObject.AllowSensor); //0 fingerprint not necessary , 1 fingerprint necessary
      this.storage.set("AllowOfflineFinger", jsonObject.AllowOfflineFIngerPrint); //0 offlinefingerprint not available , 1 offlinefingerprint available
      if (jsonObject.IsRemoved == 1){
        console.log("user logout from app")

        this.helper.presentToast(this.translate.instant("logoutFromAdmin"))
        this.navCtrl.setRoot(LoginPage)
        // this.menu.close()
        this.storage.remove("access_token");
        this.storage.remove("user_info");
        this.storage.remove("EmpId");
        this.storage.remove("UserPass");
        this.storage.remove("IsAdmin");
        this.event.publish("direction",this.langDirection);

      }else{
console.log("user staty in app")
      }

    }, (data) => {
      console.log("error from getFingerCred : ",data)
    });
  

    this.attend()

  }


  //runBackPosition(){
  //   if(this.platform.is('android')){
  //     let loaderLoc = this.loadingCtrl.create({
  //       content: this.translate.instant("wait"),
  //     });
  //     loaderLoc.present();
  //     const config: BackgroundGeolocationConfig = {
  //       desiredAccuracy: 100,
  //       stationaryRadius: 0,
  //       interval:2000,
  //       distanceFilter: 0,
  //       debug: false, //  enable this hear sounds for background-geolocation life-cycle.
  //       stopOnTerminate: true, // enable this to clear background location settings when the app terminates
  // };
  
  // this.backgroundGeolocation.configure(config)
  // this.backgroundGeolocation.on(BackgroundGeolocationEvents.error)
  // .subscribe(err=>{
  //   loaderLoc.dismiss()
  //   this.backgroundGeolocation.stop()
  //   this.helper.geoLoc(data => this.getCurrentLoc(data));
  // })
  // this.backgroundGeolocation.on(BackgroundGeolocationEvents.location)
  // .subscribe(location =>{
  //   loaderLoc.dismiss()
  // this.helper.gpsEnabled_bg = true;
  //  // alert("isFromMockProvider"+location.isFromMockProvider+" mockLocationsEnabled "+location.mockLocationsEnabled)
  //   console.log("background loc"+JSON.stringify(location))
  //   this.helper.bg_loc_determined = true
  //   this.helper.mockLocation = location.isFromMockProvider
  //   this.helper.background_lat = location.latitude
  //   this.helper.background_lng = location.longitude
  // }
  // ,err => {
  //   console.log("bg "+err)
  //   loaderLoc.dismiss()
  // }
  // )
  
  
  // // start recording location
  // this.backgroundGeolocation.start();
  //     }
  //}
  getCurrentLoc(loc) {

    //console.log("witting loc " + JSON.stringify(loc))
    if (loc == "-1") {
      this.helper.presentToast(this.translate.instant("giveGPSPermission"))
    }
    else {
      console.log("location " + JSON.stringify(loc))
      //this.runBackPosition()

    }
  }
  ionViewDidEnter(){
    // let loaderLoc = this.loadingCtrl.create({
    //   content: "",
    //   duration: 4000
    // });
    // loaderLoc.present();
    this.helper.geoLoc(data => {
      if(data == "-1"){
        this.helper.presentToast(this.translate.instant("giveGPSPermission"))
      }else{
       // this.runBackPosition()
      }
    })
  }
  ionViewWillLeave(){
    // this.backgroundGeolocation.stop()
  }
  handleAndroidAttendenceforOffline(location , showCheckAlertOrNot){
    console.log(" handleAndroidAttendenceforOffline location : ",location)

    if(!location.latitude){
      this.helper.presentToast(this.translate.instant('tryAgain'))
      return;
    }

    if(location.latitude <= 0){
      this.helper.presentToast(this.translate.instant('tryAgain'))
      return;
    }

    
      let alertConfirm = this.alertCtrl.create({
        message: this.translate.instant('msgConfirmCheckInOut'),
        buttons: [
          {
            text: this.translate.instant('Cancel'),
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: this.translate.instant('Ok'),
            handler: () => {
                this.checkforoffline(location)
            }
  
          }
        ]
      });
  
    
      if (showCheckAlertOrNot == 0){
        alertConfirm.present();
      }
  else if (showCheckAlertOrNot == 1){
    this.checkforoffline(location)
  }

    
  }

  handleAndroidAttendence(location,showCheckAlertOrNot){

console.log(" handleAndroidAttendence location : ",location)
    if(!location.latitude){
      this.helper.presentToast(this.translate.instant('tryAgain'))
      return;
    }

    if(location.latitude <= 0){
      this.helper.presentToast(this.translate.instant('tryAgain'))
      return;
    }

    if(location.isFromMockProvider){
      this.ApiservicesProvider.saveFakeLocationLog(this.helper.Empid,resp=>{
        console.log("resp from saveFakeLocationLog : ",resp)
        //this.platform.exitApp()
      },err=>{
        console.log("err from saveFakeLocationLog : ",err)
      })
      let alert = this.alertCtrl.create({
        message: this.translate.instant('FakeLocation'),
        enableBackdropDismiss: false,
        buttons: [
         
           {
             text: this.translate.instant('agree'),
             handler: () => {
               // if user accept to prove attendence
               //this.check()
             }
   
           }
        ]
      });
      alert.present();
    }
    else{
      let alertConfirm = this.alertCtrl.create({
        message: this.translate.instant('msgConfirmCheckInOut'),
        buttons: [
          {
            text: this.translate.instant('Cancel'),
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: this.translate.instant('Ok'),
            handler: () => {
                this.check(location)
            }
  
          }
        ]
      });
     

      if (showCheckAlertOrNot == 0){
        alertConfirm.present();
      }
else if (showCheckAlertOrNot == 1){
  this.check(location)
}

    
    }
  }
   // testtimer
   attend() {
    this.storage.get('AllowSensor').then((val) => {
      if (val != null) {
        console.log("AllowSensor val : ",val)
         //0 fingerprint not necessary , 1 fingerprint necessary
        // val = 0

        if (val == 0){
          console.log("AllowSensor val == 0")
          this.goAttend(0)

        }else if (val == 1){
          console.log("AllowSensor val == 1")
          this.faio.show({
            clientId: 'Fingerprint-Demo', //Android: Used for encryption. iOS: used for dialogue if no `localizedReason` is given.
            clientSecret: 'o7aoOMYUbyxaD23oFAnJ' ,//Necessary for Android encrpytion of keys. Use random secret key.
            disableBackup:true,  //Only for Android(optional)
            localizedFallbackTitle: 'Use Pin', //Only for iOS
            localizedReason: 'Please authenticate' //Only for iOS
        })
        .then((result: any) => {
          // alert("success fingerPrint :" + JSON.stringify(result))

        //"biometric_success"
        this.goAttend(1)

        })
        .catch((error: any) => {
          // alert("error fingerPrint :" + JSON.stringify(error))
        //code: -108 , message : BIOMETRIC_DISMISSED
        // -111 , "Too many attempts try again later" 
        //-106 , "BIOMETRIC_NOT_ENROLLED ,,, fingerPrint in mobile but user don't assign it
        //-104 , "BIOMETRIC_HARDWARE_NOT_SUPPORTED" ,,, mobile doesn't have fingerprint
console.log("error code : ",error.code)
if (error.code  == -104){
  this.goAttend(0)
}else if (error.code == -106 && this.plt.is('android') ){
  this.helper.presentToast(this.translate.instant("pleaseInsertFirngerPrintFromSettingsAndTryAgain"))
}else if (error.code == -106 && this.plt.is('ios') ){
  this.goAttend(0)
}else if (error.code == -111){
  this.helper.presentToast(this.translate.instant("Too many attempts try again later"))
}else if (error.code == -108){
  // this.helper.presentToast(this.translate.instant("waitLongTryAgain"))
}

        });

        }


      }
    })


   
  }

  goAttend(showCheckAlertOrNot){
    console.log("showCheckAlertOrNot : ",showCheckAlertOrNot)
    if(this.platform.is('android')){
      let loaderLoc = this.loadingCtrl.create({
        content: this.translate.instant("detectingLoc"),
      });
      loaderLoc.present()
    this.helper.geoLoc(data => {
      //loaderLoc.dismiss()
      if(data == "-1"){
        loaderLoc.dismiss()
        this.helper.presentToast(this.translate.instant("giveGPSPermission"))
      }
      else{
        AdvancedGeolocation.start((success)=>{
          
          try{
              let detect = 0
              var jsonObject = JSON.parse(success);
              console.log("jsonObject "+ JSON.stringify(jsonObject))
              
              
              switch(jsonObject.provider){
                  case "gps":{
                  if(detect==0){
                  detect = 1
                  AdvancedGeolocation.stop()
                  loaderLoc.dismiss();
                  this.handleAndroidAttendence(jsonObject,showCheckAlertOrNot)
                  }
                  
                      break;
                  }
                  case "network":{
                    if(detect==0){
                      detect = 1
                  AdvancedGeolocation.stop()
                  loaderLoc.dismiss();
                  this.handleAndroidAttendence(jsonObject,showCheckAlertOrNot)
                    }
                      break;
                  }
  
                  case "satellite":
             //TODO
                      break;
                      
                  case "cell_info":
                    //TODO
                    break;
                    
                  case "cell_location":
                    //TODO
                    break;  
                  
                  case "signal_strength":
                    //TODO
                    break;              	
              }
          }
          catch(exc){
            AdvancedGeolocation.stop()
            loaderLoc.dismiss();
            // this.helper.presentToast(this.translate.instant('tryAgain'))
            console.log("Invalid JSON: " + exc);
            console.log("1 - networkStatus : ", this.helper.netwrokStatus)
            
              this.getCurrentLocationforOffline(showCheckAlertOrNot) //s
            
           
          }
      },
      (error)=>{
        AdvancedGeolocation.stop()
        loaderLoc.dismiss();
        // this.helper.presentToast(this.translate.instant('tryAgain'))
        console.log("2 - networkStatus : ", this.helper.netwrokStatus)
          console.log("ERROR! " + JSON.stringify(error));
         
          this.getCurrentLocationforOffline(showCheckAlertOrNot) //s
          
      },
      ////////////////////////////////////////////
      //
      // REQUIRED:
      // These are required Configuration options!
      // See API Reference for additional details.
      //
      ////////////////////////////////////////////
      {
          "minTime":500,         // Min time interval between updates (ms)
          "minDistance":1,       // Min distance between updates (meters)
          "noWarn":true,         // Native location provider warnings
          "providers":"all",     // Return GPS, NETWORK and CELL locations
          "useCache":false,       // Return GPS and NETWORK cached locations
          "satelliteData":false, // Return of GPS satellite info
          "buffer":false,        // Buffer location data
          "bufferSize":0,        // Max elements in buffer
          "signalStrength":false // Return cell signal strength data
      });
      }
})
  }
  else{
// alert("netwok status : "+ this.helper.netwrokStatus)
    // if (this.helper.netwrokStatus == 1) {
      this.helper.geoLoc(data => {
        if(data == "-1"){
          this.helper.presentToast(this.translate.instant("giveGPSPermission"))
        }
        else{
          let alertConfirm = this.alertCtrl.create({
            message: this.translate.instant('msgConfirmCheckInOut'),
            buttons: [
              {
                text: this.translate.instant('Cancel'),
                role: 'cancel',
                handler: () => {
                  console.log('Cancel clicked');
                }
              },
              {
                text: this.translate.instant('Ok'),
                handler: () => {
                    this.check()
                }
      
              }
            ]
          });


          if (showCheckAlertOrNot == 0){
            alertConfirm.present();
          }
else if (showCheckAlertOrNot == 1){
  this.check()
}
          
        }})
    
    //   }else{
    //   this.getCurrentLocationforOffline()
    // }
   





  }

  }
  // testtimer
  attend_() {
    if(this.platform.is('android')){
      let loaderLoc = this.loadingCtrl.create({
        content: this.translate.instant("wait"),
      });
      loaderLoc.present();
    this.helper.geoLoc(data => {
      //loaderLoc.dismiss()
      if(data == "-1"){
        loaderLoc.dismiss()
        this.helper.presentToast(this.translate.instant("giveGPSPermission"))
      }
      else{
        loaderLoc.dismiss()
        if(!this.helper.gpsEnabled_bg){
          let alertWaitbg = this.alertCtrl.create({
            message: this.translate.instant('waitToDetermineLoc'),
            buttons: [
              {
                text: this.translate.instant('agree'),
                handler: () => {
                  //this.runBackPosition()
                }
      
              }
            ]
          });
          alertWaitbg.present();
        }
    else if(!this.helper.bg_loc_determined){
      let alertWait = this.alertCtrl.create({
        message: this.translate.instant('waitToDetermineLoc'),
        buttons: [
          {
            text: this.translate.instant('agree'),
            handler: () => {
            }
  
          }
        ]
      });
      alertWait.present();
    }
    else if(!this.helper.mockLocation){
    let alertConfirm = this.alertCtrl.create({
      message: this.translate.instant('msgConfirmCheckInOut'),
      buttons: [
        {
          text: this.translate.instant('Cancel'),
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: this.translate.instant('Ok'),
          handler: () => {
            if(!this.helper.mockLocation){
              this.check()
            }
            else{
              this.helper.presentToast(this.translate.instant('tryAgain'))
            }
          }

        }
      ]
    });
    alertConfirm.present();
  }
  else{
     let alert = this.alertCtrl.create({
         message: this.translate.instant('userUseMockLocation'),
         enableBackdropDismiss: false,
         buttons: [
          
            {
              text: this.translate.instant('agree'),
              handler: () => {
                // if user accept to prove attendence
                //this.check()
                this.ApiservicesProvider.saveFakeLocationLog(this.helper.Empid,resp=>{
                  console.log("resp from saveFakeLocationLog : ",resp)
                  this.platform.exitApp()
                },err=>{
                  console.log("err from saveFakeLocationLog : ",err)
                })
                
              }
    
            }
         ]
       });
       alert.present();
  }
}
})
  }
  else{
    this.helper.geoLoc(data => {
      if(data == "-1"){
        this.helper.presentToast(this.translate.instant("giveGPSPermission"))
      }
      else{
        let alertConfirm = this.alertCtrl.create({
          message: this.translate.instant('msgConfirmCheckInOut'),
          buttons: [
            {
              text: this.translate.instant('Cancel'),
              role: 'cancel',
              handler: () => {
                console.log('Cancel clicked');
              }
            },
            {
              text: this.translate.instant('Ok'),
              handler: () => {
                  this.check()
              }
    
            }
          ]
        });
        alertConfirm.present();
      }})
  }
  }
check(location?)
{

  this.loader = this.loadingCtrl.create({
    content: "",
  });
  this.loader.present();
  // check if location service available to use
  //let successCallback = (isAvailable) => {
    
  ////if(isAvailable == true)
  //{
    // if device is ios there is no need to check high accuracy
    if(this.plt.is('ios'))
    {
      // get current location
      console.log("3 - networkStatus : ", this.helper.netwrokStatus)
      this.getCurrentLocation();
    }
    else
    {
     // check if locaion mode is high
     //this.diagnostic.getLocationMode().then((status)=>{
     // if(!(status =="high_accuracy"))
     // {
        // if mode is not high
      //   this.loader.dismiss();
      //   const toast = this.toastCtrl.create({
      //     message: this.translate.instant('error'),
      //     duration: 3000
      //   });
      //   toast.present();
      // }
      // else{
       // if it is high
        if(this.platform.is('ios')){
        this.getCurrentLocation();
        }
        else{
         // if(!this.helper.mockLocation){
          this.ApiservicesProvider.insertFingerPrint(
            location.latitude, location.longitude, this.helper.Empid, this.platform.isRTL ? 'rtl' : 'ltr'
            , (data) => this.successCallBackLocation(data), (data) => this.failureCallBackLocation(data));
      //  }
      //  else{
          //this.helper.presentToast(this.translate.instant('tryAgain'))
       // }
      //}
    //   }
    
    // })
  }
   }
  //  else{
  //    // if location is't available
  //   let alert = this.alertCtrl.create({
  //     message: this.translate.instant('gpsmsg'),
  //     buttons: [
  //       {
  //         text: this.translate.instant('Cancel'),
  //         role: 'cancel',
  //         handler: () => {
  //           this. loader.dismiss();
  //         }
  //       },
  //       {
  //         text: this.translate.instant('open'),
  //         handler: () => {
  //           // go to device setings to open location
  //           this.diagnostic.switchToLocationSettings()
  //           this. loader.dismiss();
  //         }
  //       }
  //     ]
  //   });
  //   alert.present();
  //  }
  // };
  //let errorCallback = (e) => {
   // this. loader.dismiss();
  //  };   
  // check if location available
   // this.diagnostic.isLocationAvailable().then(successCallback).catch(errorCallback);

}

checkforoffline(location?)
{
  console.log("checkforoffline")

  // this.loader = this.loadingCtrl.create({
  //   content: "",
  // });
  // this.loader.present();

  // check if location service available to use
  //let successCallback = (isAvailable) => {
    
  ////if(isAvailable == true)
  //{
    // if device is ios there is no need to check high accuracy
    if(this.plt.is('ios'))
    {
      // get current location
      console.log("4 - networkStatus : ", this.helper.netwrokStatus)
      this.getCurrentLocation();
    }
    else
    {
     // check if locaion mode is high
     //this.diagnostic.getLocationMode().then((status)=>{
     // if(!(status =="high_accuracy"))
     // {
        // if mode is not high
      //   this.loader.dismiss();
      //   const toast = this.toastCtrl.create({
      //     message: this.translate.instant('error'),
      //     duration: 3000
      //   });
      //   toast.present();
      // }
      // else{
       // if it is high
        if(this.platform.is('ios')){
        this.getCurrentLocation();
        }
        else{
         // if(!this.helper.mockLocation){
console.log("store data in storage")
         //store data in storage

        
         if (this.helper.netwrokStatus == 1){
        
          this.ApiservicesProvider.insertFingerPrint(
            location.latitude, location.longitude, this.helper.Empid, this.platform.isRTL ? 'rtl' : 'ltr'
            , (data) => this.successCallBackLocation(data), (data) => this.failureCallBackLocation(data));

        }else{



          this.storage.get('AllowOfflineFinger').then((val) => {
            if (val != null) {
            
              if(val == 0){

                let alertConfirm = this.alertCtrl.create({
                  message: this.translate.instant('offlineFingerPrintNotAvailable'),
                  buttons: [
                    {
                      text: this.translate.instant('Ok33'),
                      role: 'cancel',
                      handler: () => {
                        console.log('Cancel clicked');
                      }
                    },
                    // {
                    //   text: this.translate.instant('Ok'),
                    //   handler: () => {
                    //       this.check(location)
                    //   }
            
                    // }
                  ]
                });
                alertConfirm.present();
      
                
                if (this.loader)
                 this.loader.dismiss();



              }else if (val == 1){




          this.storage.set("ERPOfflineData", {lat:location.latitude,long:location.longitude,empID:this.helper.Empid,dir:this.platform.isRTL ? 'rtl' : 'ltr',FingerDate:this.datepipe.transform(new Date().toISOString(), 'MM/dd/yyyy H:mm')});

          // this.helper.presentToast(this.translate.instant('DataSavedSuccessfully'))
          let alertConfirm = this.alertCtrl.create({
            message: this.translate.instant('offlineDataSavedSuccessfully1'),
            buttons: [
              {
                text: this.translate.instant('Ok33'),
                role: 'cancel',
                handler: () => {
                  console.log('Cancel clicked');
                }
              },
              // {
              //   text: this.translate.instant('Ok'),
              //   handler: () => {
              //       this.check(location)
              //   }
      
              // }
            ]
          });
          alertConfirm.present();


              }else{

                let alertConfirm = this.alertCtrl.create({
                  message: this.translate.instant('offlineFingerPrintNotAvailable'),
                  buttons: [
                    {
                      text: this.translate.instant('Ok33'),
                      role: 'cancel',
                      handler: () => {
                        console.log('Cancel clicked');
                      }
                    },
                    // {
                    //   text: this.translate.instant('Ok'),
                    //   handler: () => {
                    //       this.check(location)
                    //   }
            
                    // }
                  ]
                });
                alertConfirm.present();
      
                
                if (this.loader)
                 this.loader.dismiss();


              }
            
            }else{

              console.log("offline finger print  == null")

              let alertConfirm = this.alertCtrl.create({
                message: this.translate.instant('offlineFingerPrintNotAvailable'),
                buttons: [
                  {
                    text: this.translate.instant('Ok33'),
                    role: 'cancel',
                    handler: () => {
                      console.log('Cancel clicked');
                    }
                  },
                  // {
                  //   text: this.translate.instant('Ok'),
                  //   handler: () => {
                  //       this.check(location)
                  //   }
          
                  // }
                ]
              });
              alertConfirm.present();
    
              
              if (this.loader)
               this.loader.dismiss();

               
            }





            });




        }
        
       
          // this.ApiservicesProvider.insertFingerPrint(
          //   location.latitude, location.longitude, this.helper.Empid, this.platform.isRTL ? 'rtl' : 'ltr'
          //   , (data) => this.successCallBackLocationForoffline(data), (data) => this.failureCallBackLocationForoffline(data));
      
      
      
            //  }
      //  else{
          //this.helper.presentToast(this.translate.instant('tryAgain'))
       // }
      //}
    //   }
    
    // })
  }
   }
  //  else{
  //    // if location is't available
  //   let alert = this.alertCtrl.create({
  //     message: this.translate.instant('gpsmsg'),
  //     buttons: [
  //       {
  //         text: this.translate.instant('Cancel'),
  //         role: 'cancel',
  //         handler: () => {
  //           this. loader.dismiss();
  //         }
  //       },
  //       {
  //         text: this.translate.instant('open'),
  //         handler: () => {
  //           // go to device setings to open location
  //           this.diagnostic.switchToLocationSettings()
  //           this. loader.dismiss();
  //         }
  //       }
  //     ]
  //   });
  //   alert.present();
  //  }
  // };
  //let errorCallback = (e) => {
   // this. loader.dismiss();
  //  };   
  // check if location available
   // this.diagnostic.isLocationAvailable().then(successCallback).catch(errorCallback);

}
  opennotificaion() {
    this.navCtrl.push(NotificaionPage)
  }
  openmenu() {
    this.menu.open()
  }
  successCallback(result) {
    console.log("mock location sucess",result);
    console.log(result); // true - enabled, false - disabled
    if (result == true) {
      console.log("true result mock ")
      const toast = this.toastCtrl.create({
        message: 'Please, Stop Mock Location',
        duration: 3000
      });
      toast.present();
    } else {
      console.log("false result mock ")
      this.getCurrentLocation();
    }
  }

  errorCallback(error) {
    console.log(JSON.stringify(error));
  }
 //get current location 
 xshowAlert
 getCurrentLocationforOffline(showAlert){
 this.xshowAlert = showAlert
  // if (this.helper.netwrokStatus == 1){

    let optionsLoc = { timeout: 20000, enableHighAccuracy: true, maximumAge: 3600 };
    this.geolocation.getCurrentPosition(optionsLoc).then((res) => {
     
      this.lat = res.coords.latitude;
      this.long = res.coords.longitude;
   console.log("lat location : ",this.lat," lng location :",this.long)
  
  
  
      let location = 'lat ' + res.coords.latitude + ' lang ' + res.coords.longitude;
      this.handleAndroidAttendenceforOffline(res.coords,this.xshowAlert)
      // this.ApiservicesProvider.insertFingerPrint(
      //   this.lat, this.long, this.helper.Empid, this.platform.isRTL ? 'rtl' : 'ltr'
      //   , (data) => this.successCallBackLocationForoffline(data), (data) => this.failureCallBackLocationForoffline(data));
  
    }).catch((error) => {
     console.log('Error getting location'+ error);
      this.loader.dismiss()
      const toast = this.toastCtrl.create({
        message: 'Please, Enable your GPS Location',
        duration: 3000
      });
      toast.present();
      return;
    });
  
  

//   }else{

//     let alertConfirm = this.alertCtrl.create({
//       message: this.translate.instant('offlineDataSavedSuccessfully'),
//       buttons: [
//         {
//           text: this.translate.instant('Cancel'),
//           role: 'cancel',
//           handler: () => {
//             console.log('Cancel clicked');
//           }
//         },
//         {
//           text: this.translate.instant('Ok'),
//           handler: () => {
              
//   let optionsLoc = { timeout: 20000, enableHighAccuracy: true, maximumAge: 3600 };
//   this.geolocation.getCurrentPosition(optionsLoc).then((res) => {
   
//     this.lat = res.coords.latitude;
//     this.long = res.coords.longitude;
//  console.log("lat location : ",this.lat," lng location :",this.long)



//     let location = 'lat ' + res.coords.latitude + ' lang ' + res.coords.longitude;
//     this.handleAndroidAttendenceforOffline(res.coords)
//     // this.ApiservicesProvider.insertFingerPrint(
//     //   this.lat, this.long, this.helper.Empid, this.platform.isRTL ? 'rtl' : 'ltr'
//     //   , (data) => this.successCallBackLocationForoffline(data), (data) => this.failureCallBackLocationForoffline(data));

//   }).catch((error) => {
//    console.log('Error getting location'+ error);
//     this.loader.dismiss()
//     const toast = this.toastCtrl.create({
//       message: 'Please, Enable your GPS Location',
//       duration: 3000
//     });
//     toast.present();
//     return;
//   });


//           }

//         }
//       ]
//     });
//     alertConfirm.present();
  
//   }




 }
 
 
 getCurrentLocation() {
    console.log("5 - networkStatus : ", this.helper.netwrokStatus)

//     if (this.helper.netwrokStatus == 1){

//       let alertConfirm = this.alertCtrl.create({
//         message: this.translate.instant('offlineDataSavedSuccessfully'),
//         buttons: [
//           {
//             text: this.translate.instant('Cancel'),
//             role: 'cancel',
//             handler: () => {
//               console.log('Cancel clicked');
//             }
//           },
//           {
//             text: this.translate.instant('Ok'),
//             handler: () => {
                

//     let optionsLoc = { timeout: 20000, enableHighAccuracy: true, maximumAge: 3600 };
//     this.geolocation.getCurrentPosition(optionsLoc).then((res) => {
     
//       this.lat = res.coords.latitude;
//       this.long = res.coords.longitude;
//    console.log("lat location : ",this.lat," lng location :",this.long)



//       let location = 'lat ' + res.coords.latitude + ' lang ' + res.coords.longitude;
//       if (this.helper.netwrokStatus == 1){
//         console.log("network status 1")
//         this.ApiservicesProvider.insertFingerPrint(
//           this.helper.background_lat, this.helper.background_lng, this.helper.Empid, this.platform.isRTL ? 'rtl' : 'ltr'
//           , (data) => this.successCallBackLocation(data), (data) => this.failureCallBackLocation(data));
  
//       }else{
//         console.log("network status 0")
//         this.storage.set("ERPOfflineData", {lat:this.helper.background_lat,long:this.helper.background_lng,empID:this.helper.Empid,dir:this.platform.isRTL ? 'rtl' : 'ltr',FingerDate:this.datepipe.transform(new Date().toISOString(), 'MM/dd/yyyy h:mm')});

//         this.helper.presentToast(this.translate.instant('DataSavedSuccessfully'))
        
//         if (this.loader)
//          this.loader.dismiss();
 
//       }
      
//     }).catch((error) => {
//      console.log('Error getting location'+ error);
//       this.loader.dismiss()
//       const toast = this.toastCtrl.create({
//         message: 'Please, Enable your GPS Location',
//         duration: 3000
//       });
//       toast.present();
//       return;
//     });
    
//             }
  
//           }
//         ]
//       });
//       alertConfirm.present();

//       //

// /////
//     }else{

      let optionsLoc = { timeout: 20000, enableHighAccuracy: true, maximumAge: 3600 };
      this.geolocation.getCurrentPosition(optionsLoc).then((res) => {
       
        this.lat = res.coords.latitude;
        this.long = res.coords.longitude;
     console.log(" ios lat location : ",this.lat,"  ,, lng location :",this.long)
  
     if(!res.coords.latitude ){
      this.helper.presentToast(this.translate.instant('tryAgain'))
      return;
    }

    if(res.coords.latitude <= 0){
      this.helper.presentToast(this.translate.instant('tryAgain'))
      return;
    }
  
        let location = 'lat ' + res.coords.latitude + ' lang ' + res.coords.longitude;
        if (this.helper.netwrokStatus == 1){
          console.log("network status 1")
          // this.ApiservicesProvider.insertFingerPrint(
          //   this.helper.background_lat, this.helper.background_lng, this.helper.Empid, this.platform.isRTL ? 'rtl' : 'ltr'
          //   , (data) => this.successCallBackLocation(data), (data) => this.failureCallBackLocation(data));
    
          this.ApiservicesProvider.insertFingerPrint(
            this.lat, this.long, this.helper.Empid, this.platform.isRTL ? 'rtl' : 'ltr'
            , (data) => this.successCallBackLocation(data), (data) => this.failureCallBackLocation(data));

        }else{
          console.log("network status 0")
          // this.storage.set("ERPOfflineData", {lat:this.helper.background_lat,long:this.helper.background_lng,empID:this.helper.Empid,dir:this.platform.isRTL ? 'rtl' : 'ltr',FingerDate:this.datepipe.transform(new Date().toISOString(), 'MM/dd/yyyy h:mm')});
          // MM/dd/yyyy h:mm


          this.storage.get('AllowOfflineFinger').then((val) => {
            if (val != null) {
            
              if(val == 0){

                let alertConfirm = this.alertCtrl.create({
                  message: this.translate.instant('offlineFingerPrintNotAvailable'),
                  buttons: [
                    {
                      text: this.translate.instant('Ok33'),
                      role: 'cancel',
                      handler: () => {
                        console.log('Cancel clicked');
                      }
                    },
                    // {
                    //   text: this.translate.instant('Ok'),
                    //   handler: () => {
                    //       this.check(location)
                    //   }
            
                    // }
                  ]
                });
                alertConfirm.present();
      
                
                if (this.loader)
                 this.loader.dismiss();



              }else if (val == 1){

                this.storage.set("ERPOfflineData", {lat:this.lat,long:this.long,empID:this.helper.Empid,dir:this.platform.isRTL ? 'rtl' : 'ltr',FingerDate:this.datepipe.transform(new Date().toISOString(), 'MM/dd/yyyy H:mm')});
  
                // this.helper.presentToast(this.translate.instant('DataSavedSuccessfully'))
                let alertConfirm = this.alertCtrl.create({
                  message: this.translate.instant('offlineDataSavedSuccessfully1'),
                  buttons: [
                    {
                      text: this.translate.instant('Ok33'),
                      role: 'cancel',
                      handler: () => {
                        console.log('Cancel clicked');
                      }
                    },
                    // {
                    //   text: this.translate.instant('Ok'),
                    //   handler: () => {
                    //       this.check(location)
                    //   }
            
                    // }
                  ]
                });
                alertConfirm.present();
      
                
                if (this.loader)
                 this.loader.dismiss();

                 

              }else{

              
                
                let alertConfirm = this.alertCtrl.create({
                  message: this.translate.instant('offlineFingerPrintNotAvailable'),
                  buttons: [
                    {
                      text: this.translate.instant('Ok33'),
                      role: 'cancel',
                      handler: () => {
                        console.log('Cancel clicked');
                      }
                    },
                    // {
                    //   text: this.translate.instant('Ok'),
                    //   handler: () => {
                    //       this.check(location)
                    //   }
            
                    // }
                  ]
                });
                alertConfirm.present();
      
                
                if (this.loader)
                 this.loader.dismiss();


              }



            }else{
              console.log("offline finger print  == null")


              let alertConfirm = this.alertCtrl.create({
                message: this.translate.instant('offlineFingerPrintNotAvailable'),
                buttons: [
                  {
                    text: this.translate.instant('Ok33'),
                    role: 'cancel',
                    handler: () => {
                      console.log('Cancel clicked');
                    }
                  },
                  // {
                  //   text: this.translate.instant('Ok'),
                  //   handler: () => {
                  //       this.check(location)
                  //   }
          
                  // }
                ]
              });
              alertConfirm.present();
    
              
              if (this.loader)
               this.loader.dismiss();



            }

          });


      
        }
        
      }).catch((error) => {
       console.log('Error getting location'+ error);
        this.loader.dismiss()
        const toast = this.toastCtrl.create({
          message: 'Please, Enable your GPS Location',
          duration: 3000
        });
        toast.present();
        return;
      });
      
    // }




  }

  successCallBackLocationForoffline(data){
    console.log("successCallBackLocationForoffline sucess finger print");
    console.log(data);
   
    if (JSON.parse(data).Result == true) {
      this.storage.remove("ERPOfflineData").then(() => {
        console.log("remove ERPOfflineData from storage")
      });
      const toast = this.toastCtrl.create({
        message: this.translate.instant('DataSavedSuccessfully'),
        duration: 3000
      });
      toast.present();
      this.show=false
      this.hide=true
      //=====show Arrive and Leave Time =======
      if(this.loader)
      this.loader.dismiss();
      this.getLeaveArriveTimeForOffline();

    }
    else {
      this.loader.dismiss();
      const toast = this.toastCtrl.create({
        message: JSON.parse(data).ErrorMessage,
        duration: 3000
      });
      toast.present();

    }

    
  }


  successCallBackLocation(data) {
    console.log("sucess finger print");
    console.log(data);
   
    if (JSON.parse(data).Result == true) {
      const toast = this.toastCtrl.create({
        message: this.translate.instant('DataSavedSuccessfully'),
        duration: 3000
      });
      toast.present();
      this.show=false
      this.hide=true
      //=====show Arrive and Leave Time =======
      if (this.loader)
      this.loader.dismiss();
      this.getLeaveArriveTime();

    }
    else {
      this.loader.dismiss();
      const toast = this.toastCtrl.create({
        message: JSON.parse(data).ErrorMessage,
        duration: 3000
      });
      toast.present();

    }

  }

  failureCallBackLocationForoffline(data){
    this.loader.dismiss();

    if (JSON.stringify(data.status) == '401') {
      // UnAuthorized
      this.event.publish("login","");
    }
  }
  failureCallBackLocation(data) {
    this.loader.dismiss();

    if (JSON.stringify(data.status) == '401') {
      // UnAuthorized
      this.event.publish("login","");
    }

  }

  // ====================== get Arrive and Leave Time ============================

  getLeaveArriveTime() {
    this.storage.get('EmpId').then((val) => {
      this.ApiservicesProvider.getCheckInOut(
        val
        , this.myDate
        , this.platform.isRTL ? 'rtl' : 'ltr'//  this.helper.lang_direction
        , (data) => this.successCallBackCheck(data)
        , (data) => this.failureCallBackcheck(data));

    })
  }

  getLeaveArriveTimeForOffline() {
   
    this.storage.get('EmpId').then((val) => {
      this.ApiservicesProvider.getCheckInOut(
        val
        , this.datepipe.transform(new Date().toISOString(), 'MM/dd/yyyy')
        , this.platform.isRTL ? 'rtl' : 'ltr'//  this.helper.lang_direction
        , (data) => this.successCallBackCheckforoffline(data)
        , (data) => this.failureCallBackcheckforoffline(data));

    })
  }


  successCallBackCheckforoffline(data) {
    
    //alert("offline sucess Check In Out");
    
    this.show=false
    this.hide=true
    if(!data){
      if (this.loader)
      this.loader.dismiss();
      return;
    }
    if(JSON.stringify(data)=="[]")
    {
      if (this.loader)
      this.loader.dismiss();

    }

    this.AttendTime=JSON.parse(data)[0].AttendTime
    this.LeaveTime=JSON.parse(data)[0].LeaveTime
    this.attendtime= this.AttendTime.split('T')[1].split('.')[0]
    this.attenddate=this.AttendTime.split('T')[0]
    this.leavetime= this.LeaveTime.split('T')[1].split('.')[0]
    this.leavedate=this.LeaveTime.split('T')[0]
    this.loader.dismiss();

  }


  successCallBackCheck(data) {
   
    console.log("sucess Check In Out");
    
    this.show=false
    this.hide=true
    if(!data){
      this.loader.dismiss();
      return;
    }
    if(JSON.stringify(data)=="[]")
    {
      this.loader.dismiss();

    }
    this.AttendTime=JSON.parse(data)[0].AttendTime
    this.LeaveTime=JSON.parse(data)[0].LeaveTime
    this.attendtime= this.AttendTime.split('T')[1].split('.')[0]
    this.attenddate=this.AttendTime.split('T')[0]
    this.leavetime= this.LeaveTime.split('T')[1].split('.')[0]
    this.leavedate=this.LeaveTime.split('T')[0]
    if (this.loader)           
    this.loader.dismiss();

    this.storage.set("storageTime",{"attendTime":this.AttendTime,"leaveTime":this.LeaveTime})
    console.log("storage time savd")
  }

  failureCallBackcheck(data) {
    
    console.log("failCheck");
    console.log(data);
    if(this.loader)
    this.loader.dismiss();
    if (JSON.stringify(data.status) == '401') {
      // UnAuthorized
      this.event.publish("login","");
    }
    this.storage.get("storageTime").then(val=>{
     // alert("storageTime get")
      console.log("get   storageTimestorageTimestorageTimestorageTime time val ",val)
      if (val != null){
console.log("val !== null q")



this.AttendTime=val.attendTime
this.LeaveTime=val.leaveTime
this.attendtime= this.AttendTime.split('T')[1].split('.')[0]
this.attenddate=this.AttendTime.split('T')[0]
this.leavetime= this.LeaveTime.split('T')[1].split('.')[0]
this.leavedate=this.LeaveTime.split('T')[0]

//this.storage.set("storageTime",{"attendTime":this.AttendTime,"leaveTime":this.LeaveTime})
      }else{
console.log("val  == null else")
      }
    })

  }

  failureCallBackcheckforoffline(data) {
    
    console.log("failCheck");
    console.log(data);
    this. loader.dismiss();
    if (JSON.stringify(data.status) == '401') {
      // UnAuthorized
      this.event.publish("login","");
    }

  }

  doRefresh(ev)
  { 
    
    this.ApiservicesProvider.getFingerCred((data) => {
      console.log("refresh resp from getFingerCred : ",data)
      var jsonObject = data//JSON.parse(data);
      this.storage.set("AllowSensor", jsonObject.AllowSensor); //0 fingerprint not necessary , 1 fingerprint necessary
      this.storage.set("AllowOfflineFinger", jsonObject.AllowOfflineFIngerPrint); //0 offlinefingerprint not available , 1 offlinefingerprint available
      
      if (jsonObject.IsRemoved == 1){
        console.log("user logout from app")
        this.helper.presentToast(this.translate.instant("logoutFromAdmin"))
        this.navCtrl.setRoot(LoginPage)
        // this.menu.close()
        this.storage.remove("access_token");
        this.storage.remove("user_info");
        this.storage.remove("EmpId");
        this.storage.remove("UserPass");
        this.storage.remove("IsAdmin");
        this.event.publish("direction",this.langDirection);

      }else{
console.log("user staty in app")
      }

    }, (data) => {
      console.log("refresh error from getFingerCred : ",data)
    });
  

    
    this.storage.get('EmpId').then((val) => {
    this.ApiservicesProvider.getCheckInOut(
      val
      , this.myDate
      , this.platform.isRTL ? 'rtl' : 'ltr'
      , (data) => this.successCallBackCheck(data)
      , (data) => this.failureCallBackcheck(data));

  })
    ev.complete();

  }
}
