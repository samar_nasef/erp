import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Platform, ToastController, PopoverController  } from 'ionic-angular';
import { ApiservicesProvider } from '../../providers/apiservices/apiservices';
import { HelperProvider } from '../../providers/helper/helper';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';
import { EmployeesListPage } from '../employees-list/employees-list';

@Component({
  selector: 'page-replay',
  templateUrl: 'replay.html',
})
export class ReplayPage {
id:any
subject:any
msg:any
emps:any = []
emplyees:any=[]
langDirection:any
name:any
empid:any
empsName=""
  constructor(public toastCtrl:ToastController,public storage:Storage,public helper:HelperProvider,
    public translate:TranslateService,public platform:Platform,public provider:ApiservicesProvider,
    public ViewCtrl:ViewController,public navCtrl: NavController, public navParams: NavParams,
    public popoverCtrl: PopoverController) {
   this.id= this.navParams.get("id")
   this.name= this.navParams.get("name")

  }

  ionViewDidLoad() {
    if (this.helper.currentLang == 'ar')
    {
      this.translate.use('ar');
      this.helper.currentLang = 'ar';
      this.translate.setDefaultLang('ar');
      this.helper.lang_direction = 'rtl';
      this.langDirection = "rtl";
      this.platform.setDir('rtl',true)
    }
    else{
     
      this.translate.use('en');
      this.helper.currentLang = 'en';
      this.translate.setDefaultLang('en');
      this.helper.lang_direction = 'ltr';
      this.langDirection = "ltr";
      this.platform.setDir('ltr',true)
    }
    // this.provider.Allemps(1,this.platform.isRTL ? 'rtl' : 'ltr',"",(data)=>{
    //  console.log(JSON.stringify(data))
    //  this.emplyees=data
    //  this.emplyees.forEach(element => {
    //    if(element.Text==this.name)
    //    {
         
    //      this.empid=element.Value
    //      console.log(this.empid)
    //      this.emps=element.Value
    //    }
    //  });
    // },(data)=>{
    //   console.log(JSON.stringify(data))
    // })
  }
  geemps()
  {
    if(!(this.emps.includes(this.empid)))
    {
      this.emps.push(this.empid)
    }
  }

  openEmployees(){
    let popover = this.popoverCtrl.create(EmployeesListPage,{},{cssClass:"invitePopover"});
    popover.present({
    });
    popover.onDidDismiss(data => {
      console.log(data);
      if (data != null) {
        let EmpNameList = []
        let selectedEmps = JSON.parse(data)
        console.log(data)
        selectedEmps.forEach(element => {
          this.emps.push(element.id)
          EmpNameList.push(element.name)
          this.empsName = EmpNameList.toString()
        });
        // this.selecteHours = data
        //this.selecedJS = data
       
      }
      //console.log("this.selecedJS: ",this.selecedJS);
    })
  }
  // send replay for a specific msg
  send()
  {
    if(!(this.msg==null || this.emps.length == 0 || this.emps==[] || this.msg==""))
    {
    console.log(this.emps)
    console.log(this.msg)
   
    this.storage.get("EmpId").then((val) => {
      if (val) {
        this.provider.replay(val,this.msg,this.emps,this.id,(data)=>{
        this.msg=""
        this.emps=[]
        this.empsName = ""
        this.subject=""
        let toast = this.toastCtrl.create({
          message: this.translate.instant('saved'),
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
        },(data)=>{

        })
      }
    })
  }
  else{
    let toast = this.toastCtrl.create({
      message: this.translate.instant('msgEnterUsername'),
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
   
  }
}
}
