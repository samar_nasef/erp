
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ViewController, MenuController ,Events, Platform} from 'ionic-angular';
import { RequestsPage } from '../requests/requests';
import { ApiservicesProvider } from '../../providers/apiservices/apiservices';
import { HelperProvider } from '../../providers/helper/helper';
import { Storage } from '@ionic/storage';
import { DatePicker } from '@ionic-native/date-picker';
import { TranslateService } from '@ngx-translate/core';
import { TabsPage } from '../tabs/tabs';
import { DatePipe } from '@angular/common';
import * as Moment from 'moment';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-over-time-req',
  templateUrl: 'over-time-req.html',
})
export class OverTimeReqPage {

  DATE: any=new Date().toISOString().split('T')[0];
  NOTE: any;
  langDirection:any
  PERMISSION: number = 0;
  senddate:any
  page:any
  selectdata:any
  date2:any=new Date()
  date1:any=new Date()
  date:any
  dis:any="false"
  isReadonly:any=true
  hide:any="true"
  constructor(public navCtrl: NavController, public navParams: NavParams
    , public ApiservicesProvider: ApiservicesProvider
    ,public event:Events,
    public platform:Platform,
     public helper: HelperProvider
    , private toastCtrl: ToastController
    , public storage: Storage
    , private datePicker: DatePicker
    , public translate: TranslateService
    , public navctrl:NavController,
    public datePipe:DatePipe,
    public ViewCtrl:ViewController,
    public menue:MenuController
  ) {
  
    this.selectdata=this.navParams.get("selectedDate")
    this.page=this.navParams.get("Page")
    if(this.page=="home")
    {

      this.date=this.datePipe.transform(this.selectdata, 'MM-dd-yyyy')
      this.DATE=this.date
        this.dis="true"
        this.hide="false"
    }
    if (this.helper.currentLang == 'ar')
    {
     this.translate.use('ar');
     this.helper.currentLang = 'ar';
     this.translate.setDefaultLang('ar');
     this.helper.lang_direction = 'rtl';
     this.langDirection = "rtl";
    }
    else{
     
     this.translate.use('en');
     this.helper.currentLang = 'en';
     this.translate.setDefaultLang('en');
     this.helper.lang_direction = 'ltr';
     this.langDirection = "ltr";
    }
    this.storage.get('EmpId').then((val) => {
   
      this.ApiservicesProvider.getOverTimeMinutes(
        val
        , this.datePipe.transform(this.DATE, 'MM/dd/yyyy')
        , (data) => this.successCallbackGetTime(data)
        , (data) => this.failureCallbackGetTime(data));
    })
  }
  openmenu()
  {
    this.menue.open()

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad OverTimeReqPage');
  }
  openreq() {
    this.navCtrl.setRoot(RequestsPage);
  }
  getdate()
  {
    let localLang 
    let nowTxt
    let okTxt 
    let cancelTxt
    var minmDate;
    var maxDate;
    if (this.platform.is('ios')) {
      
      minmDate =String(new Date()); 
      let d=new Date()
      let year =d.getFullYear()
      let month=d.getMonth()
      let day=d.getDay()
      maxDate = (new Date(year + 1,month,day)).valueOf();
    }
    else {
      minmDate =(new Date()).valueOf(); 
      let d=new Date()
      let year =d.getFullYear()
      let month=d.getMonth()
      let day=d.getDay()
      maxDate = (new Date(year + 1,month,day)).valueOf();
    } 
    if (this.helper.currentLang == 'ar') {
      localLang = 'ar_eg';
      nowTxt = 'اليوم';
      okTxt = 'تم';
      cancelTxt = 'إلغاء'
      this.datePicker.show({
        date: this.date1,
        mode: 'date',
        // minDate: minmDate,
        maxDate: maxDate,
        okText: okTxt,
        cancelText: cancelTxt,
        todayText: nowTxt,
        locale: localLang,
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
      }).then(
        date =>{ 
          this.date1=date
          this.DATE=date.toLocaleDateString('ar-EG')
      this.DATE=this.DATE.replace("/","-").replace("/","-")
      this.DATE=this.datePipe.transform(date, 'MM-dd-yyyy');
      this.storage.get('EmpId').then((val) => {
        this.ApiservicesProvider.getOverTimeMinutes(
          val
          , this.datePipe.transform(this.DATE, 'MM/dd/yyyy')
          , (data) => this.successCallbackGetTime(data)
          , (data) => this.failureCallbackGetTime(data));
      })
      
    },
        err => console.log('Error occurred while getting date: ', err)
      );
    }
    else{
      localLang = 'en_us';
      nowTxt = 'today';
      okTxt = 'okay';
      cancelTxt = 'cancel'
      this.datePicker.show({
        date: this.date1,
        mode: 'date',
        okText: okTxt,
        // minDate: minmDate,
        maxDate: maxDate,
        cancelText: cancelTxt,
        todayText: nowTxt,
        locale: localLang,
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
      }).then(
        date =>{ 
          this.date1=date
          this.DATE=date.toLocaleDateString('en-US')
      this.DATE=this.DATE.replace("/","-").replace("/","-")
      this.DATE=this.datePipe.transform(date, 'MM-dd-yyyy');
      this.storage.get('EmpId').then((val) => {
        this.ApiservicesProvider.getOverTimeMinutes(
          val
          , this.datePipe.transform(this.DATE, 'MM/dd/yyyy')
          , (data) => this.successCallbackGetTime(data)
          , (data) => this.failureCallbackGetTime(data));
      })
      
    },
        err => console.log('Error occurred while getting date: ', err)
      );
    }

  }

  //========================== function to save data =================================
  saveOvertimeReq() {

    if ( this.PERMISSION && this.DATE && this.NOTE) {
      if (this.PERMISSION > 0) {
        this.storage.get('EmpId').then((val) => {
          this.ApiservicesProvider.SaveEventOverTime(
            val
            , this.helper.lang_direction
            , this.helper.EventTypeOverTime
            , this.helper.statusOverTime
            , this.PERMISSION
            , this.datePipe.transform(this.DATE, 'MM/dd/yyyy')
            , this.datePipe.transform(this.DATE, 'MM/dd/yyyy')
            , this.NOTE
            , (data) => this.successCallbackOverTime(data)
            , (data) => this.failureCallbackOverTime(data))

        })
      } else {
        console.log("minutes must be graeter than zero ");
        this.DATE = " ";
        this.PERMISSION = 0;
        this.NOTE = " ";

      }
    } else {
      console.log("you must enter data ");
      const toast = this.toastCtrl.create({
        message: this.translate.instant('msgEnterUsername'),
        duration: 3000
      });
      toast.present();
    }
  }
  
  successCallbackOverTime(data) {
    console.log(" data saved succesfully OverTimePage");
    console.log(data);
    if (data.Result == true) {
      const toast = this.toastCtrl.create({
        message: this.translate.instant('DataSavedSuccessfully'),
        duration: 3000
      });
      toast.present();
      this.PERMISSION = 0;
      this.NOTE = "";
      this.ViewCtrl.dismiss()  
      }
    else {
      const toast = this.toastCtrl.create({
        message: data.ErrorMessage,
        duration: 3000
      });
      toast.present();
    }
  
  }
  failureCallbackOverTime(status) {

    console.log(status);
  }


  //======================== function to get minutes ============================

  getovertimeMinutes() {
    console.log(this.DATE);

    this.storage.get('EmpId').then((val) => {
      this.ApiservicesProvider.getOverTimeMinutes(
        val
        , this.datePipe.transform(this.DATE, 'MM/dd/yyyy')
        , (data) => this.successCallbackGetTime(data)
        , (data) => this.failureCallbackGetTime(data));
    })
  }
  successCallbackGetTime(data) {
    console.log("Here minutes function sucess callback");
    this.PERMISSION = data;
    console.log(data);
    if (parseInt(data) < 1) {
      const toast = this.toastCtrl.create({
        message: this.translate.instant('NoMinutesToRequest'),
        duration: 3000
      });
      toast.present();
    }

  }
  failureCallbackGetTime(data) {
    if (JSON.stringify(data.status) == '401') {
      // UnAuthorized
      this.event.publish("login","");
    }
    console.log("Here minutes function failure callback");
    console.log(status);
  }
  //======================== function to open calender =====================
  opencalender() {
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date => console.log('Got date: ', date),
      err => console.log('Error occurred while getting date: ', err)
    );
  }


}
