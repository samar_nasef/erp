import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, MenuController,Events, Platform } from 'ionic-angular';
import { RequestsPage } from '../requests/requests';
import { ApiservicesProvider } from '../../providers/apiservices/apiservices';
import { HelperProvider } from '../../providers/helper/helper';
import { ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { TabsPage } from '../tabs/tabs';
import { DatePipe } from '@angular/common';
import { DatePicker } from '@ionic-native/date-picker';

import * as Moment from 'moment';
import { LoginPage } from '../login/login';


@Component({
  selector: 'page-permissionreq',
  templateUrl: 'permissionreq.html',
})
export class PermissionreqPage {
  dayNames:any=[]
  DATE: any=new Date().toISOString().split('T')[0];
  PERMISSION: number ;
  NOTE: any;
  isReadonly:any=true
  date1:any="kk"
  Balance: any;
  date2:any=new Date()
  minutes:any
  Min: any;
  langDirection:any
  Max: any;
  senddate:any
  page:any
  dis:any="false"
  hide:any="true"
  date:any
  constructor(public navCtrl: NavController, public navParams: NavParams
    , public ApiservicesProvider: ApiservicesProvider
    , public helper: HelperProvider
    ,public event:Events,
    public datePicker:DatePicker,
    public datePipe:DatePipe
    , private toastCtrl: ToastController
    , public navctrl:NavController,
    public platform:Platform,
    public ViewCtrl:ViewController,
    public menue:MenuController
    , public storage: Storage, public translate: TranslateService) {
      this.senddate=this.navParams.get("selectedDate")
      this.page=this.navParams.get("Page")
     console.log("page : ,", this.page)
      if(this.page=="home")
      {
      
        if(this.minutes==null)
        {
          this.PERMISSION=0
        }
        else{
          this.PERMISSION=this.minutes
       

        }
        this.date=this.datePipe.transform(this.senddate, 'MM-dd-yyyy')
      this.DATE=this.date
        this.dis="true"
        this.hide="false"
      }
     
      if (this.helper.currentLang == 'ar')
    {
      this.translate.use('ar');
      this.helper.currentLang = 'ar';
      this.translate.setDefaultLang('ar');
      this.helper.lang_direction = 'rtl';
      this.langDirection = "rtl";
    }
    else{
     
      this.translate.use('en');
      this.helper.currentLang = 'en';
      this.translate.setDefaultLang('en');
      this.helper.lang_direction = 'ltr';
      this.langDirection = "ltr";
    }
      this.storage.get('EmpId').then((val) => {
               this.ApiservicesProvider.getPermissionBalance(
          val
          , this.helper.lang_direction
          , this.DATE
          , (data) => this.successCallbackBalance(data)
          , (data) => this.failureCallbackBalance(data))
  
      })
  } 


  ionViewDidLoad() {
    console.log('ionViewDidLoad PermissionreqPage');
  }


  //================== Get Balance , Min ,Max from Api   ====================
  getBalance() {
   
    this.DATE=this.DATE.split(',')[1]
  
    this.storage.get('EmpId').then((val) => {
      this.ApiservicesProvider.getPermissionBalance(
        val
        , this.helper.lang_direction
        , this.DATE
        , (data) => this.successCallbackBalance(data)
        , (data) => this.failureCallbackBalance(data))

    })
  }
  getdate()
  {
    console.log("get date clicked")
    
    let localLang 
    let nowTxt
    let okTxt 
    let cancelTxt 
    var minmDate;
    var maxDate;
    if (this.platform.is('ios')) {
      
      minmDate =String(new Date()); 
      let d=new Date()
      let year =d.getFullYear()
      let month=d.getMonth()
      let day=d.getDay()
      maxDate = (new Date(year + 1,month,day)).valueOf();
    }
    else {
      minmDate =(new Date()).valueOf(); 
      let d=new Date()
      let year =d.getFullYear()
      let month=d.getMonth()
      let day=d.getDay()
      maxDate = (new Date(year + 1,month,day)).valueOf();
    }
    if (this.helper.currentLang == 'ar') {
      localLang = 'ar_eg';
      nowTxt = 'اليوم';
      okTxt = 'تم';
      cancelTxt = 'إلغاء'
      this.datePicker.show({
        date: this.date2,
        mode: 'date',
        // minDate: minmDate,
        maxDate: maxDate,
        okText: okTxt,
        cancelText: cancelTxt,
        todayText: nowTxt,
        locale: localLang,
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
      }).then(
        date =>{ 
          this.date2=date
          this.DATE=date.toLocaleDateString('ar-EG')
      this.DATE=this.DATE.replace("/","-").replace("/","-")
      this.DATE=this.datePipe.transform(date, 'MM-dd-yyyy');
      this.storage.get('EmpId').then((val) => {
        this.ApiservicesProvider.getPermissionBalance(
          val
          , this.helper.lang_direction
          , this.DATE
          , (data) => this.successCallbackBalance(data)
          , (data) => this.failureCallbackBalance(data))
  
      })
      
    },
        err => console.log('Error occurred while getting date: ', err)
      );
    }
    else{
      localLang = 'en_us';
      nowTxt = 'today';
      okTxt = 'okay';
      cancelTxt = 'cancel'
      this.datePicker.show({
        date: this.date2,
        mode: 'date',
        okText: okTxt,
        // minDate: minmDate,
        maxDate: maxDate,
        cancelText: cancelTxt,
        todayText: nowTxt,
        locale: localLang,
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
      }).then(
        date =>{ 
          this.date2=date
          this.DATE=date.toLocaleDateString('en-US')
      this.DATE=this.DATE.replace("/","-").replace("/","-")
      this.DATE=this.datePipe.transform(date, 'MM-dd-yyyy');
      this.storage.get('EmpId').then((val) => {
        this.ApiservicesProvider.getPermissionBalance(
          val
          , this.helper.lang_direction
          , this.DATE
          , (data) => this.successCallbackBalance(data)
          , (data) => this.failureCallbackBalance(data))
  
      })
      
    },
        err => console.log('Error occurred while getting date: ', err)
      );
    }
   
  
  }
 
  successCallbackBalance(data) {
    console.log("get Balance function success Callback");
    console.log("data  ==>  ", data);
    var jsonObj = JSON.parse(data);
    if (jsonObj.Result == true) {
      console.log("Balance Equal to ==>", jsonObj.BalancePerMonth);
      this.Balance = jsonObj.BalancePerMonth;
      this.Min = jsonObj.MinBalance;
      this.Max = jsonObj.MaxBalance;
     this.minutes=jsonObj.EmpDlays
   
    //  if(this.page=="home")
    //   {
      
        if(this.minutes==null)
        {
          this.PERMISSION=0
        }
        else{
          this.PERMISSION=this.minutes
          

        }
      
      // }

    } else {

      const toast = this.toastCtrl.create({
        message: JSON.parse(data).Message,
        duration: 3000
      });
      toast.present();
      this.Balance = " ";
      this.Max = " ";
      this.Min = " ";
      this.DATE = " ";
      this.PERMISSION = 0;
      this.NOTE = " ";
    }

  }


  failureCallbackBalance(err) {
    if (JSON.stringify(err.status) == '401') {
      // UnAuthorized
      this.event.publish("login","");
    }
    console.log("fail in balance");
    console.log(status);
  }


  openreq() {
    this.navCtrl.setRoot(RequestsPage);
  }

  //======================== save Event ========================
  openmenu()
  {
    this.menue.open()

  }
  savePermissionReq() {
    if (this.PERMISSION && this.DATE) {
      if (!this.Min && !this.Max && !this.Balance) {
        const toast = this.toastCtrl.create({
          message: this.translate.instant('NoBalance'),
          duration: 3000
        });
        toast.present();
        return;
      }
      if (this.PERMISSION < this.Min) {
        console.log("Please , Review your Balance !");
        const toast = this.toastCtrl.create({
          message: this.translate.instant('minmsg'),
          duration: 3000
        });
        toast.present();
        return;
      }
      else if (this.PERMISSION > this.Max) {
        console.log("Please , Review your Balance !");
        const toast = this.toastCtrl.create({
          message: this.translate.instant('maxmsg'),
          duration: 3000
        });
        toast.present();
        return;
      }
      if (this.PERMISSION > 0) {
        this.storage.get('EmpId').then((val) => {
          this.ApiservicesProvider.SavePermission(

            val
            , this.helper.lang_direction
            , this.Balance
            , this.Min
            , this.Max
            , this.PERMISSION
            , this.helper.EventTypePermission
            , this.helper.DayStatusPermission
            , this.DATE
            , this.DATE
            , (data) => this.successCallbackSave(data)
            , (data) => this.failureCallbackSave(data))

        })
      } else {
        console.log("minutes must be graeter than zero ");
        const toast = this.toastCtrl.create({
          message:  this.translate.instant('sorry'),
          duration: 3000
        });
        toast.present();
      }
    } else {
      console.log("you must enter data ");
      const toast = this.toastCtrl.create({
        message: this.translate.instant('msgEnterUsername'),
        duration: 3000
      });
      toast.present();
    }

  }
  successCallbackSave(data) {
    console.log("saved succesfully");
    console.log(data);
    console.log(data.Result);

    if (JSON.parse(data).Result == true) {
      const toast = this.toastCtrl.create({
        message: this.translate.instant('DataSavedSuccessfully'),
        duration: 3000
      });
      toast.present();
      this.Balance = " ";
      this.Max = " ";
      this.Min = " ";
      this.DATE = " ";
      this.PERMISSION = 0;
      this.NOTE = " ";
      this.ViewCtrl.dismiss()
    }
    else {
      const toast = this.toastCtrl.create({
        message: JSON.parse(data).ErrorMessage,
        duration: 3000
      });
      toast.present();
    }
  }
  failureCallbackSave(err) {
    if (JSON.stringify(err.status) == '401') {
      // UnAuthorized
      this.event.publish("login","");
    }
    console.log(status);

  }

}
