import { Component, ViewChild ,OnInit} from '@angular/core';
import { IonicPage, NavController, NavParams, Platform,PopoverController, ToastController, ViewController, LoadingController, Keyboard } from 'ionic-angular';
import { ApiservicesProvider } from '../../providers/apiservices/apiservices';
import { CKEditorModule } from 'ng2-ckeditor';
import { Storage } from '@ionic/storage/dist/storage';
import { TranslateService } from '@ngx-translate/core';
import { HelperProvider } from '../../providers/helper/helper';
import { FileChooser } from '@ionic-native/file-chooser';
import { File } from '@ionic-native/file';
import { IOSFilePicker } from '@ionic-native/file-picker';
import { FilePath } from '@ionic-native/file-path';
import { Base64 } from '@ionic-native/base64';
import { FileTransfer, FileTransferObject, FileUploadOptions } from '@ionic-native/file-transfer';
import { EmployeesListPage } from '../employees-list/employees-list';

@Component({
  selector: 'page-addmessage',
  templateUrl: 'addmessage.html',
})
export class AddmessagePage {

    to:any=0
    msg:any
    msgto:any=[]
    temp:any
    emplyees:any=[]
    editorValue:any
    show:any="false"
   Departments:any=[]
   cvdata:any
   names:any=[]
   units:any=[]
   templatebody:any
   ckeditorContent:any
   templates:any=[]
   filename:any
   hide:any="false"
   hide1:any="false"
   nativepath:any
   quantity:any
   emplyoees:any=[]
   name:any
   unit:any
   remember:any=false
   fileExt:any
   subject:any=""
   langDirection:any
   cvs:any=[]
   ckEditorConfig:any
   templatesm:any=[]
   hide2:any="false"
   dpm:any=[]
   loader:any
   empsName = ""
   fileTransfer: FileTransferObject
  constructor(public keyboard:Keyboard,public loadingCtrl:LoadingController,public transfer:FileTransfer,public popoverCtrl: PopoverController,public ViewCtrl:ViewController,public toastCtrl:ToastController,public base64:Base64,public filepath:FilePath,public filepicker:IOSFilePicker,public fileChooser:FileChooser,public file:File,public plt :Platform,public helper:HelperProvider,public translate:TranslateService,public storage:Storage,public platform:Platform,public provider:ApiservicesProvider,public navCtrl: NavController, public navParams: NavParams) {
    //this.ckEditorConfig.extraPlugins = 'sourcedialog';
    //this.ckEditorConfig.removePlugins = 'sourcearea';
  }
  ngOnInit() {
    this.keyboard.hideFormAccessoryBar(false)
    this.ckEditorConfig = {
      allowedContent: false,
      extraPlugins: 'divarea',
      forcePasteAsPlainText: true,
      removeButtons:'Cut,Copy,Paste,Undo,Redo,Anchor,Styles,Source,Save,RadioButton,TextField,Textarea,Selectionfield,Find,Templates,Print,Preview,Replace,SelectAll,Spellchecker,Setlanguage,Link,Image,Flash,Table,Iframe,CopyFormatting,RemoveFormat,Smiley,ImageButton,Superscript,Subscript,Button,HiddenField,PasteFromWord,Pasteasplaintext,Form,ShowBlocks,Maximize'    };
  }
  onBlur(event)
  {
    this.keyboard.close()
  }
  disableReturnButton(event)
  {
    if(event.which == 13){
      event.target.blur();
      return false;
    }
  }
  onKeydown(event) {

    if (event.key === "Enter") {
      console.log(event)
    }
  }
  ionViewDidLoad() {
    // set language of page
    if (this.helper.currentLang == 'ar')
    {
      this.translate.use('ar');
      this.helper.currentLang = 'ar';
      this.translate.setDefaultLang('ar');
      this.helper.lang_direction = 'rtl';
      this.langDirection = "rtl";
      this.platform.setDir('rtl',true)
    }
    else{

      this.translate.use('en');
      this.helper.currentLang = 'en';
      this.translate.setDefaultLang('en');
      this.helper.lang_direction = 'ltr';
      this.langDirection = "ltr";
      this.platform.setDir('ltr',true)
    }
    // get all employees
    // this.provider.Allemps(1,this.platform.isRTL ? 'rtl' : 'ltr',"",(data)=>{
    //   console.log(JSON.stringify(data))
    //   this.emplyoees=data
    //  },(data)=>{
    //    console.log(JSON.stringify(data))
    //  })
    this.provider.addmessage(this.platform.isRTL ? 'rtl' : 'ltr',(data)=>{
      this.emplyees=data
      console.log(JSON.stringify(data))
    },(data)=>{

    }) 
 }

 openEmployees(){
  let popover = this.popoverCtrl.create(EmployeesListPage,{},{cssClass:"invitePopover"});
  popover.present({
  });
  popover.onDidDismiss(data => {
    console.log(data);
    if (data != null) {
      let EmpNameList = []
      let selectedEmps = JSON.parse(data)
      console.log(data)
      selectedEmps.forEach(element => {
        this.msgto.push(element.id)
        EmpNameList.push(element.name)
        this.empsName = EmpNameList.toString()
      });
      // this.selecteHours = data
      //this.selecedJS = data
     
    }
    //console.log("this.selecedJS: ",this.selecedJS);
  })
}

updateCucumber()
{
  // get templates if user check that 
if(this.remember==true)
{
  this.hide1="true"
  this.provider.templates(this.platform.isRTL ? 'rtl' : 'ltr',(data)=>{
    console.log(JSON.stringify(data))
   this.templates=data
  },(data)=>{

  })
}
else
{
  this.hide1="false"
  this.hide2="false"

}

}
getTemplate()
{
  if(!this.temp){
    return
  }
  // get template body and employees if template was chosen by user
  this.hide2="true"
this.provider.templatesbody(this.temp,this.platform.isRTL ? 'rtl' : 'ltr',(data)=>{
this.templatesm=data.TemplateEmployees
this.templatebody=data.TemplateBody
this.ckeditorContent=data.TemplateBody
},(data)=>{
})
}
getdata()
{
  // get departement if to == 1 and get units if to ==2
   this.emplyees.forEach(element => {
    if(element.Value ==this.to)
    {
      this.name=element.Text
    }
  });
  if(this.to==1)
  {
   
this.provider.getdepart(1,this.platform.isRTL ? 'rtl' : 'ltr',"",(data)=>{
  console.log(JSON.stringify(data))
  this.Departments=data
  this.hide="false"

this.show="true"
},(data)=>{
})
  }
  else if(this.to==2)
  {
this.provider.getdepart(2,this.platform.isRTL ? 'rtl' : 'ltr',"",(data)=>{
  console.log(JSON.stringify(data))
  this.hide="true"
  this.show="false"

  this.units=data
},(data)=>{

})
  }
  // get all employess if user choose employess ( to == 0 )
  else if(this.to==0)
  {
    this.show="false"
    this.hide="false"
    // this.provider.Allemps(1,this.platform.isRTL ? 'rtl' : 'ltr',"",(data)=>{
    //   console.log(JSON.stringify(data))
    //   this.emplyoees=data
    //  },(data)=>{
    //    console.log(JSON.stringify(data))
    //  })
  }

}
getdepemp()
{  
  this.dpm=[]
  this.provider.Allemps(1,this.platform.isRTL ? 'rtl' : 'ltr',"",(data)=>{
    console.log(JSON.stringify(data))
    this.emplyoees=data
   },(data)=>{
     console.log(JSON.stringify(data))
   })
     // get employees of departement if user choose one 

this.provider.getdepmanager(this.quantity,2,(data)=>{
data.forEach(element => {
  this.dpm.push(element)
});
this.msgto=this.dpm
},(data)=>{

})
}
getunitemp()
{
       // get employees of unit if user choose one 
  this.dpm=[]
  this.provider.getdepmanager(this.unit,3,(data)=>{
    console.log(JSON.stringify(data))
    data.forEach(element => {
      this.emplyoees.forEach(element1 => {
        if(element==element1.Value)
        {
          this.dpm.push(element)
        }
      });
    });
   
    this.msgto=this.dpm
    },(data)=>{
    
    })
}
send()
{
  
 // send message
    if(this.to==1 && (this.quantity==""|| this.quantity==null ||this.quantity==undefined))
    {
     
      let toast = this.toastCtrl.create({
        message: this.translate.instant('insertdata'),
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
    }
    else if (this.to==2 && (this.unit==""|| this.unit==null ||this.unit==undefined)){
    

        let toast = this.toastCtrl.create({
          message: this.translate.instant('insertdata'),
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
      
    }
    else{
  this. loader = this.loadingCtrl.create({
    content: "",
  });
  this.loader.present();
  this.storage.get("EmpId").then((val) => {
    if (val) {
      // if user not select a cv
     if(this.cvdata== null || this.cvdata== "" || this.cvdata== [] || this.cvdata==undefined)
{
  // if user check to choose a template
      if(this.remember==true)
      {
        if(this.templatebody=="" || this.templatebody==null ||this.templatebody==undefined || this.temp==""|| this.temp==undefined || this.temp==null  ||  this.subject=="" || this.subject==undefined || this.subject==null)
        {
          let toast = this.toastCtrl.create({
            message: this.translate.instant('insertdata'),
            duration: 3000,
            position: 'bottom'
          });
          toast.present();
          this.loader.dismiss()

        }
     
        else{
      this.provider.savemsg([],this.templatebody,this.subject,"true",this.temp,this.msgto.toString(),val,(data)=>{
        console.log(JSON.stringify(data))
        this.msg=""
        this.loader.dismiss()
        this.subject=""
        this.remember=false
        this.msgto=[]
        this.empsName = ""
        this.hide1="false"
        this.hide2="false"
        this.temp=""
        this.filename=""
        this.templatesm=[]
        this.to=0
        this.names=[]
        this.ckeditorContent=""
        this.quantity=""
        let toast = this.toastCtrl.create({
          message: this.translate.instant('added'),
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
      },(data)=>{
        this.loader.dismiss()
        let toast = this.toastCtrl.create({
          message: "Connection Server Error",
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
      })
    }
  }
    else
    {
          // if user not check to choose a template

      if(this.ckeditorContent=="" || this.msgto.length == 0|| this.msgto==undefined  )
      {
        
        let toast = this.toastCtrl.create({
          message: this.translate.instant('insertdata'),
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
        this.loader.dismiss()

      }
      else{
        let first =this.ckeditorContent.split('>')
        let second=first[1].split('<')
        this.ckeditorContent=second[0]
      this.provider.savemsg([],this.ckeditorContent,this.subject,"false",0,this.msgto.toString(),val,(data)=>{
        console.log(JSON.stringify(data))
        this.msg=""
        this.subject=""
        this.loader.dismiss()
        this.remember=false
        this.msgto=[]
        this.empsName = ""
        this.hide1="false"
        this.hide2="false"
        this.templatesm=[]
        this.temp=""
        this.filename=""
        this.to=0
        this.names=[]
        this.ckeditorContent=""
        this.quantity=""
        let toast = this.toastCtrl.create({
          message: this.translate.instant('added'),
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
      },(data)=>{
        this.loader.dismiss()
        let toast = this.toastCtrl.create({
          message: "Connection Server Error",
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
      })
    }
  }
  }
  else{
    // if user select a cv file
    if(this.remember==true)
      {
        if(this.templatebody=="" || this.templatebody==null ||this.templatebody==undefined || this.temp==""|| this.temp==undefined || this.temp==null  ||  this.subject=="" || this.subject==undefined || this.subject==null)
        {
          let toast = this.toastCtrl.create({
            message: this.translate.instant('insertdata'),
            duration: 3000,
            position: 'bottom'
          });
          toast.present();
          this.loader.dismiss()

        }
        else{
      this.provider.savemsg(this.cvs,this.templatebody,this.subject,"true",this.temp,this.msgto.toString(),val,(data)=>{
        console.log(JSON.stringify(data))
        this.loader.dismiss()
        this.msg=""
        this.subject=""
        this.remember=false
        this.msgto=[]
        this.empsName = ""
        this.templatesm=[]
        this.hide1="false"
        this.hide2="false"
        this.temp=""
        this.filename=""
        this.ckeditorContent=""
        this.to=0
        this.names=[]
        this.cvs=[]
        let toast = this.toastCtrl.create({
          message: this.translate.instant('added'),
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
      },(data)=>{
        this.loader.dismiss()
        let toast = this.toastCtrl.create({
          message: "Connection Server Error",
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
      })
    }
  }
    else
    {
     
      if( this.ckeditorContent=="" || this.ckeditorContent==null ||this.ckeditorContent==undefined || this.msgto.length == 0|| this.msgto==undefined || this.msgto==null  ||  this.subject=="" || this.subject==undefined || this.subject==null)
      {
        let toast = this.toastCtrl.create({
          message: this.translate.instant('insertdata'),
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
        this.loader.dismiss()

      }
      else{
        let first =this.ckeditorContent.split('>')
        let second=first[1].split('<')
        this.ckeditorContent=second[0]
      this.provider.savemsg(this.cvs,this.ckeditorContent,this.subject,"false",0,this.msgto.toString(),val,(data)=>{
        console.log(JSON.stringify(data))
        this.msg=""
        this.subject=""
        this.remember=false
        this.msgto=[]
        this.empsName = ""
        this.filename=""
        this.hide1="false"
        this.hide2="false"
        this.temp=""
        this.names=[]
        this.templatesm=[]
        this.ckeditorContent=""
        this.to=0
        this.cvs=[]
        this.loader.dismiss()
        let toast = this.toastCtrl.create({
          message: this.translate.instant('added'),
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
      },(data)=>{
        this.loader.dismiss()
        let toast = this.toastCtrl.create({
          message: "Connection Server Error",
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
      })
    }
  }
  }
    }
  
  })
}

}
getfile()
{
  // to get a cv from device
  this.fileTransfer = this.transfer.create();
//for ios devices
  if (this.plt.is('ios')) {
    let self = this;
    self.filepicker.pickFile()
      .then(uri => {
      let correctPath = uri.substr(0, uri.lastIndexOf('/') + 1);
      self. filename=uri.substr(uri.lastIndexOf('/') + 1)
      self. fileExt = this.filename.split('.').pop();
      let name=this.filename
      this.names.push(name)
      self .file.readAsDataURL("file:///"+ correctPath,this.filename).then((val)=>{
        this.base64.encodeFile(this.nativepath).then((base64File: string) => {
          this.cvdata=base64File
          let cv_strip = this.cvdata.split(',')[1]
          console.log(this.cvdata)
          let filename={
            'FileName': this.filename,
            'Bytes':cv_strip
          }
          this.cvs.push(filename)
        }, (err) => {
          console.log("base"+err);
        });
     }).catch(err => console.log('Error reader'+ err));


  }) .catch(err => console.log('Error'+ err));


}
// for android devices
  else if(this.plt.is('android'))
  {
  this.fileChooser.open()
  .then(uri => {
    this.filepath.resolveNativePath(uri).then((result) => {
      this.nativepath = result;
      this.filename=this.nativepath.substr(this.nativepath.lastIndexOf('/') + 1)
      this.fileExt = this.filename.split('.').pop();
      this.names.push(this.filename)
      this.base64.encodeFile(this.nativepath).then((base64File: string) => {
        this.cvdata=base64File
        let cv_strip = this.cvdata.split(',')[1]
        console.log(this.cvdata)
        let filename={
          'FileName': this.filename,
          'Bytes':cv_strip
        }
        this.cvs.push(filename)
      }, (err) => {
        console.log("base"+err);
      });

    }, (err) => {
      console.log(err);
    })


 })
  .catch(e => console.log(e));
}
}

}
