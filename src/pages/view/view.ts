import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ViewController } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { TranslateService } from '@ngx-translate/core';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';

// page to view message's replays
@Component({
  selector: 'page-view',
  templateUrl: 'view.html',
})
export class ViewPage {
array:any
main:any=[]
attach:any=[]
approvelist:any=[]
napprovelist:any=[]
langDirection:any
replays:any=[]
  constructor(public transfer:FileTransfer,public file:File,public ViewCtrl:ViewController,public platform:Platform,public helper:HelperProvider,public translate:TranslateService,public navCtrl: NavController, public navParams: NavParams) {
    this.array=this.navParams.get("data")
  }

  ionViewDidLoad() {
    if (this.helper.currentLang == 'ar')
    {
      this.translate.use('ar');
      this.helper.currentLang = 'ar';
      this.translate.setDefaultLang('ar');
      this.helper.lang_direction = 'rtl';
      this.langDirection = "rtl";
      this.platform.setDir('rtl',true)
    }
    else{
     
      this.translate.use('en');
      this.helper.currentLang = 'en';
      this.translate.setDefaultLang('en');
      this.helper.lang_direction = 'ltr';
      this.langDirection = "ltr";
      this.platform.setDir('ltr',true)
    }
   let array=this.array.Main
   
      this.main.push(array[0])
    
    this.attach=this.array.Attachments
    this.replays=this.array.Replys
    this.approvelist=this.array.Approve
    this.napprovelist=this.array.Disapprove
  }
  // for download attatchment from msg
  download(url,name)
  {
    console.log("transfer "+url + " "+ name)
    let uri=encodeURI( url)
    //window.open(uri,"_blank")
    window.open("http://169.51.128.168:1111//Uploads//27_03_2019_16_12_37_1237//Screenshot_2019-03-27-10-15-23-06.png","_blank")
    // const fileTransfer: FileTransferObject = this.transfer.create();
    // fileTransfer.download("https://cs5.erppluscloud.com/Uploads/HRPlus/5/12_11_2018_11_22_14_2214/10-farm-uses-of-air-compressor.jpg", this.file.externalRootDirectory + name,true).then((entry) => {
    // }, (error) => {
    //   console.log("error transfer")
    //   console.log(error)
    //      });
  }
}
