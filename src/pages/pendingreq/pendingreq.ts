import { Component } from '@angular/core';
import { NavController, NavParams, Platform, ToastController, MenuController, ViewController, Events } from 'ionic-angular';
import { ApiservicesProvider } from '../../providers/apiservices/apiservices';
import { HelperProvider } from '../../providers/helper/helper';
import { Storage } from '@ionic/storage';
import { DatePipe } from '@angular/common';
import { AlertController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-pendingreq',
  templateUrl: 'pendingreq.html',
})
// this page appear only for admins to show employees requests
export class PendingreqPage {
  vacationAndMissionList: any[] = [];
  permissionAndOvertimeList: any[] = [];
  invoice: any;
  langDirection:any
  constructor(public event:Events,public navCtrl: NavController, public navParams: NavParams, public helper: HelperProvider, public apiservicesProvider: ApiservicesProvider,
    public platform: Platform,private alertCtrl: AlertController,
     public storage: Storage, public datePipe: DatePipe, 
     public toastCtrl: ToastController
     , public translate: TranslateService,
     public ViewCtrl:ViewController,
    public menue:MenuController) {
    if (this.helper.currentLang == 'ar')
    {
      this.translate.use('ar');
      this.helper.currentLang = 'ar';
      this.translate.setDefaultLang('ar');
      this.helper.lang_direction = 'rtl';
      this.langDirection = "rtl";
    }
    else{
     
      this.translate.use('en');
      this.helper.currentLang = 'en';
      this.translate.setDefaultLang('en');
      this.helper.lang_direction = 'ltr';
      this.langDirection = "ltr";
    }
    this.invoice = "vacationMission";
    storage.get("EmpId").then((val) => {
      if (val) {
        apiservicesProvider.readVacationRequests(val, 1, this.platform.isRTL ? 'rtl' : 'ltr',
          (data) => {
            console.log(JSON.stringify(data))

            if (data) {
              for (var i = 0; i < data.length; i++) {
                data[i].StartDate = this.datePipe.transform(data[i].StartDate, 'MM/dd/yyyy');
                data[i].EndDate = this.datePipe.transform(data[i].EndDate, 'MM/dd/yyyy');
              }
              this.vacationAndMissionList = data;
            }
          },
          (err) => {
            if (JSON.stringify(err.status) == '401') {
              // UnAuthorized
              this.event.publish("login","");
            }
            console.log(err);
          });
        apiservicesProvider.readVacationRequests(val, 2, this.platform.isRTL ? 'rtl' : 'ltr',
          (data) => {
            console.log('Succeed 2');
            console.log(JSON.stringify(data))
           
            if (data) {
              for (var i = 0; i < data.length; i++) {
                data[i].StartDate = this.datePipe.transform(data[i].StartDate, 'MM/dd/yyyy');
              }
              this.permissionAndOvertimeList = data;
            }
          },
          (err) => {
            if (JSON.stringify(err.status) == '401') {
              // UnAuthorized
              this.event.publish("login","");
            }
            console.log(err);
          }
        );
      }
    });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad PendingreqPage');
  }
  // to accept the request
  acceptEvent(eventId) {
    let alert = this.alertCtrl.create({
      message:this.translate.instant('MsgAccept') ,
      buttons: [
        {
          text:this.translate.instant('cancel'),
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: this.translate.instant('ok'),
          handler: () => {
              this.apiservicesProvider.vacationRequestAccept(this.helper.Empid, eventId, this.platform.isRTL ? 'rtl' : 'ltr',
              (data) => {
                console.log(data);
                if (data.Result == true) {
                  let toast = this.toastCtrl.create({
                    message: this.translate.instant('confirmMsgPending'),
                    duration: 3000,
                    position: 'bottom'
                  });
                  toast.present();
                  this.storage.get("EmpId").then((val) => {
                    if (val) {
                     this. apiservicesProvider.readVacationRequests(val, 1, this.platform.isRTL ? 'rtl' : 'ltr',
                        (data) => {
                          if (data) {
                            for (var i = 0; i < data.length; i++) {
                              data[i].StartDate = this.datePipe.transform(data[i].StartDate, 'MM/dd/yyyy');
                              data[i].EndDate = this.datePipe.transform(data[i].EndDate, 'MM/dd/yyyy');
                            }
                            this.vacationAndMissionList = data;
                          }
                        },
                        (err) => {
                          if (JSON.stringify(err.status) == '401') {
                            // UnAuthorized
                            this.event.publish("login","");
                          }
                          console.log(err);
                        });
                      this.apiservicesProvider.readVacationRequests(val, 2, this.platform.isRTL ? 'rtl' : 'ltr',
                        (data) => {
                          console.log('Succeed 2');
                          console.log(data);
                         
                          if (data) {
                            for (var i = 0; i < data.length; i++) {
                              data[i].StartDate = this.datePipe.transform(data[i].StartDate, 'MM/dd/yyyy');
                            }
                            this.permissionAndOvertimeList = data;
                          }
                        },
                        (err) => {
                          if (JSON.stringify(err.status) == '401') {
                            // UnAuthorized
                            this.event.publish("login","");
                          }
                          console.log(err);
                        }
                      );
                    }
                  });
                }
                else {
                  let toast = this.toastCtrl.create({
                    message: data.ErrorMessage,
                    duration: 3000,
                    position: 'bottom'
                  });
                  toast.present();
                }
              },
              (err) => {
                console.log(err);
              }
            ); 

          }
        }
      ]
    });
    alert.present();
   
  }
 // to reject the request
  rejectEvent(eventId) {

    let alert = this.alertCtrl.create({
      message:this.translate.instant('MsgReject'),
      buttons: [
        {
          text: this.translate.instant('cancel'),
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: this.translate.instant('ok'),
          handler: () => {
    this.apiservicesProvider.vacationRequestReject(this.helper.Empid, eventId, this.platform.isRTL ? 'rtl' : 'ltr',
    (data) => {
      console.log("data is   ", data);
       if (data.Result == true) {
        let toast = this.toastCtrl.create({
          message:this.translate.instant('confirmMsgPending'),
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
        this.storage.get("EmpId").then((val) => {
          if (val) {
            this.apiservicesProvider.readVacationRequests(val, 1, this.platform.isRTL ? 'rtl' : 'ltr',
              (data) => {
                if (data) {
                  for (var i = 0; i < data.length; i++) {
                    data[i].StartDate = this.datePipe.transform(data[i].StartDate, 'MM/dd/yyyy');
                    data[i].EndDate = this.datePipe.transform(data[i].EndDate, 'MM/dd/yyyy');
                  }
                  this.vacationAndMissionList = data;
                }
              },
              (err) => {
                if (JSON.stringify(err.status) == '401') {
                  // UnAuthorized
                  this.event.publish("login","");
                }
                console.log(err);
              });
            this.apiservicesProvider.readVacationRequests(val, 2, this.platform.isRTL ? 'rtl' : 'ltr',
              (data) => {
                console.log('Succeed 2');
                console.log(data);
               
                if (data) {
                  for (var i = 0; i < data.length; i++) {
                    data[i].StartDate = this.datePipe.transform(data[i].StartDate, 'MM/dd/yyyy');
                  }
                  this.permissionAndOvertimeList = data;
                }
              },
              (err) => {
                if (JSON.stringify(err.status) == '401') {
                  // UnAuthorized
                  this.event.publish("login","");
                }
                console.log(err);
              }
            );
          }
        });
      }
      else {
        let toast = this.toastCtrl.create({
          message: data.ErrorMessage,
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
      }
    },
    (err) => {
      console.log(err);
    }
  ); 
          }
        }
      ]
    });
    alert.present();
  }

  doRefresh(ev)
  {
    this.storage.get("EmpId").then((val) => {
      if (val) {
        this.apiservicesProvider.readVacationRequests(val, 1, this.platform.isRTL ? 'rtl' : 'ltr',
          (data) => {
            console.log('Succeed 1');
            console.log(data);
          

            if (data) {
              for (var i = 0; i < data.length; i++) {
                data[i].StartDate = this.datePipe.transform(data[i].StartDate, 'MM/dd/yyyy');
                data[i].EndDate = this.datePipe.transform(data[i].EndDate, 'MM/dd/yyyy');
              }
              this.vacationAndMissionList = data;
            }
          },
          (err) => {
            if (JSON.stringify(err.status) == '401') {
              // UnAuthorized
              this.event.publish("login","");
            }
            console.log(err);
          });
       this.apiservicesProvider.readVacationRequests(val, 2, this.platform.isRTL ? 'rtl' : 'ltr',
          (data) => {
            console.log('Succeed 2');
            console.log(data);
           
            if (data) {
              for (var i = 0; i < data.length; i++) {
                data[i].StartDate = this.datePipe.transform(data[i].StartDate, 'MM/dd/yyyy');
              }
              this.permissionAndOvertimeList = data;
            }
          },
          (err) => {
            if (JSON.stringify(err.status) == '401') {
              // UnAuthorized
              this.event.publish("login","");
            }
            console.log(err);
          }
        );
      }
    });
    ev.complete();

  }
  openmenu()
  {
    this.menue.open()

  }
}
