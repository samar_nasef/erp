import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Platform, ViewController, MenuController ,Events} from 'ionic-angular';
import { ApiservicesProvider } from '../../providers/apiservices/apiservices';
import { HelperProvider } from '../../providers/helper/helper';
import { ToastController } from 'ionic-angular';
import * as Moment from 'moment';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { TabsPage } from '../tabs/tabs';
import { DatePicker } from '@ionic-native/date-picker';
import { DatePipe } from '@angular/common';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-leavereq',
  templateUrl: 'leavereq.html',
})
export class LeavereqPage {
  myPhoto: string;
  date:any=new Date()
  date1:any=new Date()

  isReadonly:any=true
  Type: any;
  status: number=1;
  page:any
  senddate:any
  appear:any
  from: any= this.translate.instant('date');
  to: any= this.translate.instant('date')
  note: any;
  array:any=[]
 to1:any
 from1:any
  balance: number = 0;
  vacationList: any[] = [];
  langDirection:any
  hide1:any="true"
  dis:any="false"
  hide:any="true"
  dis1:any="false"
  constructor(public menue:MenuController,public navCtrl: NavController, public navParams: NavParams
    , public ApiservicesProvider: ApiservicesProvider
    , public helper: HelperProvider
    , private toastCtrl: ToastController,
    public ViewCtrl:ViewController
    ,public datePicker:DatePicker
    , public storage: Storage,public platform:Platform,
    public event:Events,
    public datepipe: DatePipe,
    public navctrl:NavController
    //,private camera: Camera
    //,private transfer: FileTransfer
    //, private file: File
    , public loadingCtrl: LoadingController, public translate: TranslateService

  ) {
    this.senddate=this.navParams.get("date")
    this.page=this.navParams.get("Page")
    if(this.page=="home")
    {
      if(this.status==2||this.status==3||this.status==4)
      {
      this.to1=this.datepipe.transform(this.senddate, 'MM-dd-yyyy')
      this.from1=this.datepipe.transform(this.senddate, 'MM-dd-yyyy')
      this.to=this.to1
      this.from=this.from1
      this.dis="true"
      this.hide="false"
      this.hide1="false"
      this.dis1="true"
      }
      else if (this.status==1)
      {
        
        this.from1=this.datepipe.transform(this.senddate, 'MM-dd-yyyy')
        this.from=this.datepipe.transform(this.senddate, 'MM-dd-yyyy')
        this.dis="false"
        this.dis1="true"
        this.hide1="false"
      this.hide="true"
      }
    }

    if (this.helper.currentLang == 'ar')
    {
      this.translate.use('ar');
      this.helper.currentLang = 'ar';
      this.translate.setDefaultLang('ar');
      this.helper.lang_direction = 'rtl';
      this.langDirection = "rtl";
      this.platform.setDir('rtl',true)
    }
    else{
     
      this.translate.use('en');
      this.helper.currentLang = 'en';
      this.translate.setDefaultLang('en');
      this.helper.lang_direction = 'ltr';
      this.langDirection = "ltr";
      this.platform.setDir('ltr',true)
    }

    this.getVacationsIDs();

  }
  // get All Vactions  IDs  from Api 
  getVacationsIDs() {
    this.storage.get('EmpId').then((val) => {
      this.ApiservicesProvider.getVacations(
        val
        , this.helper.lang_direction
        , (data) => this.successCallbackvacations(data)
        , (data) => this.failureCallbackvacations(data))
    })

  }
  changeperiod()
  {
    if(this.page=="home")
  {
    if(this.status==2||this.status==3||this.status==4)
      {
        this.dis="true"
        
        this.hide="false"
        this.to1=this.from1
        this.to=this.to1

      }
      else{

        this.from1=this.senddate
        this.from=this.from1
        this.dis="false"
        this.dis1="true"
        this.hide1="false"
        this.hide="true"
      }
    }
    else
    {
     
      if(this.status==2||this.status==3||this.status==4)
      {
       this.to=this.from

      }
      else{
    
       this.to=this.translate.instant('date');
       this.dis="false"
       this.dis1="false"
       this.hide1="true"
       this.hide="true"
      }

    }
  }
  getdate()
  {
   
    let localLang 
    let nowTxt
    let okTxt 
    let cancelTxt 
    var minmDate;
    var maxDate;
    if (this.platform.is('ios')) {
      
      minmDate =String(new Date()); 
      let d=new Date()
      let year =d.getFullYear()
      let month=d.getMonth()
      let day=d.getDay()
      maxDate = (new Date(year + 1,month,day)).valueOf();
    }
    else {
      minmDate =(new Date()).valueOf(); 
      let d=new Date()
      let year =d.getFullYear()
      let month=d.getMonth()
      let day=d.getDay()
      maxDate = (new Date(year + 1,month,day)).valueOf();
    }
    if (this.helper.currentLang == 'ar') {
      localLang = 'ar_eg';
      nowTxt = 'اليوم';
      okTxt = 'تم';
      cancelTxt = 'إلغاء'
      this.datePicker.show({
        date: this.date1,
        mode: 'date',
        // minDate: minmDate,
        maxDate: maxDate,
        okText: okTxt,
        cancelText: cancelTxt,
        todayText: nowTxt,
        locale: localLang,
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
      }).then(
        date =>{ 
          this.date1=date
          this.from=date.toLocaleDateString('ar-EG')
          this.from=this.from.replace("/","-").replace("/","-")
          this.from=this.datepipe.transform(date, 'MM-dd-yyyy');
          if(this.status==2 || this.status==3 || this.status==4 )
          {
            this.to=this.from
          }
          
    },
        err => console.log('Error occurred while getting date: ', err)
      );
    }
    else{
      localLang = 'en_us';
      nowTxt = 'today';
      okTxt = 'okay';
      cancelTxt = 'cancel'
      this.datePicker.show({
        date: this.date1,
        mode: 'date',
        // minDate: minmDate,
        maxDate: maxDate,
        okText: okTxt,
        cancelText: cancelTxt,
        todayText: nowTxt,
        locale: localLang,
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
      }).then(
        date =>{ 
          this.date1=date

          this.from=date.toLocaleDateString('en-US')
  
      this.from=this.from.replace("/","-").replace("/","-")
      this.from=this.datepipe.transform(date, 'MM-dd-yyyy');
      if(this.status==2 || this.status==3 || this.status==4 )
      {
        this.to=this.from
      }
      
    },
        err => console.log('Error occurred while getting date: ', err)
      );
    }
   
  }
  getdate1()
  {
    let localLang 
    let nowTxt
    let okTxt 
    let cancelTxt 
    var minmDate;
    var maxDate;
    if (this.platform.is('ios')) {
      
      minmDate =String(new Date()); 
      let d=new Date()
      let year =d.getFullYear()
      let month=d.getMonth()
      let day=d.getDay()
      maxDate = (new Date(year + 1,month,day)).valueOf();
    }
    else {
      minmDate =(new Date()).valueOf(); 
      let d=new Date()
      let year =d.getFullYear()
      let month=d.getMonth()
      let day=d.getDay()
      maxDate = (new Date(year + 1,month,day)).valueOf();
    } 
    if (this.helper.currentLang == 'ar') {
      localLang = 'ar_eg';
      nowTxt = 'اليوم';
      okTxt = 'تم';
      cancelTxt = 'إلغاء'
      this.datePicker.show({
        date: this.date,
        mode: 'date',
        // minDate: minmDate,
        maxDate: maxDate,
        okText: okTxt,
        cancelText: cancelTxt,
        todayText: nowTxt,
        locale: localLang,
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
      }).then(
        date =>{ 
          this.date=date

          this.to=date.toLocaleDateString('ar-EG')
        this.to=this.to.replace("/","-").replace("/","-")
       this.to=this.datepipe.transform(date, 'MM-dd-yyyy');
          if(this.status==2 || this.status==3 || this.status==4 )
          {
           
           this.from=this.to
          
          }
    },
        err => console.log('Error occurred while getting date: ', err)
      );
    }
    else{
      localLang = 'en';
      nowTxt = 'today';
      okTxt = 'okay';
      cancelTxt = 'cancel'
      this.datePicker.show({
        date: this.date,
        mode: 'date',
        okText: okTxt,
        // minDate: minmDate,
        maxDate: maxDate,
        cancelText: cancelTxt,
        todayText: nowTxt,
        locale: localLang,
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
      }).then(
        date =>{ 
          this.date=date

          this.to=date.toLocaleDateString('en-US')
    
        this.to=this.to.replace("/","-").replace("/","-")
        this.to=this.datepipe.transform(date, 'MM-dd-yyyy');
          if(this.status==2 || this.status==3 || this.status==4 )
          {
           
           this.from=this.to
          
          }
    },
        err => console.log('Error occurred while getting date: ', err)
      );
    }
  
  }
  successCallbackvacations(data) {
    this.vacationList = JSON.parse(data);
    this.array.push(this.vacationList[0])
    this.array.forEach(element => {
      this.appear=element.Name
      this.balance=element.Balance
      this.Type=element.Id
    });
    if (this.vacationList.length == 0) {

      const toast = this.toastCtrl.create({
        message: this.translate.instant('msgNoVacations'),
        duration: 3000
      });
      toast.present();

    } else {
      console.log("there is data !");
    }

  }
  failureCallbackvacations(data) {
    if (JSON.stringify(data.status) == '401') {
      // UnAuthorized
      this.event.publish("login","");
    }

  }
  ionViewDidLoad() {
  }
  ionViewDidEnter() {
  }
  //======================== save Event ========================
  saveVacation() {
    if(this.status== 1)
    {
      if(this.from<=this.to)
  {
    if (this.status && this.from && this.to ) {
      this.storage.get('EmpId').then((val) => {
        this.ApiservicesProvider.SaveRequestVacation(

          val
          , this.helper.lang_direction
          , this.helper.EventTypeLeave
          , this.Type
          , this.status
          , this.from
          , this.to
          , this.note
          , (data) => this.successCallbackSave(data)
          , (data) => this.failureCallbackSave(data))


      })
    } else {
      console.log("you must enter data ");
      const toast = this.toastCtrl.create({
        message: this.translate.instant('msgEnterUsername'),
        duration: 3000
      });
      toast.onDidDismiss(() => {
        console.log('Dismissed toast');
      });

      toast.present();
    }
  }
  else{
    const toast = this.toastCtrl.create({
      message: this.translate.instant('fromandto'),
      duration: 3000
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
    }
    else if(this.status==2 || this.status==3 || this.status==4)
    {
    if(this.from==this.to)
  {

  
  console.log(
     this.helper.lang_direction
    , this.helper.EventTypeLeave
    , this.Type
    , this.status
    , this.from
    , this.to
    , this.note)
    if (this.status && this.from && this.to ) {
      this.storage.get('EmpId').then((val) => {
        this.ApiservicesProvider.SaveRequestVacation(
          val
          , this.helper.lang_direction
          , this.helper.EventTypeLeave
          , this.Type
          , this.status
          , this.from
          , this.to
          , this.note
          , (data) => this.successCallbackSave(data)
          , (data) => this.failureCallbackSave(data))


      })
    } else {
      console.log("you must enter data ");
      const toast = this.toastCtrl.create({
        message: this.translate.instant('msgEnterUsername'),
        duration: 3000
      });
      toast.onDidDismiss(() => {
        console.log('Dismissed toast');
      });

      toast.present();
    }
  }
  else{
    const toast = this.toastCtrl.create({
      message: this.translate.instant('oneday'),
      duration: 3000
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
}
  }
  frommethod()
  {
    if(this.status==2 || this.status==3 || this.status==4 )
    {   
      this.to=this.from
    }
  }
  tomethod()
  {
    if(this.status==2 || this.status==3 || this.status==4 )
    {
     this.from=this.to

    }
  }
  successCallbackSave(data) {
    console.log("saved succesfully");
    console.log(data);
    if (JSON.parse(data).Result == true) {
      const toast = this.toastCtrl.create({
        message: this.translate.instant('DataSavedSuccessfully'),
        duration: 3000
      });
      //
      toast.present();
      this.Type = " ";
      this.status = 1;
      this.from = " ";
      this.to = " ";
      this.note = " ";
      this.ViewCtrl.dismiss()
    }
    else {
      const toast = this.toastCtrl.create({
        message: JSON.parse(data).ErrorMessage,
        duration: 3000
      });
 
      toast.present();
    
    }


  }
  failureCallbackSave(data) {
    console.log(status);
    if (JSON.stringify(data.status) == '401') {
      // UnAuthorized
      this.event.publish("login","");
    }
  }

  uploadImg() {

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();


  }
  //
  changeBalance() {
    console.log(this.Type);
    //
    for (var i = 0; i < this.vacationList.length; i++) {
      if (this.vacationList[i].Id == this.Type) {
        this.balance = this.vacationList[i].Balance;
        console.log(this.balance);
      }
    }
  }
  openmenu()
  {
    this.menue.open()

  }
}
