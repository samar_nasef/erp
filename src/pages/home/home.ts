
import { Component } from '@angular/core';
import { NavController ,Events, LoadingController} from 'ionic-angular';
import { MenuController} from 'ionic-angular';
import { NotificaionPage } from '../notificaion/notificaion';
import { Storage } from '@ionic/storage/dist/storage';
import { HelperProvider } from '../../providers/helper/helper';
import { TranslateService } from '@ngx-translate/core';
import { Platform } from 'ionic-angular/platform/platform';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { RequestsPage } from '../requests/requests';
import { AlertController } from 'ionic-angular';
import { OutsideReqPage } from '../outside-req/outside-req';
import { PermissionreqPage } from '../permissionreq/permissionreq';
import { OverTimeReqPage } from '../over-time-req/over-time-req';
import { LeavereqPage } from '../leavereq/leavereq';
import { ApiservicesProvider } from '../../providers/apiservices/apiservices';
import { Device } from '@ionic-native/device';
import { LoginPage } from '../login/login';
import { sharedStylesheetJitUrl } from '@angular/compiler';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  langDirection: any = this.helper.lang_direction;
  serviceurl: any
  userid: any
  accesstoken: any
  year: any
  day: any
  varra: any = []
  actionArray: any = []
  dayame: any
  lang_direction: any
  month: any
  y: number
  senddate:any
  constructor(public loadingCtrl:LoadingController,public device:Device,public event :Events,private alertCtrl: AlertController, public toastCtrl: ToastController, public http: HttpClient, public platform: Platform,
    public translate: TranslateService, public helper: HelperProvider, public storage: Storage, public menu: MenuController, public navCtrl: NavController, public ApiservicesProvider: ApiservicesProvider) {
      this.helper.DeviceId =this.device.uuid
     
      this.senddate= new Date().toISOString();

      
      if (this.helper.currentLang == 'en') {

        let date = new Date().toDateString();
    this.day = new Date().getDate();
    this.year = new Date().getFullYear();
    let currentDate = new Date();
    let weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    this.dayame = weekdays[currentDate.getDay()];
    let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    this.month = months[currentDate.getMonth()];
      }
      else{

        let date = new Date().toDateString();
        this.day = new Date().getDate();
        this.year = new Date().getFullYear();
        let currentDate = new Date();
        let weekdays = ["الأحد", "الإثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعه", "السبت"];
        this.dayame = weekdays[currentDate.getDay()];
        let months = ["يناير", "فبراير", "مارس", "أبريل", "مايو", "يونيو", "يوليو", "أغسطس", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"];
        this.month = months[currentDate.getMonth()];
      }

    storage.get("EmpId").then((val) => {
      if (val) {
        this.userid = val;
        this.helper.Empid = val;
        storage.get("UserPass").then((val) => {

        });

      }
    });       

  }
  success(data) {
    this.varra = data;
    console.log(data);
  }
 
  actionsucess(data) {
    console.log(data);
    this.actionArray = data;
  }
  
  actionfail(data) {
    if (JSON.stringify(data.status) == '401') {
      this.event.publish("login","");
    }
  }
  ionViewDidLoad() {
    console.log(JSON.stringify(this.helper.accesstoken))
    this.storage.get("url").then((val1) => {
      this.serviceurl = val1
      this.helper.serviceurl = val1
    // this.storage.get("access_token").then((val) => {
    //   this.accesstoken = val
    //   this.helper.accesstoken = val;
    //   this.getAlvacation(val, (data) => this.success(data), data => this.fail(data));
    //   this.getActions(val, (data) => this.actionsucess(data), (data) => this.actionfail(data));
    // });
  });
  }
  ionViewDidEnter() {
  
    this.ApiservicesProvider.getFingerCred((data) => {
      console.log("refresh resp from getFingerCred : ",data)
      var jsonObject = data//JSON.parse(data);
      this.storage.set("AllowSensor", jsonObject.AllowSensor); //0 fingerprint not necessary , 1 fingerprint necessary
      this.storage.set("AllowOfflineFinger", jsonObject.AllowOfflineFIngerPrint); //0 offlinefingerprint not available , 1 offlinefingerprint available
      if (jsonObject.IsRemoved == 1){
        console.log("user logout from app")
        this.helper.presentToast(this.translate.instant("logoutFromAdmin"))
        this.navCtrl.setRoot(LoginPage)
        // this.menu.close()
        this.storage.remove("access_token");
        this.storage.remove("user_info");
        this.storage.remove("EmpId");
        this.storage.remove("UserPass");
        this.storage.remove("IsAdmin");
        this.event.publish("direction",this.langDirection);

      }else{
console.log("user staty in app")
      }

    }, (data) => {
      console.log("refresh error from getFingerCred : ",data)
    });

    
    this.storage.get("url").then((val1) => {
      this.serviceurl = val1
      this.helper.serviceurl = val1
      this.storage.get("access_token").then((val) => {
        this.accesstoken = val
        this.helper.accesstoken = val;
        this.getAlvacation(val, (data) => this.success(data), data => this.fail(data));
        this.getActions(val, (data) => this.actionsucess(data), (data) => this.actionfail(data));
      });
    });
  }
  presentToast() {
    let toast = this.toastCtrl.create({
      message: this.translate.instant('InternetError'),
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }
  fail(data) {
    console.log(data);
    console.log(JSON.stringify(data));
    console.log(JSON.stringify(data.status));
    if (JSON.stringify(data.status) == '401') {
      // UnAuthorized
      this.event.publish("login","");
    }
  }
  openmenu() {
   
    this.menu.open()
  }
  opennotificaion() {
    this.navCtrl.push(NotificaionPage)
  }
  isvalid() {

  }
  getAlvacation(token, authSuccessCallback, authFailureCallback) {
    let loader = this.loadingCtrl.create({
      content: "",
    });
    loader.present();
    let serviceUrl = this.serviceurl;

    let headers = new HttpHeaders()
    headers = headers.set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + token);//client_credentialsserviceurl
    if (this.langDirection == this.helper.Direction) {
                loader.dismiss().catch(() => console.log('ERROR CATCH: LoadingController dismiss'));

      serviceUrl = serviceUrl + '/api/HR/GetEmployeePaidVacation?EmpId=' + this.helper.Empid + '&Direction=' + (this.platform.isRTL ? 'rtl' : 'ltr');// this.helper.Direction;

    }
    else {
      serviceUrl = serviceUrl + '/api/HR/GetEmployeePaidVacation?EmpId=' + this.helper.Empid + '&Direction=' + (this.platform.isRTL ? 'rtl' : 'ltr');// this.helper.Direction;

    }
    this.http.get(serviceUrl, { headers: headers })
      .subscribe(
        data => {
          console.log(data);
          loader.dismiss().catch(() => console.log('ERROR CATCH: LoadingController dismiss'));

          authSuccessCallback(data)
        },
        err => {
          loader.dismiss().catch(() => console.log('ERROR CATCH: LoadingController dismiss'));

          authFailureCallback(err);
        }
      )
  }
  getActions(token, authSuccessCallback, authFailureCallback) {

    let serviceUrl;
    let serviceurl = this.serviceurl;
    let headers = new HttpHeaders()
    headers = headers.set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + token);//client_credentials
    if (this.langDirection == this.helper.Direction) {
      serviceUrl = serviceurl + '/api/HR/GetAttendanceMonthlyIssues?EmpId=' + this.helper.Empid + '&Direction=' + (this.platform.isRTL ? 'rtl' : 'ltr');// this.helper.Direction;
    } else {
      serviceUrl = serviceurl + '/api/HR/GetAttendanceMonthlyIssues?EmpId=' + this.helper.Empid + '&Direction=' + (this.platform.isRTL ? 'rtl' : 'ltr');// this.helper.Direction;

    }
    this.http.get(serviceUrl, { headers: headers })
      .subscribe(
        data => {

          authSuccessCallback(data)
        },
        err => {
          authFailureCallback(err);
        }
      )
  }
  fix(y, selectedDate) {
    console.log(selectedDate);

      this.senddate=selectedDate
    console.log(y);
    if (y == 1) {
      this.navCtrl.push(PermissionreqPage, {selectedDate:selectedDate,Page:"home"});
    }
    if (y == 2) {
      this.presentConfirm();
    }
    if (y == 3) {
      this.navCtrl.push(OverTimeReqPage , {selectedDate:selectedDate,Page:"home"})
    }
  }
  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: this.translate.instant('SelectAnswer'),
      message: this.translate.instant('SelectAnswerQuestion'),
      buttons: [
        {
          text: this.translate.instant('Vacation'),
          role: 'cancel',
          handler: () => {
            this.navCtrl.push(LeavereqPage,{date:this.senddate,Page:"home"});
          }
        },
        {
          text: this.translate.instant('Mission'),
          handler: () => {
            this.navCtrl.push(OutsideReqPage,{date:this.senddate,Page:"home"});
          }
        }
      ]
    });
    alert.present();
  }
  doRefresh(ev)
  {

    this.ApiservicesProvider.getFingerCred((data) => {
      console.log("refresh resp from getFingerCred : ",data)
      var jsonObject = data//JSON.parse(data);
      this.storage.set("AllowSensor", jsonObject.AllowSensor); //0 fingerprint not necessary , 1 fingerprint necessary
      this.storage.set("AllowOfflineFinger", jsonObject.AllowOfflineFIngerPrint); //0 offlinefingerprint not available , 1 offlinefingerprint available
      
      if (jsonObject.IsRemoved == 1){
        console.log("user logout from app")
        this.helper.presentToast(this.translate.instant("logoutFromAdmin"))
        this.navCtrl.setRoot(LoginPage)
        // this.menu.close()
        this.storage.remove("access_token");
        this.storage.remove("user_info");
        this.storage.remove("EmpId");
        this.storage.remove("UserPass");
        this.storage.remove("IsAdmin");
        this.event.publish("direction",this.langDirection);

      }else{
console.log("user staty in app")
      }

    }, (data) => {
      console.log("refresh error from getFingerCred : ",data)
    });


    this.storage.get("access_token").then((val) => {
      this.accesstoken = val
      this.helper.accesstoken = val;
      //
      this.getAlvacation(val, (data) => this.success(data), data => this.fail(data));
      this.getActions(val, (data) => this.actionsucess(data), (data) => this.actionfail(data));
    });
    ev.complete();

  }
  //
}
