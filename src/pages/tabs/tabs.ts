import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { LoginPage } from '../login/login';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { MessagesPage } from '../messages/messages';
import { RequestsPage } from '../requests/requests';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = ContactPage;
  tab2Root = AboutPage;
  tab3Root = HomePage;
  tab4Root = RequestsPage;
  tab5Root = MessagesPage;
  tabIndex: Number = 2;

  constructor(public params: NavParams) {
    let tabIndex2 = this.params.get('tabIndex');
    if (parseInt(tabIndex2) > -1) {
      this.tabIndex = tabIndex2;
    }
  }
}
