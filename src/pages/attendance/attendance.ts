import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, ViewController, Events } from 'ionic-angular';
import { NotificaionPage } from '../notificaion/notificaion';
import { HelperProvider } from '../../providers/helper/helper';
import { ApiservicesProvider } from '../../providers/apiservices/apiservices';
import { LoginPage } from '../login/login';
// page of payslips details 
@Component({
  selector: 'page-attendance',
  templateUrl: 'attendance.html',
})
export class AttendancePage {
pagename:any
id:any
workinghours:any
totaldelayhours:any
totaldelayhoursval:any
totalovertime:any
totalovertimeval:any
hide:any=false
show:any=true
salaryarray:any=[]
bounes:any
penalites:any
show1:any=true
langDirection:any
  constructor(public event:Events,public ViewCtrl:ViewController,public menue:MenuController,public helper:HelperProvider,public ApiservicesProvider:ApiservicesProvider,public navCtrl: NavController, public navParams: NavParams) {
   this.pagename= this.navParams.get("Page")
   this.workinghours= this.navParams.get("Working")
   this.totaldelayhours= this.navParams.get("DelayHours")
   this.totaldelayhoursval= this.navParams.get("DelayValues")
   this.totalovertime= this.navParams.get("OvertimeHours")
   this.totalovertimeval= this.navParams.get("Overtimeval")
   this.bounes= this.navParams.get("bounes")
   this.penalites= this.navParams.get("penalties")
   if (this.helper.currentLang == 'ar')
   {
     this.langDirection = "rtl";
   }
   else{
     this.langDirection = "ltr";
   }
   if(this.pagename=="SalaryPage")
   {
    this.id= this.navParams.get("Id")
   this.hide=true
   this.show=false
   this.show1=true
    this.ApiservicesProvider.ReadEmployeeDueCuts(this.id,this.helper.lang_direction,(data)=>this.success(data),(data)=>this.fail(data))
   }
   else if (this.pagename=="Bounes")
   {
    this.show=true
    this.hide=true
    this.show1=false
   }
  }
  success(data)
  {
  this.salaryarray=data
  }
  fail(data)
  {
    if (JSON.stringify(data.status) == '401') {
      // UnAuthorized
      this.event.publish("login","");
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad AttendancePage');
  }
  opennotificaion()
  {
    this.navCtrl.push(NotificaionPage);
  }
  openmenu()
  {
    this.menue.open()
  }
}
