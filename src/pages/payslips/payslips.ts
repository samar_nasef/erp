import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Platform, ViewController, Events } from 'ionic-angular';
import { NotificaionPage } from '../notificaion/notificaion';
import { TranslateService } from '@ngx-translate/core';
import { HelperProvider } from '../../providers/helper/helper';
import { ApiservicesProvider } from '../../providers/apiservices/apiservices';
import { Storage } from '@ionic/storage';
import { DatePipe } from '@angular/common';
import { AttendancePage } from '../attendance/attendance';
import { LoginPage } from '../login/login';




@Component({
  selector: 'page-payslips',
  templateUrl: 'payslips.html',
})
export class PayslipsPage {
  HourPay:any;
  NetSalary:any;
  workinghours:any
  totaldelayhours:any
  totaldelayhoursval:any
  totalovertime:any
  totalovertimeval:any
  hidedata:any=false
  startDate:any;
  array:any=[]
  EndDate:any;
  id:any
  TotalBounses:any;
  TotalPenalties:any;
  Loans:any;
  dateList: any[] = [];
  hide:any=false
  show:any=true
  hideoption:any=true
  dis1:any
  Date:any;
  arr:any=[]
  date:any
  langDirection:any
  constructor(public event:Events,public navCtrl: NavController, public navParams: NavParams
    , public ApiservicesProvider: ApiservicesProvider
    , public helper: HelperProvider
    , private toastCtrl: ToastController
    , public storage: Storage
    , public translate: TranslateService
    , public datepipe: DatePipe,
    public platform:Platform,
    public ViewCtrl:ViewController
  ) {
    if (this.helper.currentLang == 'ar')
    {
      this.translate.use('ar');
      this.helper.currentLang = 'ar';
      this.translate.setDefaultLang('ar');
      this.helper.lang_direction = 'rtl';
      this.langDirection = "rtl";
      this.platform.setDir('rtl',true)
    }
    else{
     
      this.translate.use('en');
      this.helper.currentLang = 'en';
      this.translate.setDefaultLang('en');
      this.helper.lang_direction = 'ltr';
      this.langDirection = "ltr";
      this.platform.setDir('ltr',true)
    }
    // get All Dates IDs  from Api 
    storage.get("EmpId").then(val =>{
      if(val){
        this.ApiservicesProvider.getAllSalaryDates(val,

      this.helper.lang_direction
      , (data) => this.successCallbackDates(data)
      , (data) => this.failureCallbackDates(data))
      }
    })
    

   

  }

successCallbackDates(data) {
  console.log("date IDs");
 
  console.log(JSON.stringify(data));
  this.dateList = data;
  this.arr.push(this.dateList[0])
this.arr.forEach(element => {
  this.date=element.Text
  this.Date=element.Id
  // get all employee salary for this date
  this.storage.get('EmpId').then((val) => {
    this.ApiservicesProvider.getEmployeeSalary(
      this.Date
      ,val
      ,this.helper.lang_direction
      , (data) => this.successCallbacksalary(data)
      , (data) => this.failureCallbacksalary(data))  
      
    })
});

  
}
getdata(){
  this.hideoption=true
  this.storage.get('EmpId').then((val) => {
    this.ApiservicesProvider.getEmployeeSalary(
      this.Date
      ,val
      ,this.helper.lang_direction
      , (data) => this.successCallbacksalary(data)
      , (data) => this.failureCallbacksalary(data))  
      
    })

}
failureCallbackDates(data) {
  console.log(" fail date");
  console.log(status);
  if (JSON.stringify(data.status) == '401') {
    // UnAuthorized
    this.event.publish("login","");
  }

}

successCallbacksalary(data) {
  console.log("success Callback");
  console.log(JSON.stringify(data));
  this.array=data
   
if(this.array.length==0)
{
  this.hidedata=true
  const toast = this.toastCtrl.create({
    message: this.translate.instant('nosalary'),
    duration: 3000

  });
  toast.present();
}
else{
  this.hidedata=false
  this.array.forEach(element => {
    this.Loans=element.TotalLoans
  this.HourPay =element.HourPay;
  this.HourPay=this.HourPay.toFixed(2)
  this.NetSalary = element.NetSalary;
 this.NetSalary=this.NetSalary.toFixed(2)
this.TotalBounses=element.TotalBounses
this.TotalPenalties=element.TotalPenalties
this.workinghours=element.TotalWorkedHours
this.totaldelayhours=element.TotalDelayHours
this.totaldelayhoursval=element.TotalDelayValue
this.totalovertime=element.TotalOverTimeHours
this.totalovertimeval=element.TotalOverTimeValue
this.id=element.Id
  });
}
  
  
}
failureCallbacksalary(data) {
  console.log("failure Callback");
  console.log(status);
  if (JSON.stringify(data.status) == '401') {
    // UnAuthorized
    this.event.publish("login","");
  }

}
  ionViewDidLoad() {
    console.log('ionViewDidLoad PayslipsPage');
  }
  opennotificaion()
  {
    this.navCtrl.push(NotificaionPage);
  }

  openbounes()
  {
    this.hide=true
    this.show=false

  }
  openattend(page)
  {

    if(this.array.length != 0)
    {
    this.navCtrl.push(AttendancePage,{Page:page,bounes:this.TotalBounses,penalties:this.TotalPenalties,Id:this.id,Working:this.workinghours,DelayHours:this.totaldelayhours,DelayValues:this.totaldelayhoursval,OvertimeHours:this.totalovertime,Overtimeval:this.totalovertimeval})
  }
}


}
