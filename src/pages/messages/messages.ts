import { Component, ReflectiveInjector } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, ToastController, AlertController } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { TranslateService } from '@ngx-translate/core';
import { Platform } from 'ionic-angular/platform/platform';
import { NotificaionPage } from '../notificaion/notificaion';
import { ApiservicesProvider } from '../../providers/apiservices/apiservices';
import { Storage } from '@ionic/storage/dist/storage';
import { ViewPage } from '../view/view';
import { ReplayPage } from '../replay/replay';
import { AddmessagePage } from '../addmessage/addmessage';

@Component({
  selector: 'page-messages',
  templateUrl: 'messages.html',
})
export class MessagesPage {
  langDirection:any
  myInput:any
  messages:any=[]
  index:any=1
  show:any=true
  total:any
  unread:any
  constructor(public toastCtrl:ToastController,public alertCtrl:AlertController,public toastctrl:ToastController,public storage:Storage,public provider:ApiservicesProvider,public menu:MenuController,public helper:HelperProvider,public translate:TranslateService,public platform:Platform,public navCtrl: NavController, public navParams: NavParams) {
 
    if (this.helper.currentLang == 'ar')
    {
      this.translate.use('ar');
      this.helper.currentLang = 'ar';
      this.translate.setDefaultLang('ar');
      this.helper.lang_direction = 'rtl';
      this.langDirection = "rtl";
      this.platform.setDir('rtl',true)
    }
    else{
     
      this.translate.use('en');
      this.helper.currentLang = 'en';
      this.translate.setDefaultLang('en');
      this.helper.lang_direction = 'ltr';
      this.langDirection = "ltr";
      this.platform.setDir('ltr',true)
    }
  }
  onInput(input)
  {
    if(input=="" || input==" ")
  {
    
    this.storage.get("EmpId").then((val) => {
      if (val) {
    this.provider.messages(1,this.index,"",val,this.platform.isRTL ? 'rtl' : 'ltr',(data)=>{
      this.messages=data
    
    },(data)=>{
    })
  }
})
  }
else{
  this.storage.get("EmpId").then((val) => {
    if (val) {
  this.provider.messages(1,this.index,input,val,this.platform.isRTL ? 'rtl' : 'ltr',(data)=>{
    this.messages=data
  
  },(data)=>{
    console.log("fail")
  })
}
})
  }
  
  }
  onCancel()
  {
       
    this.storage.get("EmpId").then((val) => {
      if (val) {
    this.provider.messages(1,this.index,"",val,this.platform.isRTL ? 'rtl' : 'ltr',(data)=>{
      this.messages=data
      if(this.messages.length>0)
      {
        this.show=true

      }
      else{
        this.show=false
      }
    },(data)=>{
      console.log("fail")
    })
  }
})
  }
  ionViewDidLoad() {
    
    this.storage.get("EmpId").then((val) => {
      if (val) {
        //get numbers of readed or unreaded msgs
        this.provider.listnumbers(val,(data)=>{
          this.total=data.TotalMessagesCount
          this.unread=data.UnreadMessagesCount
        },(data)=>{
        })
        // to get all messages
    this.provider.messages(1,this.index,"",val,this.platform.isRTL ? 'rtl' : 'ltr',(data)=>{
      this.messages=data
      if(this.messages.length>0)
      {
        this.show=true

      }
      else{
        this.show=false
      }
    },(data)=>{
      console.log("fail")
    })
  }
      })
  }
  opennotificaion() {
    this.navCtrl.push(NotificaionPage)
  }
  openmenu() {
    this.menu.open()
  }
  // paging to get more messages
  doInfinite(ev)
  {
    setTimeout(() => {
    if(this.index>=0)
    {
      this.index+=1
    this.storage.get("EmpId").then((val) => {
      if (val) {
    this.provider.messages(1,this.index,"",val,this.platform.isRTL ? 'rtl' : 'ltr',(data)=>{
     console.log(data)
      if(data.length>0)
      {
        data.forEach(element => {
          this.messages.push(element)

        });
        
        ev.complete();
      }
      else{
        this.index=-1;
        this.offsetToast();
        ev.complete();
      }
    },(data)=>{
      ev.complete();
    })
  }
      })
    }
    else { ev.complete(); };
  }, 500);
  }
  offsetToast () {
    let toast = this.toastctrl.create({
    message: this.translate.instant('noti'),
    duration: 2000,
    position: 'bottom'
    });
    toast.present();
    }
    // do refresh for page
    doRefresh(ev)
    {
      this.index=1
      this.storage.get("EmpId").then((val) => {
        if (val) {
          this.provider.listnumbers(val,(data)=>{
            this.total=data.TotalMessagesCount
            this.unread=data.UnreadMessagesCount
          },(data)=>{
          })
      this.provider.messages(1,this.index,"",val,this.platform.isRTL ? 'rtl' : 'ltr',(data)=>{
        this.messages=data
       
        if(this.messages.length>0)
        {
          this.show=true
  
        }
        else{
          this.show=false
        }
      },(data)=>{
        console.log("fail")
      })
    }

        })
        ev.complete();

    }
    // open ad message page to add message
    addmessgae()
    {
      this.navCtrl.push(AddmessagePage)
    }
    // to accept msg and ask for confirmation from user before accept it
    accept(data,id)
    {
  let alert = this.alertCtrl.create({
    title: this.translate.instant('accept1'),
    buttons: [
      {
        text: this.translate.instant('cancel'),
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: this.translate.instant('ok'),
        handler: () => {
       this.help1(id)
        }
      }
    
  ]
})
alert.present()
}
help1(id)
{
  this.storage.get("EmpId").then((val) => {
    if (val) {
this.provider.AcceptReject(id,val,this.platform.isRTL ? 'rtl' : 'ltr',true,(data)=>{this.success(data)},(data)=>{
})
    }
  })
}
reject(data,id)
{
// to reject msg and ask for confirmation from user to reject it
  let alert = this.alertCtrl.create({
    title: this.translate.instant('reject1'),
    buttons: [
      {
        text: this.translate.instant('cancel'),
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: this.translate.instant('ok'),
        handler: () => {
        this. help(id)
        
        }
      }
    
  ]
})
alert.present()

}
   help(id)
   {
    this.storage.get("EmpId").then((val) => {
      if (val) {
  this.provider.AcceptReject(id,val,this.platform.isRTL ? 'rtl' : 'ltr',false,(data)=>{this.success(data)},(data)=>{
  })
      }
    })
   } 
    success(data)
    {
if(data.Result==false)
{
  const toast = this.toastCtrl.create({
    message: data.ErrorMessage,
    duration: 3000
  });
  toast.present();
}
else{
      this.storage.get("EmpId").then((val) => {
        if (val) {
          this.provider.listnumbers(val,(data)=>{
            this.total=data.TotalMessagesCount
            this.unread=data.UnreadMessagesCount
          },(data)=>{
          })
      this.provider.messages(1,this.index,"",val,this.platform.isRTL ? 'rtl' : 'ltr',(data)=>{
        this.messages=data
        if(this.messages.length>0)
        {
          this.show=true
    
        }
        else{
          this.show=false
        }
      },(data)=>{
      })
    }
        })
    }
  }
  // view messages and its replays
    view(id)
    {
this.storage.get("EmpId").then((val) => {
this.provider.viewmessages(id,val,this.platform.isRTL ? 'rtl' : 'ltr',(data)=>{
  this.navCtrl.push(ViewPage , {data: data})
},(data)=>{
  let toast = this.toastctrl.create({
    message: data.message,
    duration: 2000,
    position: 'bottom'
    });
    toast.present();
})
})
    }
    // to open replay page to replay for msg
    replay(id,name)
    {
this.navCtrl.push(ReplayPage,{id:id,name:name})
    }
}
