import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, MenuController,Events, Platform } from 'ionic-angular';
import { ApiservicesProvider } from '../../providers/apiservices/apiservices';
import { HelperProvider } from '../../providers/helper/helper';
import { ToastController } from 'ionic-angular';
import { DatePipe } from '@angular/common';
import { Storage } from '@ionic/storage';
import { RequestsPage } from '../requests/requests';
import { DatePicker } from '@ionic-native/date-picker';
import { TranslateService } from '@ngx-translate/core';
import { TabsPage } from '../tabs/tabs';
import * as Moment from 'moment';
import { LoginPage } from '../login/login';


@Component({
  selector: 'page-outside-req',
  templateUrl: 'outside-req.html',
})
export class OutsideReqPage {
  project: any;
  isReadonly:any=true
  date:any=new Date()
  date1:any=new Date()
  status: number = 1;
  from: any = this.translate.instant('date');
  to: any=this.translate.instant('date');
  location: any;
  note: any;
  langDirection:any
  IDList: any[] = [];
  durationList: any[] = [];
  senddate:any
  page:any
  hide1:any="true"
  appear:any
  dis:any="false"
  hide:any="true"
  dis1:any="false"
  to1:any
  array:any=[]
  from1:any
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public event:Events
    , public ApiservicesProvider: ApiservicesProvider
    , public helper: HelperProvider
    ,public datepipe: DatePipe
    , private toastCtrl: ToastController
    , public storage: Storage
    , private datePicker: DatePicker
    , public translate: TranslateService,
    public platform:Platform,
    public navctrl:NavController,
    public ViewCtrl:ViewController,
    public menue:MenuController
  ) {
    this.senddate=this.navParams.get("date")
    this.page=this.navParams.get("Page")
    if(this.page=="home")
    {
      if(this.status==2||this.status==3||this.status==4)
      {
      this.to1=this.datepipe.transform(this.senddate, 'MM-dd-yyyy')
      this.from1=this.datepipe.transform(this.senddate, 'MM-dd-yyyy')
      this.to=this.to1
      this.from=this.from1
      this.dis="true"
      this.hide="false"
      this.hide1="false"
      this.dis1="true"
      }
      else if (this.status==1)
      {
        this.to1=this.datepipe.transform(this.senddate, 'MM-dd-yyyy')
        this.from1=this.datepipe.transform(this.senddate, 'MM-dd-yyyy')
        this.to=this.to1
        this.from=this.from1
        this.dis="false"
        this.dis1="true"
        this.hide1="false"
      this.hide="true"
      }
    }
    if (this.helper.currentLang == 'ar')
    {
      this.translate.use('ar');
      this.helper.currentLang = 'ar';
      this.translate.setDefaultLang('ar');
      this.helper.lang_direction = 'rtl';
      this.langDirection = "rtl";
    }
    else{
     
      this.translate.use('en');
      this.helper.currentLang = 'en';
      this.translate.setDefaultLang('en');
      this.helper.lang_direction = 'ltr';
      this.langDirection = "ltr";
    }
  
    // get All projects IDs  from Api 
    this.ApiservicesProvider.getProjectsID(

      this.helper.lang_direction
      , (data) => this.successCallbackProjects(data)
      , (data) => this.failureCallbackProjects(data))

  }
  changeperiod()
  {
    if(this.page=="home")
    {
    if(this.status==2||this.status==3||this.status==4)
    {
      this.dis="true"
      this.hide="false"
      this.to1=this.datepipe.transform(this.senddate, 'MM-dd-yyyy')
      this.to=this.to1
  
    }
    else{

      this.from1=this.datepipe.transform(this.senddate, 'MM-dd-yyyy')
      this.from=this.from1
      this.dis="false"
      this.dis1="true"
      this.hide1="false"
    this.hide="true"
    }
  }
  else
  {
    // if status is half day or day from and to date are the same
    if(this.status==2||this.status==3||this.status==4)
    {
     this.to=this.from
    }
    else{
 // if it's period from and to date are different
      this.to=this.translate.instant('date');
      this.dis="false"
      this.dis1="false"
      this.hide1="true"
      this.hide="true"
    }

  }
 
  }
  getdate()
  {
    // get date 
    let localLang 
    let nowTxt
    let okTxt 
    let cancelTxt 
    var minmDate;
    var maxDate;
    if (this.platform.is('ios')) {
      
      minmDate =String(new Date()); 
      let d=new Date()
      let year =d.getFullYear()
      let month=d.getMonth()
      let day=d.getDay()
      maxDate = (new Date(year + 1,month,day)).valueOf();
    }
    else {
      minmDate =(new Date()).valueOf(); 
      let d=new Date()
      let year =d.getFullYear()
      let month=d.getMonth()
      let day=d.getDay()
      maxDate = (new Date(year + 1,month,day)).valueOf();
    }
    if (this.helper.currentLang == 'ar') {
      localLang = 'ar_eg';
      nowTxt = 'اليوم';
      okTxt = 'تم';
      cancelTxt = 'إلغاء'
      this.datePicker.show({
        date: this.date1,
        mode: 'date',
        // minDate: minmDate,
        maxDate: maxDate,
        okText: okTxt,
        cancelText: cancelTxt,
        todayText: nowTxt,
        locale: localLang,
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
      }).then(
        date =>{ 
          this.date1=date
          this.from=date.toLocaleDateString('ar-EG')
  
      this.from=this.from.replace("/","-").replace("/","-")
      this.from=this.datepipe.transform(date, 'MM-dd-yyyy');
      if(this.status==2 || this.status==3 || this.status==4 )
      { 
        this.to=this.from

      }
      
    },
        err => console.log('Error occurred while getting date: ', err)
      );
    }
    else{
      localLang = 'en_us';
      nowTxt = 'today';
      okTxt = 'okay';
      cancelTxt = 'cancel'
      this.datePicker.show({
        date: this.date1,
        mode: 'date',
        // minDate: minmDate,
        maxDate: maxDate,
        okText: okTxt,
        cancelText: cancelTxt,
        todayText: nowTxt,
        locale: localLang,
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
      }).then(
        date =>{ 
          this.date1=date
          this.from=date.toLocaleDateString('en-US')
  
      this.from=this.from.replace("/","-").replace("/","-")
      var dateData = this.from.split('-');
        var year = dateData [0];
        var month = dateData [1];
        var day = dateData [2];
        this.from = day + "-" + month + "-" + year;
        this.from=this.datepipe.transform(date, 'MM-dd-yyyy');
      if(this.status==2 || this.status==3 || this.status==4 )
      {
        this.to=this.from

      }
      
    },
        err => console.log('Error occurred while getting date: ', err)
      );
    }
   
  }
  getdate1()
  {
    let localLang 
    let nowTxt
    let okTxt 
    let cancelTxt 
    var minmDate;
    var maxDate;
    if (this.platform.is('ios')) {
      
      minmDate =String(new Date()); 
      let d=new Date()
      let year =d.getFullYear()
      let month=d.getMonth()
      let day=d.getDay()
      maxDate = (new Date(year + 1,month,day)).valueOf();
    }
    else {
      minmDate =(new Date()).valueOf(); 
      let d=new Date()
      let year =d.getFullYear()
      let month=d.getMonth()
      let day=d.getDay()
      maxDate = (new Date(year + 1,month,day)).valueOf();
    } 
    if (this.helper.currentLang == 'ar') {
      localLang = 'ar_eg';
      nowTxt = 'اليوم';
      okTxt = 'تم';
      cancelTxt = 'إلغاء'
      this.datePicker.show({
        date: this.date,
        mode: 'date',
        // minDate: minmDate,
        maxDate: maxDate,
        okText: okTxt,
        cancelText: cancelTxt,
        todayText: nowTxt,
        locale: localLang,
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
      }).then(
        date =>{ 
          this.date=date
          this.to=date.toLocaleDateString('ar-EG')
    
        this.to=this.to.replace("/","-").replace("/","-")
        this.to=this.datepipe.transform(date, 'MM-dd-yyyy');
          if(this.status==2 || this.status==3 || this.status==4 )
    {
     
     this.from=this.to
    
    }
      
    },
        err => console.log('Error occurred while getting date: ', err)
      );
    }
    else{
      localLang = 'en';
      nowTxt = 'today';
      okTxt = 'okay';
      cancelTxt = 'cancel'
      this.datePicker.show({
        date: this.date,
        mode: 'date',
        okText: okTxt,
        cancelText: cancelTxt,
        todayText: nowTxt,
        locale: localLang,
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
      }).then(
        date =>{ 
          this.date=date
          this.to=date.toLocaleDateString('en-US')
        this.to=this.to.replace("/","-").replace("/","-")
        this.to=this.datepipe.transform(date, 'MM-dd-yyyy');
        if(this.status==2 || this.status==3 || this.status==4 )
    {
     
     this.from=this.to
    
    }
      
    },
        err => console.log('Error occurred while getting date: ', err)
      );
    }
    
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad OutsideReqPage');
  }

  successCallbackProjects(data) {
    console.log("projects IDs");
    console.log(data);
    this.IDList = data;
    this.array.push(this.IDList[0])
    this.array.forEach(element => {
      this.appear=element.Name
      this.project=element.Id
    });

  }
  failureCallbackProjects(data) {
    if (JSON.stringify(data.status) == '401') {
      // UnAuthorized
      this.event.publish("login","");
    }
    console.log(status);

  }

  openmenu()
  {
    this.menue.open()

  }

  //======================== save Event ========================
  frommethod()
  {
   
  }
  tomethod()
  {
    if(this.status==2 || this.status==3 || this.status==4 )
    {
     
     this.from=this.to
    
    }
  }
  saveOutsideReq() {
    if(this.status== 1)
    {
      if(this.from<=this.to)
  {

  
    if (this.project && this.status && this.from && this.to && this.location ) {
      this.storage.get('EmpId').then((val) => {
        this.ApiservicesProvider.SaveEvent(

          val
          , this.helper.lang_direction
          , this.helper.EventType
          , this.status
          , this.from
          , this.to
          , this.location
          , this.project
          , this.note
          , (data) => this.successCallbackSave(data)
          , (data) => this.failureCallbackSave(data))


      })
    } else {
      console.log("you must enter data ");
      const toast = this.toastCtrl.create({
        message: this.translate.instant('msgEnterUsername'),
        duration: 3000
      });
      toast.present();
    }
  }
  else{
    const toast = this.toastCtrl.create({
      message: this.translate.instant('fromandto'),
      duration: 3000
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
}
else if(this.status==2 || this.status==3 || this.status==4)
    {
    if(this.from==this.to)
  {
    if (this.project && this.status && this.from && this.to && this.location ) {
      this.storage.get('EmpId').then((val) => {
        this.ApiservicesProvider.SaveEvent(
          val
          , this.helper.lang_direction
          , this.helper.EventType
          , this.status
          , this.from
          , this.to
          , this.location
          , this.project
          , this.note
          , (data) => this.successCallbackSave(data)
          , (data) => this.failureCallbackSave(data))


      })
    } else {
      console.log("you must enter data ");
      const toast = this.toastCtrl.create({
        message: this.translate.instant('msgEnterUsername'),
        duration: 3000
      });
      toast.present();
    }
  }
  else{
    const toast = this.toastCtrl.create({
      message: this.translate.instant('oneday'),
      duration: 3000
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
}
  }
  successCallbackSave(data) {
    console.log(data);
    if (data.Result == true) {
      const toast = this.toastCtrl.create({
        message: this.translate.instant('DataSavedSuccessfully'),
        duration: 3000
      });
      toast.onDidDismiss(() => {
        console.log('Dismissed toast');
      });

      toast.present();
      this.project = " ";
      this.status = 1;
      this.from = " ";
      this.location = " ";
      this.note = " ";
      this.ViewCtrl.dismiss()

    }
    else {
      console.log("Error, Not Saved");
      const toast = this.toastCtrl.create({
        message: data.ErrorMessage,
        duration: 3000
      });
      toast.present();
    }


  }
  failureCallbackSave(data) {
    if (JSON.stringify(data.status) == '401') {
      // UnAuthorized
      this.event.publish("login","");
    }
    console.log(status);
  }

  opencalender() {
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date => console.log('Got date: ', date),
      err => console.log('Error occurred while getting date: ', err)
    );
  }

}
