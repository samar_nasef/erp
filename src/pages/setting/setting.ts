import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { TranslateService } from '@ngx-translate/core';
import { Platform } from 'ionic-angular/platform/platform';
import { NotificaionPage } from '../notificaion/notificaion';
@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {
  langDirection:any
  constructor(public helper:HelperProvider,public translate:TranslateService,public platform:Platform,public navCtrl: NavController, public navParams: NavParams) {
    if (this.helper.currentLang == 'ar')
    {
      this.translate.use('ar');
      this.helper.currentLang = 'ar';
      this.translate.setDefaultLang('ar');
      this.helper.lang_direction = 'rtl';
      this.langDirection = "rtl";
      this.platform.setDir('rtl',true)
    }
    else{
      this.translate.use('en');
      this.helper.currentLang = 'en';
      this.translate.setDefaultLang('en');
      this.helper.lang_direction = 'ltr';
      this.langDirection = "ltr";
      this.platform.setDir('ltr',true)
    }
  }

  ionViewDidLoad() {
  }
  opennotificaion()
  {
    this.navCtrl.push(NotificaionPage)
  }

}
