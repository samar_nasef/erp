
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HelperProvider } from '../../providers/helper/helper';
import { TranslateService } from '@ngx-translate/core';
import { Platform } from 'ionic-angular/platform/platform';
import { Storage } from '@ionic/storage';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import { HomePage } from '../home/home';
import { TabsPage } from '../tabs/tabs';
import { Cordova } from '@ionic-native/core';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { ApiservicesProvider } from '../../providers/apiservices/apiservices';
import { Device } from '@ionic-native/device';
import 'rxjs/add/operator/catch';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  // providers: [Device]  
})
export class LoginPage {
  username: any;
  accesstoken: any
  devID: any
  pass: any;
  MobId = this.device.uuid;
  loginForm;
  usernameTxt;
  langDirection: string;
  serverurl: any;
  submitAttempt = false;
  id: number;
  password: number;
  dir: any
  isValid: any = true
  options: BarcodeScannerOptions
  encodetext: any = ''
  encodedata: any = {}
  scanneddata: any = {}
  re: any
  lang_direction: any

  constructor(public event: Events, public toastCtrl: ToastController, public scanner: BarcodeScanner
    , public storage: Storage, public http: HttpClient, public helper: HelperProvider
    , public translate: TranslateService, public platform: Platform,
    public formBuilder: FormBuilder, public navCtrl: NavController,
    public navParams: NavParams,
    public device: Device,
    public ApiservicesProvider: ApiservicesProvider) {
    this.storage.get("LanguageApp").then((val) => {
      if (val == null) {
        // if offline lang value not saved get mobile language.
        var userLang = navigator.language.split('-')[0];
        // check if mobile lang is arabic.
        if (userLang == 'ar') {
          this.translate.use('ar');
          this.helper.currentLang = 'ar';
          this.translate.setDefaultLang('ar');
          this.helper.lang_direction = 'rtl';
          this.langDirection = "rtl";
          this.platform.setDir('rtl', true)
          this.storage.set("LanguageApp", "ar");
          
        }
        else {
          // if mobile language isn't arabic then make application language is English.
          this.translate.setDefaultLang('en');
          this.helper.currentLang = 'en'
          this.translate.use('en');
          this.helper.lang_direction = 'ltr';
          this.langDirection = "ltr";
          this.platform.setDir('ltr', true)
          this.storage.set("LanguageApp", "en");
        }
      }
      //If application language is saved offline and it is arabic.
      else if (val == 'ar') {
        this.translate.use('ar');
        this.helper.currentLang = 'ar';
        this.translate.setDefaultLang('ar');
        this.helper.lang_direction = 'rtl';
        this.langDirection = "rtl";
        this.platform.setDir('rtl', true);
        this.dir="right"
      }
      //If application language is saved offline and it is English.
      else if (val == 'en') {
        this.translate.setDefaultLang('en');
        this.translate.use('en');
        this.helper.currentLang = 'en'
        this.langDirection = "ltr";
        this.helper.lang_direction = 'ltr';
        this.platform.setDir('ltr', true)
        this.dir="left"
      }
    });
    this.event.subscribe("direction", (dir) => {
      this.langDirection=dir
    });
    this.lang_direction = helper.lang_direction
    this.loginForm = formBuilder.group({
      password: ['', Validators.required],
      serverurl: ['', Validators.required,],
      id: ['', Validators.compose([Validators.required])],
    });
    this.devID = device.uuid
    this.helper.DeviceId= device.uuid
    storage.get("url").then((val) => {
      if (val) {
        this.serverurl = val;
      }
    });
  }
  //scan qr code
  scan() {
   
    let options: BarcodeScannerOptions =
      {
        preferFrontCamera: false, 
        showFlipCameraButton: false, 
        showTorchButton: true, 
        torchOn: false, 
        prompt: "Place a barcode inside the scan area", 
        resultDisplayDuration: 500,
        disableAnimations: true, 
        disableSuccessBeep: false 

      }
    this.scanner.scan(options).then((data) => {
      if (!data) {

        console.log(data);
      }
      else {
        this.serverurl = data.text;
      }

    }, (err) => {
      console.log("Error: "+err);

    })

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  // login to app
  login() {
    if (this.username && this.pass && this.serverurl) {
      if(this.pass.length < 6)
      {
        const toast = this.toastCtrl.create({
          message: this.translate.instant('incorrect'),
          duration: 3000
        });
        toast.present();
      }
      //123414345
      else
      {
      this.helper.serviceurl = this.serverurl;
      this.ApiservicesProvider.login(this.username, this.pass,this.helper.DeviceId , this.helper.registration,this.helper.device_type,this.helper.grant_Type, (data) => this.successCallBackLogin(data), (data) => this.failureCallBackLogin(data));

      }  
    }
    else {
      const toast = this.toastCtrl.create({
        message: this.translate.instant('msgEnterUsername'),
        duration: 3000
      });
      toast.present();
    }
  }
  successCallBackLogin(data) {
    console.log(data);
    var jsonObject = JSON.parse(data);
    if (jsonObject.access_token) {
      this.storage.set("UserPass", this.pass);
      this.storage.set("url", this.serverurl);
      this.storage.set("access_token", jsonObject.access_token);
      this.storage.set("IsAdmin", jsonObject.IsAdmin);
      
      // this.storage.set("AllowSensor", jsonObject.AllowSensor); //0 fingerprint not necessary , 1 fingerprint necessary
      // this.storage.set("AllowOfflineFinger", jsonObject.AllowOfflineFIngerPrint); //0 offlinefingerprint not available , 1 offlinefingerprint available
      
      console.log('IsAdmin: ' + jsonObject.IsAdmin);
      this.accesstoken = jsonObject.access_token
      this.helper.serviceurl = this.serverurl;
      this.helper.Empid = this.username;
      this.helper.accesstoken = jsonObject.access_token;
      this.ApiservicesProvider.checkvalidemployee(this.username, (data) => {
        if(JSON.stringify(data)=="true")
        {   
      this.storage.set("EmpId", this.username).then(() => {
        this.getAllemployees(data => this.getsuccess(data, jsonObject.IsAdmin), data => this.getfail(data))
        this.navCtrl.setRoot(TabsPage);
      });
    }
  
  else{
    const toast = this.toastCtrl.create({
      message: this.translate.instant('validation'),
      duration: 3000
    });
    toast.present();
  }
  
 
}, (data) => {
  if (JSON.stringify(data.status) == '401') {
    // UnAuthorized
    this.event.publish("login","");
  }
  console.log(JSON.stringify(data))


})
  
    }
  }
  getsuccess(data, isAdmin) {

    console.log("lllllllllll" + JSON.stringify(data))
    this.storage.set("name", data.EmployeeName)
    this.storage.set("id", data.Id)
    this.storage.set("photo", data.PhotoUrl)
    this.event.publish("userinfo", data.EmployeeName, data.Id, data.PhotoUrl, isAdmin,this.langDirection);
  }
  getfail(data) {

  }

  failureCallBackLogin(err) {
    if (JSON.stringify(err.status) == '401') {
      // UnAuthorized
      this.event.publish("login","");
    }
    else{
    console.log(err);
    if (err && err.error && err.error.error_description) {
      const toast = this.toastCtrl.create({
        message: err.error.error_description,
        duration: 3000
      });
      toast.present();
    }
    else {
      const toast = this.toastCtrl.create({
        message:err.message,
        duration: 3000
      });
      toast.present();
    }
  }
  }
  // change app language
  changelang() {
    if (this.helper.currentLang == 'ar') {
      this.translate.setDefaultLang('en');
      this.translate.use('en');
      this.helper.currentLang = 'en';
      this.helper.lang_direction = 'ltr';
      this.langDirection = "ltr";
      this.storage.set("LanguageApp", "en");
      this.platform.setDir('ltr', true)
    }
    else {
      this.translate.use('ar');
      this.helper.currentLang = 'ar';
      this.translate.setDefaultLang('ar');
      this.helper.lang_direction = 'rtl';
      this.langDirection = "rtl";
      this.storage.set("LanguageApp", "ar");

      this.platform.setDir('rtl', true)
     
    }
  }
  presentToast() {
    if (this.langDirection == 'rtl') {
      let toast = this.toastCtrl.create({
        message: ' اللينك الذي ادخلته غير صحيح',
        duration: 4000,
        position: 'bottom'
      });
      toast.present();

    } else {
      let toast = this.toastCtrl.create({
        message: ' Server usl is not correct',
        duration: 4000,
        position: 'bottom'
      });
      toast.present();
    }

  }
  presentToast2() {
    if (this.langDirection == 'rtl') {
      let toast = this.toastCtrl.create({
        message: 'من فضلك انتظر الموافقه من الادمن',
        duration: 4000,
        position: 'bottom'
      });
      toast.present();

    } else {
      let toast = this.toastCtrl.create({
        message: 'please wait admin comfirmation',
        duration: 4000,
        position: 'bottom'
      });
      toast.present();
    }

  }
  // get all employees
  getAllemployees(authSuccessCallback, authFailureCallback) {
    let serviceUrl;

    let headers = new HttpHeaders()
    let parameter = new HttpParams().set('EmpId', this.helper.Empid).set('Direction', this.helper.Direction);
    headers = headers.set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.accesstoken);//client_credentials

    serviceUrl = this.helper.serviceurl + '/api/HR/GetEmployeeProfile?EmpId=' + this.helper.Empid + '&Direction=' + this.helper.Direction;
    this.http.get(serviceUrl, { headers: headers })
      .subscribe(
        data => {

          authSuccessCallback(data)
        },
        err => {
          authFailureCallback(err);
        }
      )
  }
}
