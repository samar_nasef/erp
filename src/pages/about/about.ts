import { Component } from '@angular/core';
import { NavController,MenuController } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { TranslateService } from '@ngx-translate/core';
import { Platform } from 'ionic-angular/platform/platform';
import { NotificaionPage } from '../notificaion/notificaion';
import { Storage } from '@ionic/storage';
import { ToastController } from 'ionic-angular';
import { PayslipsPage } from '../payslips/payslips';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  langDirection:any;
  password:any;

  constructor(public helper:HelperProvider
    ,public translate:TranslateService
    ,public platform:Platform
    ,public navCtrl: NavController
    ,public menu:MenuController
    , public storage: Storage
    , private toastCtrl: ToastController
     ) {
       //set language of page
    if (this.helper.currentLang == 'ar')
    {
      this.translate.use('ar');
      this.helper.currentLang = 'ar';
      this.translate.setDefaultLang('ar');
      this.helper.lang_direction = 'rtl';
      this.langDirection = "rtl";
      this.platform.setDir('rtl',true)
    }
    else{
     
      this.translate.use('en');
      this.helper.currentLang = 'en';
      this.translate.setDefaultLang('en');
      this.helper.lang_direction = 'ltr';
      this.langDirection = "ltr";
      this.platform.setDir('ltr',true)
    }
  }
  //open notification page
  opennotificaion()
  {
    this.navCtrl.push(NotificaionPage)
  }
// take password and check if it true
  checkPassword(){
     if(this.password){
     this.storage.get('UserPass').then((val) => {
    if(this.password.length<6)
    {
      const toast = this.toastCtrl.create({
        message:this.translate.instant("incorrect"),
        duration: 3000
      });
      
      toast.present();
    }
       else if(val == this.password){
          console.log("Password  is correct ")
          this.navCtrl.push(PayslipsPage);
          this.password="";

        }else{
         console.log(" Password  is Incorrect ! ");
         const toast = this.toastCtrl.create({
           message:this.translate.instant("msgPayslips"),
           duration: 3000
         });
         toast.present();
        }
 
     })  
   }else{
     console.log(" Password field is empty ! ");
     const toast = this.toastCtrl.create({
       message:this.translate.instant("msgPayslips2"),
       duration: 3000
     });
     toast.present();
   }
  }
  //open side menu
  openmenu() {
    this.menu.open()
  }
}
