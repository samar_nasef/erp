import { Component } from '@angular/core';
import { IonicPage,MenuController, NavController, NavParams, Platform, AlertController, ToastController ,Events, ViewController} from 'ionic-angular';
import { NotificaionPage } from '../notificaion/notificaion';
import { HelperProvider } from '../../providers/helper/helper';
import { TranslateService } from '@ngx-translate/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { OutsideReqPage } from '../outside-req/outside-req';
import { PermissionreqPage } from '../permissionreq/permissionreq';
import { OverTimeReqPage } from '../over-time-req/over-time-req';
import { LeavereqPage } from '../leavereq/leavereq';
import { DatePipe } from '@angular/common';

import { Storage } from '@ionic/storage';
import { ApiservicesProvider } from '../../providers/apiservices/apiservices';
import { LoginPage } from '../login/login';
@Component({
  selector: 'page-requests',
  templateUrl: 'requests.html',
})
export class RequestsPage {
  langDirection: any = this.helper.lang_direction;
  req: any = []
  states: any[]
  per: any[]
  overtime: any[]
  compare:any
  cat: string ="LEAVE" 
 page:any
 tap:any
  constructor(public menu:MenuController,public event:Events,public toastCtrl: ToastController, public alertCtrl: AlertController, public serve: ApiservicesProvider, public storage: Storage, public platform: Platform, public helper: HelperProvider, public http: HttpClient, public translate: TranslateService,
    public viewctrl:ViewController,public navCtrl: NavController, public navParams: NavParams, public datepipe: DatePipe) {
  this.page=this.navParams.get("Page")
  this.tap=this.navParams.get("tap")
  if(this.page=="notification")
  {
    this.cat=this.tap
  
  }
  }

  ionViewDidLoad() {
    this.langDirection == this.helper.Direction
    
  }
  ionViewWillEnter() {
   // get requests of different types
     if(this.cat=="LEAVE")
    {
     
      this.getRequests();

    }
    else if(this.cat=="PERMISSION")
    {
this.getPermission()
    }
    else if(this.cat=="OVERTIME")
    {
     
this.getOvertime()
    }
    else if(this.cat=="OUTSIDEOFFICE")
    {
      console.log("here")
     
      this.getoffState()

    }
  }
  opennotificaion() {
    this.navCtrl.push(NotificaionPage)
  }

  add() {
    // add a new outside request
    this.navCtrl.push(OutsideReqPage)
  }

  addLeave(page) {
 // add a new leave request
    if(page=="LEAVE")
    {
    this.navCtrl.push(LeavereqPage)
    }
    else if(page=="PERMISSION")
    {
      this.navCtrl.push(PermissionreqPage);

    }
    else if(page=="OVERTIME")
    {
      this.navCtrl.push(OverTimeReqPage);

    }
    else if(page=="OUTSIDEOFFICE")
    {
      this.navCtrl.push(OutsideReqPage)

    }
  }
// add a new outside request
  addOutside() {
    this.navCtrl.push(OutsideReqPage)
  }

  addOvertime() {
    this.navCtrl.push(OverTimeReqPage);
  }

  addPermission() {
    this.navCtrl.push(PermissionreqPage);
  }

  getRequests() {
    this.storage.get("access_token").then(val => {
      this.getLeaverequests(val, (data) => this.success(data), (data) => {  if (JSON.stringify(data.status) == '401') {
        // UnAuthorized
         this.event.publish("login","");
      } })
    })
  }

  success(data) {

    console.log(data);
    if (data) {
      for (let i = 0; i < data.length; i++) {
        data[i].StartDate = this.datepipe.transform(data[i].StartDate, 'dd-MM-yyyy');
        data[i].EndDate = this.datepipe.transform(data[i].EndDate, 'dd-MM-yyyy');
      }
    }
    for (let x = 0; x < data.length; x++) {
      data[x].hide = 0
    }
    this.req = data;
  }

  hideCard(index) {
    (this.states).splice(index, 1);
  }
  openmenu() {
    this.menu.open()
  }
  getPermission() {
    this.storage.get("access_token").then(val => {
      this.getPermissionrequests(val, (data) => this.persuccess(data), (data) => {   if (JSON.stringify(data.status) == '401') {
        // UnAuthorized
         this.event.publish("login","");
      }})
    })
  }

  persuccess(data) {

    console.log(data);
    if (data) {
      for (let i = 0; i < data.length; i++) {
        data[i].StartDate = this.datepipe.transform(data[i].StartDate, 'dd-MM-yyyy');
        data[i].EndDate = this.datepipe.transform(data[i].EndDate, 'dd-MM-yyyy');
      }
    }
    this.per = data;
  }

  getOvertime() {
    this.storage.get("access_token").then(val => {
      this.getOvertimerequests(val, (data) => this.oversuccess(data), (data) => {   if (JSON.stringify(data.status) == '401') {
        // UnAuthorized
         this.event.publish("login","");
      }})
    })
  }

  oversuccess(data) {

    console.log(data);
    if (data) {
      for (let i = 0; i < data.length; i++) {
        data[i].StartDate = this.datepipe.transform(data[i].StartDate, 'dd-MM-yyyy');
        data[i].EndDate = this.datepipe.transform(data[i].EndDate, 'dd-MM-yyyy');
      }
    }

    this.overtime = data;
  }

  getoffState() {
    console.log('start');

    this.storage.get("access_token").then(val => {
      console.log('start 2');

      this.getOffside(val, (data) => this.state(data), (data) => {  if (JSON.stringify(data.status) == '401') {
        // UnAuthorized
         this.event.publish("login","");
      } })
    })
  }

  state(data) {
    if (data) {
      for (let i = 0; i < data.length; i++) {
        data[i].StartDate = this.datepipe.transform(data[i].StartDate, 'dd-MM-yyyy');
        data[i].EndDate = this.datepipe.transform(data[i].EndDate, 'dd-MM-yyyy');
      }
    }
    this.states = data;
  }

  getLeaverequests(token, authSuccessCallback, authFailureCallback) {
    let serviceUrl = this.helper.serviceurl;
    let headers = new HttpHeaders()
    headers = headers.set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + token);
    if (this.langDirection == this.helper.Direction) {
      serviceUrl = serviceUrl + '/api/HR/ReadVacations?EmpId=' + this.helper.Empid + '&Direction=' + (this.platform.isRTL ? 'rtl' : 'ltr');

    }
    else {
      serviceUrl = serviceUrl + '/api/HR/ReadVacations?EmpId=' + this.helper.Empid + '&Direction=' + (this.platform.isRTL ? 'rtl' : 'ltr');

    }
    this.http.get(serviceUrl, { headers: headers })
      .subscribe(
        data => {

          authSuccessCallback(data)
        },
        err => {
          authFailureCallback(err);
        }
      )
  }

  getPermissionrequests(token, authSuccessCallback, authFailureCallback) {
    let serviceUrl = this.helper.serviceurl;

    let headers = new HttpHeaders()
    headers = headers.set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + token);
    if (this.langDirection == this.helper.Direction) {
      serviceUrl = serviceUrl + '/api/HR/ReadPermission?EmpId=' + this.helper.Empid + '&Direction=' + (this.platform.isRTL ? 'rtl' : 'ltr');

    }
    else {
      serviceUrl = serviceUrl + '/api/HR/ReadPermission?EmpId=' + this.helper.Empid + '&Direction=' + (this.platform.isRTL ? 'rtl' : 'ltr');

    }
    this.http.get(serviceUrl, { headers: headers })
      .subscribe(
        data => {

          authSuccessCallback(data)
        },
        err => {
          authFailureCallback(err);
        }
      )
  }

  getOvertimerequests(token, authSuccessCallback, authFailureCallback) {
    let serviceUrl = this.helper.serviceurl;

    let headers = new HttpHeaders()
    headers = headers.set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + token);
    if (this.langDirection == this.helper.Direction) {
      serviceUrl = serviceUrl + '/api/HR/ReadOverTimes?EmpId=' + this.helper.Empid + '&Direction=' + (this.platform.isRTL ? 'rtl' : 'ltr');

    }
    else {
      serviceUrl = serviceUrl + '/api/HR/ReadOverTimes?EmpId=' + this.helper.Empid + '&Direction=' + (this.platform.isRTL ? 'rtl' : 'ltr');

    }
    this.http.get(serviceUrl, { headers: headers })
      .subscribe(
        data => {

          authSuccessCallback(data)
        },
        err => {
          authFailureCallback(err);
        }
      )
  }

  getOffside(token, authSuccessCallback, authFailureCallback) {
    let serviceUrl = this.helper.serviceurl;

    let headers = new HttpHeaders()
    headers = headers.set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + token);
    if (this.langDirection == this.helper.Direction) {
      serviceUrl = serviceUrl + '/api/HR/ReadMissions?EmpId=' + this.helper.Empid + '&Direction=' + (this.platform.isRTL ? 'rtl' : 'ltr');

    }
    else {
      serviceUrl = serviceUrl + '/api/HR/ReadMissions?EmpId=' + this.helper.Empid + '&Direction=' + (this.platform.isRTL ? 'rtl' : 'ltr');

    }
    console.log(serviceUrl);
    this.http.get(serviceUrl, { headers: headers })
      .subscribe(
        data => {
          console.log(data);
          authSuccessCallback(data)
        },
        err => {
          authFailureCallback(err);
        }
      )
  }
  deleteevent(id) {

    let alert = this.alertCtrl.create({
      title: this.translate.instant('confirmMsg'),
      buttons: [
        {
          text: this.translate.instant('cancel'),
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: this.translate.instant('ok'),
          handler: () => {
            for(var i=0; i<this.req.length;i++)
            {
              if(this.req[i].EventId==id)
              {
                this.req.splice(i, 1);
                this.serve.RemoveEvent(id, (data) => this.successcallback(data), (data) => this.failuercallback(data))

              }
            }
           
          }
        }
      ]
    });
    alert.present();


  }
  successcallback(data) {

    let toast = this.toastCtrl.create({
      message: this.translate.instant('deleted'),
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
    console.log(JSON.stringify(data))
  }
  failuercallback(data) {
    if (JSON.stringify(data.status) == '401') {
      // UnAuthorized
       this.event.publish("login","");
    }
  }
  deleteevent1(id) {
    let alert = this.alertCtrl.create({
      title: this.translate.instant('confirmMsg'),
      buttons: [
        {
          text: this.translate.instant('cancel'),
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: this.translate.instant('ok'),
          handler: () => {
            for(var i=0; i<this.states.length;i++)
            {
              if(this.states[i].EventId==id)
              {
                this.states.splice(i, 1);
                this.serve.RemoveEvent(id, (data) => this.successcallback(data), (data) => this.failuercallback(data))

              }
            }


          }
        }
      ]
    });
    alert.present();


  }
  
  deleteevent2(id) {
    let alert = this.alertCtrl.create({
      title: this.translate.instant('confirmMsg'),
      buttons: [
        {
          text: this.translate.instant('cancel'),
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: this.translate.instant('ok'),
          handler: () => {
            for(var i=0; i<this.overtime.length;i++)
            {
              if(this.overtime[i].EventId==id)
              {
                this.overtime.splice(i, 1);
                this.serve.RemoveEvent(id, (data) => this.successcallback(data), (data) => this.failuercallback(data))

              }
            }


          }
        }
      ]
    });
    alert.present();


  }
  deleteevent3(id) {
    let alert = this.alertCtrl.create({
      title: this.translate.instant('confirmMsg'),
      buttons: [
        {
          text: this.translate.instant('cancel'),
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: this.translate.instant('ok'),
          handler: () => {
            for(var i=0; i<this.per.length;i++)
            {
              if(this.per[i].EventId==id)
              {
                this.per.splice(i, 1);
                this.serve.RemoveEvent(id, (data) => this.successcallback(data), (data) => this.failuercallback(data))

              }
            }

          }
        }
      ]
    });
    alert.present();


  }
  doRefresh(ev)
  {
    if(this.cat=="LEAVE")
    {
      
    this.storage.get("access_token").then(val => {
      this.getLeaverequests(val, (data) => this.success(data), (data) => {   if (JSON.stringify(data.status) == '401') {
        // UnAuthorized
         this.event.publish("login","");
      }})
    })
    ev.complete();
  }
  else if(this.cat=="OUTSIDEOFFICE")
{

  this.storage.get("access_token").then(val => {
    console.log('start 2');

    this.getOffside(val, (data) => this.state(data), (data) => {  if (JSON.stringify(data.status) == '401') {
      // UnAuthorized
       this.event.publish("login","");
    } })
  })
  ev.complete();
}
else if(this.cat=="OVERTIME")
{

  this.storage.get("access_token").then(val => {
    this.getOvertimerequests(val, (data) => this.oversuccess(data), (data) => { })
  })
  ev.complete();
}
else if(this.cat=="PERMISSION")
{
  this.storage.get("access_token").then(val => {
    this.getPermissionrequests(val, (data) => this.persuccess(data), (data) => {   if (JSON.stringify(data.status) == '401') {
      // UnAuthorized
       this.event.publish("login","");
    }})
  })
  ev.complete();
}
}

}
