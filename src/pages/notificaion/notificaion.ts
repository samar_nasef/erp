import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, ViewController, ToastController, Events } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { TranslateService } from '@ngx-translate/core';
import { Platform } from 'ionic-angular/platform/platform';
import { ApiservicesProvider } from '../../providers/apiservices/apiservices';
import { Storage } from '@ionic/storage';
import * as Moment from 'moment';
import { AboutPage } from '../about/about';
import { TabsPage } from '../tabs/tabs';
import { RequestsPage } from '../requests/requests';
import { LoginPage } from '../login/login';


@Component({
  selector: 'page-notificaion',
  templateUrl: 'notificaion.html',
})
export class NotificaionPage {
  langDirection:any
  notifications:any=[]
  show:any=true
  getNotification:any
  notificationid:any;
  constructor(public event:Events,public toastCtrl:ToastController,public ViewCtrl:ViewController,public menue:MenuController,public serve :ApiservicesProvider,public storage:Storage,public platform:Platform,public helper:HelperProvider,public translate:TranslateService,public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection =this.helper.Direction ;
     console.log(this.langDirection);
     
    if (this.helper.currentLang == 'ar')
    {
      this.translate.use('ar');
      this.helper.currentLang = 'ar';
      this.translate.setDefaultLang('ar');
      this.helper.lang_direction = 'rtl';
      this.langDirection = "rtl";
      this.platform.setDir('rtl',true)
    }
    else{
      this.translate.use('en');
      this.helper.currentLang = 'en';
      this.translate.setDefaultLang('en');
      this.helper.lang_direction = 'ltr';
      this.langDirection = "ltr";
      this.platform.setDir('ltr',true)
    }
    
      let empid;
      this.storage.get("EmpId").then((val) => {
        if (val != null) {
          empid=val
          // get list of notifications
            this.serve.getnewNotification(empid,(data)=>this.success(data),(data)=>this.fail(data))
               }
        })
    
    
  
  }
  opennotification(type)
  {
if(type=="1")
{
  // open payslips page
  this.navCtrl.setRoot(TabsPage).then(() => {
    this.navCtrl.push(AboutPage,{Page:"notification"})
    });
}if(type=="2")
{
  this.navCtrl.setRoot(TabsPage).then(() => {
    this.navCtrl.push(RequestsPage,{Page:"notification",tap:"LEAVE"})
    });
}
if(type=="3")
{
  this.navCtrl.setRoot(TabsPage).then(() => {
    this.navCtrl.push(RequestsPage,{Page:"notification",tap:"OUTSIDEOFFICE"})    });
}
if(type=="4")
{
  this.navCtrl.setRoot(TabsPage).then(() => {
    this.navCtrl.push(RequestsPage,{Page:"notification",tap:"PERMISSION"})    });
    
}
if(type=="5")
{
  this.navCtrl.setRoot(TabsPage).then(() => {
    this.navCtrl.push(RequestsPage,{Page:"notification",tap:"OVERTIME"})
  });
}
  }
  openmenu()
  {
    this.menue.open()

  }
  fail(data)
    {
      if (JSON.stringify(data.status) == '401') {
        // UnAuthorized
        this.event.publish("login","");
      }
      console.log("fail get new notification");
      console.log(data);
    }
    success(data)
    {

      console.log("sucess get new notification");
      console.log(data);
      this.notifications=data ;
      if(this.notifications.length==0)
      {
        this.show=false
      }
      else{
        this.show=true
      }
    }
  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificaionPage');
  }

  // ==== mark as seen =========
  mark(id)
  {
    this.serve.markasread(id,data=>this.Marksuccess(data),data=>this.Markfail(data))
  }
  Marksuccess(data)
  {

  }
  Markfail(data)
  {
    if (JSON.stringify(data.status) == '401') {
      // UnAuthorized
      this.event.publish("login","");
    }
  }
  doRefresh(ev)
  {
    let empid;
    this.storage.get("EmpId").then((val) => {
      if (val != null) {
        empid=val
          this.serve.getnewNotification(empid,(data)=>this.success(data),(data)=>this.fail(data))
             }
      })
    ev.complete();

  }
  
}
