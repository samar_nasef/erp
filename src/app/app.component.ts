import { Component, ViewChild, NgZone } from '@angular/core';
import { Platform, NavController, Nav, ToastController, Events, LoadingController} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { TabsPage } from '../pages/tabs/tabs';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { SettingPage } from '../pages/setting/setting';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { HelperProvider } from '../providers/helper/helper';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';
import { Device } from '@ionic-native/device';
import { ContactPage } from '../pages/contact/contact';
import { PendingreqPage } from '../pages/pendingreq/pendingreq';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { RequestsPage } from '../pages/requests/requests';
import { PayslipsPage } from '../pages/payslips/payslips';
import { AboutPage } from '../pages/about/about';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { Network } from '@ionic-native/network';
import { ApiservicesProvider } from '../providers/apiservices/apiservices';
import { DatePipe } from '@angular/common';
// import { BackgroundMode } from '@ionic-native/background-mode/ngx';


declare var cordova
@Component({
  templateUrl: 'app.html',
  providers:[Push,Network]
})
export class MyApp {
  Keyboard: any;
  @ViewChild(Nav) navctrl: Nav;
  @ViewChild("content") nav: NavController
  isRtl:any
  rootPage: any;
  accesstoken: any
  lang_direction: any = ""
  direction:any
  dir: any
  photo: any
  id: any
  name: any
  devID: any
  langDirection: any
  serviceurl: any
  userid: any;
  isAdmin: boolean = false;
  status:any
  constructor(public datepipe: DatePipe,  private network: Network, public ApiservicesProvider: ApiservicesProvider,
    public loadingCtrl: LoadingController,
    private push: Push,
    public keyboard:Keyboard,zone: NgZone,
    public event: Events, public device: Device, public toastCtrl: ToastController, public http: HttpClient, public storage: Storage, public translate: TranslateService, public helper: HelperProvider, public menu: MenuController, public platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {

      // this.backgroundMode.enable();

      

      console.log("time xx : ", this.datepipe.transform(new Date().toISOString(), 'MM/dd/yyyy H:mm'))


      platform.ready().then(() => {
        setTimeout(()=>{
          this.pushnotification();

          let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
            console.log('network was disconnected :-(');
            this.helper.netwrokStatus = 0;
          });

          let connectSubscription = this.network.onConnect().subscribe(() => {
            console.log('network connected!');
            this.helper.netwrokStatus = 1;
            // We just got a connection but we need to wait briefly
             // before we determine the connection type. Might need to wait.
            // prior to doing any api requests as well.
            setTimeout(() => {
              if (this.network.type === 'wifi') {
                console.log('we got a wifi connection, woohoo!');
                this.storage.get("ERPOfflineData").then((val) => {
                  if (val == null) {
                    console.log("nosaved data ")
                  }else{
                    console.log("saved data "+JSON.stringify(val))

                   
                    

            //         this.ApiservicesProvider.insertFingerPrintForOffline(
            // val.lat, val.long, val.empID, val.dir,val.FingerDate,new Date().toLocaleString(),
            //  (data) => this.contactpage.successCallBackLocationForoffline(data), (data) => this.contactpage.failureCallBackLocationForoffline(data));
          console.log("call insert api ")
          
            this.ApiservicesProvider.insertFingerPrintForOffline(
              val.lat, val.long, val.empID, val.dir,val.FingerDate,this.datepipe.transform(new Date().toISOString(), 'MM/dd/yyyy H:mm'),
               (data) =>  this.event.publish("successData",data), (data) => this.event.publish("errorData",data));
          
               
          
             // this.storage.remove("ERPOfflineData");
      

                  }
                })
               
                  

              }
            }, 3000);
          });

          
        }, 1000);
        statusBar.overlaysWebView(false);
        statusBar.backgroundColorByHexString("#4456b9");
      splashScreen.hide();
      
      // event.subscribe('runBackPosition',()=>{
      //   this.runBackPosition()
      // })
      
    
   
    if (this.platform.is('ios')) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
      this.helper.device_type = "1"
    }
    else{
        this.helper.device_type = "0"
    }
    this.event.subscribe("login", ()=>{
      this.navctrl.setRoot(LoginPage); 
      this.storage.remove("access_token");
     this.storage.remove("user_info");
     this.storage.remove("EmpId");
     this.storage.remove("UserPass");
     this.storage.remove("IsAdmin");
    })
 
    this.event.subscribe("userinfo", (name, id, photo, isAdmin,dir) => {
      this.name = name;
      this.langDirection=dir
      this.id = id;
      this.photo = photo;
     
      this.isAdmin = isAdmin;
      this.storage.set("name", name);
      this.storage.set("id", id);
      this.storage.set("photo", photo);
      this.storage.set("IsAdmin", isAdmin);



    });
    this.event.subscribe("dir", (dir) => {
      this.langDirection=dir
      console.log(this.langDirection)
     
    })
    this.storage.get("LanguageApp").then((val) => {
      if (val == null) {
        // if offline lang value not saved get mobile language.
        var userLang = navigator.language.split('-')[0];
        // check if mobile lang is arabic.
        if (userLang == 'ar') {
          this.helper.status="right"
          this.status=this.helper.status
          this.translate.use('ar');
          this.helper.currentLang = 'ar';
          this.translate.setDefaultLang('ar');
          this.helper.lang_direction = 'rtl';
          this.langDirection = "rtl";
          this.platform.setDir('rtl', true)
          this.storage.set("LanguageApp", "ar");
          
        }
        else {
          // if mobile language isn't arabic then make application language is English.
          this.translate.setDefaultLang('en');
          this.helper.currentLang = 'en'
          this.helper.status="left"
          this.status=this.helper.status
          this.translate.use('en');
          this.helper.lang_direction = 'ltr';
          this.langDirection = "ltr";
          this.platform.setDir('ltr', true)
          this.storage.set("LanguageApp", "en");
        }
      }
      //If application language is saved offline and it is arabic.
      else if (val == 'ar') {
        this.helper.status="right"
        this.status=this.helper.status
        this.translate.use('ar');
        this.helper.currentLang = 'ar';
        this.translate.setDefaultLang('ar');
        this.helper.lang_direction = 'rtl';
        this.langDirection = "rtl";
        this.platform.setDir('rtl', true);
        this.dir="right"
      }
      //If application language is saved offline and it is English.
      else if (val == 'en') {
        this.helper.status="left"
        this.status=this.helper.status
        this.translate.setDefaultLang('en');
        this.translate.use('en');
        this.helper.currentLang = 'en'
        this.langDirection = "ltr";
        this.helper.lang_direction = 'ltr';
        this.platform.setDir('ltr', true)
        this.dir="left"
        
      }
      // this.runBackPosition()
      //this.helper.geoLoc(data => this.getCurrentLoc(data));
    });
    this.lang_direction = this.helper.lang_direction

    console.log("lang_direction : ",this.lang_direction)
    //this.runBackPosition()
    
    this.storage.get("name").then((val) => {
      if (val != null) {
        this.name = val
      }
    });
    this.storage.get("id").then((val) => {
      if (val != null) {
        this.id = val
      }
    });
    this.storage.get("photo").then((val) => {
      if (val != null) {
        this.photo = val
      }
    });
    storage.get("access_token").then((val) => {
      if (val) {
        this.accesstoken = val
      }
    });
    console.log("initializeApp")
   this.initializeApp()
  });
  }
  initializeApp(){

    
   

    this.storage.get("EmpId").then((val) => {
      if (val) {
        this.storage.get("IsAdmin").then((val) => {
          if (val) {
            this.isAdmin = val;
          }
        });
        this.helper.Empid = val;
        this.userid = val;
        this.storage.get("url").then((val) => {
          if (val) {
            this.serviceurl = val;
            this.helper.serviceurl = val;
            this.devID = this.device.uuid;
            this.helper.DeviceId = this.device.uuid;
           
            this.checkvalid(data => this.checksuccess(data), data => this.checkfail(data));
          }
        });
       // this.helper.geoLoc(data => {

        // this.storage.get("UserPass").then((val) => {
        //   if (val) {
        //     console.log("user pass : ",val )
        //     this.ApiservicesProvider.login(this.helper.Empid,val,this.helper.DeviceId , this.helper.registration,this.helper.device_type,this.helper.grant_Type, (data) => this.successCallBackLogin(data), (data) => this.failureCallBackLogin(data));
        //   }
        // });

          this.navctrl.setRoot(TabsPage);
       // })
       
      }
      else {
       this.navctrl.setRoot(LoginPage);
      }
    });
  }
  

  // ngOnInit() {
  //   this.platform.ready().then(() => {
  //  cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
  //   this.keyboard.hideFormAccessoryBar(false)
    

  // });
  // }
  checksuccess(data) {
    console.log('Check Valid Result: ' + data);
    if (data == true) {


      

      this.navctrl.setRoot(TabsPage, { tabIndex: 2 });
    }
    else {
      this.rootPage = LoginPage;
    }
  }
 // open messages page
  openmessgaes() {
    this.navctrl.setRoot(TabsPage, { tabIndex: 4 });
    this.menu.close()
  }
  // to open attend ( البصمه ) page
  openattend() {
    this.navctrl.setRoot(TabsPage, { tabIndex: 0 });
    this.menu.close()
  }
  checkfail(data) {
  }
  getAllemployees(authSuccessCallback, authFailureCallback) {
    let serviceUrl;

    let headers = new HttpHeaders()
    let parameter = new HttpParams().set('EmpId', this.helper.Empid).set('Direction', this.helper.Direction);
    headers = headers.set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.accesstoken);//client_credentials

    serviceUrl = this.helper.serviceurl + '/api/HR/GetEmployeeProfile?EmpId=' + this.helper.Empid + '&Direction=' + this.helper.Direction;
    this.http.get(serviceUrl, { headers: headers })
      .subscribe(
        data => {

          authSuccessCallback(data)
        },
        err => {
          authFailureCallback(err);
        }
      )
  }
  checkvalid(authSuccessCallback, authFailureCallback) {
    let headers = new HttpHeaders()
    headers = headers.set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.accesstoken);//client_credentials
    let serviceUrl = this.serviceurl + '/api/HR/IsValidEmployee?EmpId=' + this.helper.Empid;
    this.http.get(serviceUrl, { headers: headers })
      .subscribe(
        data => {

          authSuccessCallback(data)
        },
        err => {
          authFailureCallback(err);
        }
      )
  }
  // to open settings page
  opensetting() {
    if (this.helper.currentLang=="ar" ) {
       this.translate.use('en');
     
       this.platform.setDir('ltr',true)
       this.helper.currentLang = 'en';
    
      this.storage.set("LanguageApp", 'en');
      this.navctrl.setRoot(TabsPage);
      this.helper.Direction='ltr'
    

       
    }
    else {
      this.translate.use('ar');
      this.platform.setDir('rtl',true)
      this.helper.Direction='rtl'

  
      this.helper.currentLang = 'ar';
      
       this.storage.set("LanguageApp", 'ar');
        this.navctrl.setRoot(TabsPage);
    }
 
    this.menu.close()

  }
  // logout from app
  logout() {
    this.navctrl.setRoot(LoginPage)
    this.menu.close()
    this.storage.remove("access_token");
    this.storage.remove("user_info");
    this.storage.remove("EmpId");
    this.storage.remove("UserPass");
    this.storage.remove("IsAdmin");
    this.event.publish("direction",this.langDirection);
  }
  // to open requests page
  requests() {
    this.navctrl.setRoot(TabsPage, { tabIndex: 3 });

    this.menu.close()

  }
  // to open home page
  openhome() {
    this.navctrl.setRoot(TabsPage, { tabIndex: 2 });
    this.menu.close()

  }
  // to open payslips page 
  openpaylips() {
    this.nav.setRoot(TabsPage, { tabIndex: 1 });
    this.menu.close();
  }
  // to open pending request page
  openPendingRequests() {
    this.navctrl.push(PendingreqPage);
    this.menu.close();
  }





  successCallBackLogin(data) {
    console.log(data);
    var jsonObject = JSON.parse(data);
    if (jsonObject.access_token) {
      // this.storage.set("UserPass", this.pass);
      // this.storage.set("url", this.serverurl);
      // this.storage.set("access_token", jsonObject.access_token);
      // this.storage.set("IsAdmin", jsonObject.IsAdmin);

      // this.storage.set("AllowSensor", jsonObject.AllowSensor); //0 fingerprint not necessary , 1 fingerprint necessary
      // this.storage.set("AllowOfflineFinger", jsonObject.AllowOfflineFIngerPrint); //0 offlinefingerprint not available , 1 offlinefingerprint available
      // console.log("set AllowSensor and  AllowOfflineFinger again")
      
      // console.log('IsAdmin: ' + jsonObject.IsAdmin);
      // this.accesstoken = jsonObject.access_token
      // this.helper.serviceurl = this.serverurl;
      // this.helper.Empid = this.username;
      // this.helper.accesstoken = jsonObject.access_token;
      // this.ApiservicesProvider.checkvalidemployee(this.username, (data) => {
    //     if(JSON.stringify(data)=="true")
    //     {   
    //   this.storage.set("EmpId", this.username).then(() => {
    //     this.getAllemployees(data => this.getsuccess(data, jsonObject.IsAdmin), data => this.getfail(data))
    //     this.navCtrl.setRoot(TabsPage);
    //   });
    // }
  
  // else{
  //   const toast = this.toastCtrl.create({
  //     message: this.translate.instant('validation'),
  //     duration: 3000
  //   });
  //   toast.present();
  // }
  
 
// }, (data) => {
//   if (JSON.stringify(data.status) == '401') {
//     // UnAuthorized
//     this.event.publish("login","");
//   }
  console.log(JSON.stringify(data))


// })
  
    }
  }


  failureCallBackLogin(err) {
    console.log("faliure                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ")
  }                                                   







// initialize notification
  pushnotification() {
   
    let options: PushOptions
      options = {
    android: {
         senderID:"936729258762",
         forceShow: true,
         sound: true
        },
        ios: {
          alert: 'true',
          badge: true,
          sound: 'false'
        }
      }
    const pushObject: PushObject = this.push.init(options);
    pushObject.on('notification').subscribe((notification: any) => {
    // for ios devices
      if (this.platform.is('ios')) {
        if (notification.additionalData["gcm.notification.type"] == "1" ) 
        {
          this.navctrl.setRoot(TabsPage).then(() => {
            this.navctrl.push(AboutPage,{Page:"notification"})
            });
        }
        if (notification.additionalData["gcm.notification.type"] == "2" ) 
        {
          this.navctrl.setRoot(TabsPage).then(() => {
            this.navctrl.push(RequestsPage,{Page:"notification",tap:"LEAVE"})
            });
        }
        if (notification.additionalData["gcm.notification.type"] == "3" ) 
        {
          this.navctrl.setRoot(TabsPage).then(() => {
            this.navctrl.push(RequestsPage,{Page:"notification",tap:"OUTSIDEOFFICE"})
            });
        }
        if (notification.additionalData["gcm.notification.type"] == "4" ) 
        {
          this.navctrl.setRoot(TabsPage).then(() => {
            this.navctrl.push(RequestsPage,{Page:"notification",tap:"PERMISSION"})
            });
        }
        if (notification.additionalData["gcm.notification.type"] == "5" ) 
        {
          this.navctrl.setRoot(TabsPage).then(() => {
            this.navctrl.push(RequestsPage,{Page:"notification",tap:"OVERTIME"})
            });
        }
       
      }
// for android devices
      else {
   
     
       if( notification.additionalData.type =="1" )
       {
        this.navctrl.setRoot(TabsPage).then(() => {
          this.navctrl.push(AboutPage,{Page:"notification"})
          });
       }
       if( notification.additionalData.type =="2" )
       {
        this.navctrl.setRoot(TabsPage).then(() => {
          this.navctrl.push(RequestsPage,{Page:"notification",tap:"LEAVE"})
          });
       }
       if( notification.additionalData.type =="3" )
       {
        this.navctrl.setRoot(TabsPage).then(() => {
          this.navctrl.push(RequestsPage,{Page:"notification",tap:"OUTSIDEOFFICE"})
          });
       }
       if( notification.additionalData.type =="4" )
       {
        this.navctrl.setRoot(TabsPage).then(() => {
          this.navctrl.push(RequestsPage,{Page:"notification",tap:"PERMISSION"})
          });

       }
       if( notification.additionalData.type =="5" )
       {
        this.navctrl.setRoot(TabsPage).then(() => {
          this.navctrl.push(RequestsPage,{Page:"notification",tap:"OVERTIME"})
          });
       }
      }
     


    });
    pushObject.on('registration').subscribe((registration: any) => {
  
      this.helper.registration = registration.registrationId;
      console.log(JSON.stringify("registration"+registration.registrationId))
    });

    pushObject.on('error').subscribe(error =>console.log('Error with Push plugin'+JSON.stringify(error)))
  }
}
