import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { SettingPage } from '../pages/setting/setting';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { HttpClient } from '@angular/common/http';
import { RequestsPage } from '../pages/requests/requests';
import {PayslipsPage} from '../pages/payslips/payslips';
import {AttendancePage} from '../pages/attendance/attendance';
import { AddmessagePage } from '../pages/addmessage/addmessage';
import { HttpClientModule } from '@angular/common/http';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { HttpModule, Http } from '@angular/http';
import { TabsPage } from '../pages/tabs/tabs';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { NotificaionPage } from '../pages/notificaion/notificaion';
import { MessagesPage } from '../pages/messages/messages';
import { Geolocation } from '@ionic-native/geolocation';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HelperProvider } from '../providers/helper/helper';
import { IonicStorageModule } from '@ionic/storage';
import { Device } from '@ionic-native/device';
import { Storage } from '@ionic/storage';
import { ApiservicesProvider } from '../providers/apiservices/apiservices';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { OutsideReqPage } from '../pages/outside-req/outside-req';
import { PermissionreqPage } from '../pages/permissionreq/permissionreq';
import { OverTimeReqPage } from '../pages/over-time-req/over-time-req';
import { LeavereqPage } from '../pages/leavereq/leavereq';
import { DatePipe } from '@angular/common';
import { EditPage } from '../pages/edit/edit';
import { Diagnostic } from '@ionic-native/diagnostic';
import { DatePicker } from '@ionic-native/date-picker';
import { PendingreqPage } from '../pages/pendingreq/pendingreq';
import { ViewPage } from '../pages/view/view';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { ReplayPage } from '../pages/replay/replay';
import { CKEditorModule } from 'ng2-ckeditor';
import { FileChooser } from '@ionic-native/file-chooser';
import { IOSFilePicker } from '@ionic-native/file-picker';
import { FilePath } from '@ionic-native/file-path';
import { Base64 } from '@ionic-native/base64';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { EmployeesListPage } from '../pages/employees-list/employees-list';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');

}
@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    SettingPage,
    ContactPage,
    HomePage,
    TabsPage,
    ViewPage,
    LoginPage,
    NotificaionPage,
    MessagesPage,
    RequestsPage,
    OverTimeReqPage,
    EmployeesListPage,
    EditPage,
    PermissionreqPage,
    OutsideReqPage,
    LeavereqPage,
    AddmessagePage,
    PayslipsPage,
    AttendancePage,
    PendingreqPage,
    ReplayPage
  ],
  imports: [
    BrowserModule,
    CKEditorModule,
    HttpModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    EditPage,
    EmployeesListPage,
    AttendancePage,
    MessagesPage,
    PayslipsPage,
    RequestsPage,
    ReplayPage,
    AddmessagePage,
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    SettingPage,
    TabsPage,
    LoginPage,
    NotificaionPage,
    OverTimeReqPage,
    PermissionreqPage,
    OutsideReqPage,
    LeavereqPage,
    ViewPage,
    PendingreqPage
  ],
  providers: [
    Keyboard,
    Base64,
    LocationAccuracy,
    FilePath,
    FileChooser,
    IOSFilePicker,
    DatePicker,
    Diagnostic,
    FileTransfer,
    File,
    StatusBar, 
    Geolocation,
    Device,
    SplashScreen,
    BarcodeScanner,
    DatePicker,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    [DatePipe],
    HelperProvider,
    HttpClient,
    ApiservicesProvider,
  ]
})
export class AppModule { }
